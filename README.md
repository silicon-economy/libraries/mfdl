<div align="center">
  
Material Flow Description Language
===========================

<img src="documentation/img/mfdl_logo.png" alt="mfdl_logo" width="600"/>
<br><br>

[![pipeline status](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/services/odyn-logos/logos/mfdl-scheduler/badges/main/pipeline.svg)](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/services/odyn-logos/logos/mfdl-scheduler/-/commits/main)
</div>

---
<div align="left">
The Material Flow Description Language (MFDL) is used to define transports in a declarative way.
Here, transports are only described, the assignment and the execution of a transport order happens afterwards.
In general, a transport order can be executed by a human, by an automated guided vehicle (AGV), an autonomous mobile robot (AMR) or in collaborative mode.
<br><br>
This repository contains the grammar specification of the MFDL and the corresponding scheduler.
The grammar is realized with the help of [ANTLR](https://www.antlr.org/).

## Install
To use the MFDL Scheduler you have to install the following requirements. Afterwards, the Scheduler and ANTLR grammar can be used as described below.

### Requirements
- Python version >= 3.10
- Pip packages from requirements.txt (`pip install -r requirements.txt`)
- [GraphViz](https://graphviz.org/) (if you want to use the scheduler)

### Generate ANTLR files from the grammar
You can generate ANTLR classes (Lexer, Parser, Visitor and Listener) from the grammar specification.
Make sure to first install ANTLR correctly (this is only mandatory if you make changes to the grammar).
Follow the guide on the [official ANTLR site](https://www.antlr.org/) to install it.
To generate the Lexer and Parser with an additional visitor as Python classes, run the following command inside the `mfdl_grammar` directory:

> antlr4 -Dlanguage=Python3 -visitor MFDLLexer.g4 MFDLParser.g4

Now, the generated classes can be used within a Python script.

### Start the demo
There is a demo file (scheduler_demo.py) demonstrating the use of the scheduler.
To run it, execute the following command from the root directory of the project :
> python scheduler_demo.py --file_path <path_to_mfdl_file>

## License
See the license file in the top directory.

## Contact information

Maintainer:
- Peter Detzner (peter.detzner@iml.fraunhofer.de)
- Maximilian Hörstrup (maximilian.hoerstrup@iml.fraunhofer.de)

Development Team:
- Peter Detzner (peter.detzner@iml.fraunhofer.de)
- Maximilian Hörstrup (maximilian.hoerstrup@iml.fraunhofer.de)
