# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

# standard libraries
import json
import os
import logging
import logging.config
import argparse
from time import sleep
from typing import Dict, List, Union

# local sources
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.actionorder import ActionOrder
from mfdl_scheduler.model.moveorder import MoveOrder
from mfdl_scheduler.model.transportorder import TransportOrder

from mfdl_scheduler.scheduling.scheduler import Scheduler

TEST_FILE_COMMAND_DELAY = 2


def cb_materialflow_started(tasks: List[Task], scheduler_id: str):
    message = "Materiallfow started with the following Tasks: "
    delim = ""
    for task in tasks:
        message = message + delim + task
        delim = ", "
    print(message)


def cb_task_started(task: Task, scheduler_id: str):
    print("Task '" + task.name + "' started!")


def cb_order_started(
    uuid: str, order: Union[TransportOrder, MoveOrder, ActionOrder], scheduler_id: str
):
    print("A " + type(order).__name__ + " with the UUID '" + uuid + "' has started")


def cb_started_by(scheduler_id: str):
    print("StartedBy condition not satisfied yet.")


def cb_waiting_for_move(uuid: str, orderstep_api):
    print("Orderstep with UUID " + uuid + " is waiting for move")


def cb_moved_to_location(uuid: str, location: str, scheduler_id: str):
    print("Moved to location '" + location + "' for Order '" + uuid + "'.")


def cb_waiting_for_action(uuid: str, orderstep_api):
    print("Orderstep with UUID " + uuid + " is waiting for action to be executed")


def cb_action_executed(uuid: str, scheduler_id: str):
    print("Executed Action for Order '" + uuid + "'.")


def cb_order_finished(uuid: str, scheduler_id: str):
    print("Order with the uuid '" + uuid + "' finished.")


def cb_finished_by(
    order: Union[TransportOrder, MoveOrder, ActionOrder], task: Task, scheduler_id: str
):
    print("Order '" + order.uuid_ + "' is waiting for FinishedBy")


def cb_instance_updated(instance_name: str, data: Dict, scheduler_id: str):
    print(
        "The instance '"
        + instance_name
        + "' was updated succesfully with the data: \n"
        + json.dumps(data)
    )


def cb_task_finished(task_name: str, scheduler_id: str):
    print("Task '" + task_name + "' finished.")


def cb_materialflow_finished(scheduler_id: str):
    print("Materialflow finished.")


def main():
    parser = argparse.ArgumentParser(description="Validation demo for the MFDL Scheduler")
    parser.add_argument("--file_path")
    parser.add_argument("-t", "--test_file")
    parser.add_argument("--test_ids", default=False, action="store_true")

    args = parser.parse_args()

    if args.file_path or args.test_file:
        mfdl_string = ""
        with open(args.file_path, "r", encoding="utf-8") as file:
            mfdl_string = file.read()

        use_test_ids = False
        if args.test_ids:
            use_test_ids = True

        scheduler = Scheduler(mfdl_string, file_path=args.file_path, use_test_ids=use_test_ids)

        if scheduler.mfdl_program_valid():
            scheduler.callbacks.register_callback_materialflow_started(cb_materialflow_started)
            scheduler.callbacks.register_callback_task_started(cb_task_started)
            scheduler.callbacks.register_callback_order_started(cb_order_started)
            scheduler.callbacks.register_callback_started_by(cb_started_by)
            scheduler.callbacks.register_callback_waiting_for_move(cb_waiting_for_move)
            scheduler.callbacks.register_callback_moved_to_location(cb_moved_to_location)
            scheduler.callbacks.register_callback_waiting_for_action(cb_waiting_for_action)
            scheduler.callbacks.register_callback_action_executed(cb_action_executed)
            scheduler.callbacks.register_callback_order_finished(cb_order_finished)
            scheduler.callbacks.register_callback_finished_by(cb_finished_by)
            scheduler.callbacks.register_callback_instance_updated(cb_instance_updated)
            scheduler.callbacks.register_callback_task_finished(cb_task_finished)
            scheduler.callbacks.register_callback_materialflow_finished(cb_materialflow_finished)
            scheduler.start()

            if args.test_file:
                commands = []
                with open(args.test_file) as f:
                    for line in f:
                        if not line.startswith("#") and not line == "\n":
                            commands.append(line)

            i = 0
            while scheduler.is_running():
                if args.test_file and i < len(commands):
                    if i > 0:
                        sleep(TEST_FILE_COMMAND_DELAY)
                    input_str = commands[i]
                    i = i + 1
                else:
                    input_str = str(input("Wait for input:>"))

                if input_str != "":
                    if input_str.startswith("sleep"):
                        index = input_str.index(")")
                        sleep(int(input_str[len("sleep(") : index]))
                    elif input_str.startswith("{"):
                        scheduler.fire_event(input_str)
                    else:
                        task_name, orderstep_id, status = input_str.split(",")
                        scheduler.fire_event(
                            '{"event_type": "orderstep_update", "data": {"task": "'
                            + task_name
                            + '", "orderstep_id": "'
                            + orderstep_id
                            + '", "status":"'
                            + status
                            + '"}}'
                        )


if __name__ == "__main__":
    main()
