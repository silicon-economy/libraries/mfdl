# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains integration tests for the SemanticErrorChecker."""

# standard libraries
import unittest

# 3rd party libs
from antlr4.InputStream import InputStream
from antlr4.CommonTokenStream import CommonTokenStream

# local sources
from mfdl_scheduler.parser.MFDLLexer import MFDLLexer
from mfdl_scheduler.parser.MFDLParser import MFDLParser
from mfdl_scheduler.parser.mfdl_visitor import MFDLVisitor

from mfdl_scheduler.validation.error_handler import ErrorHandler
from mfdl_scheduler.validation.semantic_error_checker import SemanticErrorChecker


TEST_FILE_FOLDER_PATH = "tests/test_files/invalid/semantic/"


class TestSemanticErrorChecker(unittest.TestCase):
    """Testcase containing integration tests for the SemanticErrorChecker.

    Attributes:
        lexer: The generated ANTLR Lexer for the MFDL.
        parser: The generated ANTLR Parser for the MFDL.
        self.mfdl_visitor: MFDLVisitor instance.
        error_handler: ErrorHandler instance for couting the errors.
        semantic_error_checker: The SemanticErrorChecker instance which should be tested.
    """

    def setUp(self):
        self.lexer: MFDLLexer = None
        self.parser: MFDLParser = None
        self.mfdl_visitor: MFDLVisitor = None
        self.error_handler: ErrorHandler = None
        self.semantic_error_checker: SemanticErrorChecker = None

    def load_file(self, test_file_name: str):
        """Loads a file from the given path and parses it if it is a MFDL program."""

        file_path = TEST_FILE_FOLDER_PATH + test_file_name
        if file_path.endswith(".mfdl"):
            mfdl_string = ""
            with open(file_path, "r", encoding="utf8") as file:
                mfdl_string = file.read()

            self.lexer = MFDLLexer(InputStream(mfdl_string))
            token_stream = CommonTokenStream(self.lexer)
            self.parser = MFDLParser(token_stream)
            tree = self.parser.program()
            self.error_handler = ErrorHandler(file_path)
            self.mfdl_visitor = MFDLVisitor(file_path, self.error_handler)
            mfdl_program = None
            if not self.error_handler.has_error():
                mfdl_program = self.mfdl_visitor.visitProgram(tree)
            self.semantic_error_checker = SemanticErrorChecker(
                file_path, mfdl_program, self.error_handler
            )

    def test_attribute_missing_in_assignment(self):
        self.load_file("attribute_missing_in_assignment.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_attribute_missing_in_nested_assignment(self):
        self.load_file("attribute_missing_in_nested_assignment.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_duplicate_components(self):
        self.load_file("duplicate_components.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 7)  # 7: Struct, Instance, ...

    def test_duplicate_parameter_name(self):
        self.load_file("duplicate_parameter_name.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_error_in_module(self):
        self.load_file("error_in_module.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_import_module_file_not_found(self):
        self.load_file("import_module_file_not_found.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_import_unknown_module(self):
        self.load_file("import_unknown_module.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_invalid_component_names(self):
        self.load_file("invalid_component_names.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 7)  # 7: Struct, Instance, ...

    def test_invalid_expression_in_constraints(self):
        self.load_file("invalid_expression_in_constraints.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_invalid_expression_in_finished_by(self):
        self.load_file("invalid_expression_in_finished_by.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_invalid_expression_in_started_by(self):
        self.load_file("invalid_expression_in_started_by.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_on_done_and_repeat(self):
        self.load_file("on_done_and_repeat.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_unknown_aos_in_action(self):
        self.load_file("unknown_aos_in_action.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_unknown_attribute_in_nested_assignment(self):
        self.load_file("unknown_attribute_in_nested_assignment.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_unknown_datatype_in_struct(self):
        self.load_file("unknown_datatype_in_struct.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_unknown_identifier_in_rule(self):
        self.load_file("unknown_identifier_in_rule.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_unknown_instance_in_location(self):
        self.load_file("unknown_instance_in_location.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_unknown_parent_struct(self):
        self.load_file("unknown_parent_struct.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_unknown_rule_name_in_rule_call(self):
        self.load_file("unknown_rule_name_in_rule_call.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_unknown_rule_name_in_rule_definition(self):
        self.load_file("unknown_rule_name_in_rule_definition.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        # 2 errors: left and right side dont match and unknown rule name
        self.assertEqual(self.error_handler.semantic_error_count, 2)

    def test_unknown_struct_in_instance(self):
        self.load_file("unknown_struct_in_instance.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_unknown_task_in_on_done(self):
        self.load_file("unknown_task_in_on_done.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_unknown_tos_in_from(self):
        self.load_file("unknown_tos_in_from.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_unknown_tos_in_move(self):
        self.load_file("unknown_tos_in_move.mfdl")
        self.assertEqual(self.error_handler.syntax_error_count, 0)
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_unknown_tos_in_to(self):
        self.load_file("unknown_tos_in_to.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_value_dont_match_in_attribute_assignment(self):
        self.load_file("value_dont_match_in_attribute_assignment.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)

    def test_value_dont_match_in_nested_attribute_assignment(self):
        self.load_file("value_dont_match_in_nested_attribute_assignment.mfdl")
        self.semantic_error_checker.mfdl_program_valid()
        self.assertEqual(self.error_handler.semantic_error_count, 1)
