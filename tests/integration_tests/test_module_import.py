# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains integration tests for the import functionality of the MFDL.

The TestLexerAndParser class contains integration tests which loads the test files
with an absolute and relative import path for the given modules. The correct file
should be found in both cases and only the module content should be loaded
into the final MFDL program.
"""

# standard libraries
import unittest

# 3rd party libs
from antlr4.InputStream import InputStream
from antlr4.error.ErrorListener import ErrorListener
from antlr4.CommonTokenStream import CommonTokenStream

# local sources
from mfdl_scheduler.parser.MFDLLexer import MFDLLexer
from mfdl_scheduler.parser.MFDLParser import MFDLParser
from mfdl_scheduler.parser.mfdl_visitor import MFDLVisitor
from mfdl_scheduler.model.mfdl_program import MFDLProgram
from mfdl_scheduler.validation.error_handler import ErrorHandler


class TestModuleImport(unittest.TestCase):
    def setUp(self) -> None:
        self.lexer: MFDLLexer = None
        self.parser: MFDLParser = None
        self.mfdl_visitor: MFDLVisitor = None

    def load_file(self, file_path: str) -> MFDLProgram:
        """Loads a file from the given path and parses it if it is a MFDL program."""

        if file_path.endswith(".mfdl"):
            mfdl_string = ""
            with open(file_path, "r", encoding="utf8") as file:
                mfdl_string = file.read()

            self.lexer = MFDLLexer(InputStream(mfdl_string))
            self.lexer.removeErrorListeners()
            token_stream = CommonTokenStream(self.lexer)
            self.parser = MFDLParser(token_stream)
            tree = self.parser.program()
            self.mfdl_visitor = MFDLVisitor(file_path, ErrorHandler(file_path))
            return self.mfdl_visitor.visitProgram(tree)

    # The testfiles for both methods only contain an import statement, so every program component
    # in the returned MFDL program should be from the imported modules
    def test_import_from_relative_path(self):
        mfdl_program = self.load_file("tests/test_files/import/ImportTestRelative.mfdl")
        self.assertIsNotNone(mfdl_program)
        self.assertEqual(len(mfdl_program.structs), 4)
        self.assertTrue("Location" in mfdl_program.structs)
        self.assertTrue("Event" in mfdl_program.structs)
        self.assertTrue("Time" in mfdl_program.structs)
        self.assertTrue("Constraint" in mfdl_program.structs)
        self.assertEqual(len(mfdl_program.instances), 2)
        self.assertTrue("location_1" in mfdl_program.instances)
        self.assertTrue("location_2" in mfdl_program.instances)
        self.assertEqual(len(mfdl_program.transportordersteps), 2)
        self.assertTrue("tos_1" in mfdl_program.transportordersteps)
        self.assertTrue("tos_2" in mfdl_program.transportordersteps)
        self.assertEqual(len(mfdl_program.tasks), 1)
        self.assertTrue("simple_transport" in mfdl_program.tasks)

    def test_import_from_absolute_path(self):
        mfdl_program = self.load_file("tests/test_files/import/ImportTestAbsolute.mfdl")
        self.assertIsNotNone(mfdl_program)
        self.assertEqual(len(mfdl_program.structs), 4)
        self.assertTrue("Location" in mfdl_program.structs)
        self.assertTrue("Event" in mfdl_program.structs)
        self.assertTrue("Time" in mfdl_program.structs)
        self.assertTrue("Constraint" in mfdl_program.structs)
        self.assertEqual(len(mfdl_program.instances), 2)
        self.assertTrue("location_1" in mfdl_program.instances)
        self.assertTrue("location_2" in mfdl_program.instances)
        self.assertEqual(len(mfdl_program.transportordersteps), 2)
        self.assertTrue("tos_1" in mfdl_program.transportordersteps)
        self.assertTrue("tos_2" in mfdl_program.transportordersteps)
        self.assertEqual(len(mfdl_program.tasks), 1)
        self.assertTrue("simple_transport" in mfdl_program.tasks)
