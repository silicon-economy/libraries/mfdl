# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains integration tests for the scheduling."""

# standard libraries
from typing import List
import unittest

# 3rd party libs
from snakes.nets import Marking, MultiSet

# local sources
from mfdl_scheduler.scheduling.scheduler import Scheduler

TEST_FILE_FOLDER_PATH = "tests/test_files/valid/scheduling/"
EVENT_FILE_FOLDER_PATH = "tests/test_files/scheduler/"


class TestScheduling(unittest.TestCase):
    def setUp(self) -> None:
        self.scheduler = None

    def load_file(self, test_file_name: str) -> None:
        """Loads a file from the given path and parses it if it is a MFDL program."""

        file_path = TEST_FILE_FOLDER_PATH + test_file_name + ".mfdl"
        mfdl_string = ""
        with open(file_path, "r", encoding="utf8") as file:
            mfdl_string = file.read()

        self.scheduler = Scheduler(
            mfdl_string, file_path=file_path, draw_petri_net=False, use_test_ids=True
        )

    def load_events_from_file(self, file_name) -> List[str]:
        file_path = EVENT_FILE_FOLDER_PATH + file_name + ".txt"
        events = []
        with open(file_path) as f:
            for line in f:
                if not line.startswith("#") and not line == "\n":
                    events.append(line)
        return events

    def check_for_finish(self, test_case_name: str):
        self.load_file(test_case_name)
        events = self.load_events_from_file(test_case_name)
        self.scheduler.start()
        self.assertTrue(self.scheduler.is_running())
        for event in events:
            self.scheduler.fire_event(event)
        self.assertFalse(self.scheduler.is_running())
        petri_net = self.scheduler.petri_net_logic.petri_net

        # check if only the last token in the MF finished place is there
        final_marking = Marking(mf_finished=MultiSet([1]))
        self.assertEqual(petri_net.get_marking(), final_marking)

    def test_child_task_finishes(self):
        self.check_for_finish("child_task")

    def test_everything_finishes(self):
        self.check_for_finish("everything")

    def test_finished_by_finished(self):
        self.check_for_finish("finished_by")

    def test_multiple_orders_finishes(self):
        self.check_for_finish("multiple_orders")

    def test_on_done_task_finishes(self):
        self.check_for_finish("on_done_task")

    def test_parallel_tasks_finishes(self):
        self.check_for_finish("parallel_tasks")

    def test_parameters_finishes(self):
        self.check_for_finish("parameters")

    def test_picklist_task_finishes(self):
        self.check_for_finish("picklist_task")

    def test_rule_in_task_finishes(self):
        self.check_for_finish("rule_in_task")

    def test_simple_action_finishes(self):
        self.check_for_finish("simple_action")

    def test_simple_move_finishes(self):
        self.check_for_finish("simple_move")

    def test_simple_transport_finishes(self):
        self.check_for_finish("simple_transport")

    def test_started_by_finishes(self):
        self.check_for_finish("started_by")

    def test_task_repeat_finishes(self):
        # not implemented yet
        # self.check_for_finish("task_repeat")
        pass

    def test_task_sequence_finishes(self):
        self.check_for_finish("task_sequence")


if __name__ == "__main__":
    unittest.main()
