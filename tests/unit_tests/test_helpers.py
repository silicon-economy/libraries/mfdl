# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

from io import StringIO
import unittest
from typing import List, Any
from unittest.mock import patch
from mfdl_scheduler.helpers import *


class TestHelpers(unittest.TestCase):
    def test_load_file(self):
        file_content = "This is a test file."

        mock_file = StringIO(file_content)

        with patch("builtins.open", return_value=mock_file):
            result = load_file("path/to/test_file.txt")
            self.assertEqual(result, file_content)

    def test_import_list_to_path(self):
        self.assertEqual(import_list_to_path(["path", "to", "file"]), "path/to/file")
        self.assertEqual(import_list_to_path(["path/to", "file"]), "path/to/file")
        self.assertEqual(import_list_to_path(["file"]), "file")
        self.assertEqual(import_list_to_path([]), "")

    def test_check_type_of_value(self):
        self.assertTrue(check_type_of_value(123, "number"))
        self.assertTrue(check_type_of_value(123.45, "number"))
        self.assertFalse(check_type_of_value(True, "number"))
        self.assertFalse(check_type_of_value("123", "number"))
        self.assertTrue(check_type_of_value(True, "boolean"))
        self.assertTrue(check_type_of_value(False, "boolean"))
        self.assertFalse(check_type_of_value(123, "boolean"))
        self.assertTrue(check_type_of_value("hello", "string"))
        self.assertFalse(check_type_of_value(123, "string"))

    def test_is_con(self):
        self.assertTrue(is_con("123"))
        self.assertTrue(is_con("123.45"))
        self.assertTrue(is_con("True"))
        self.assertTrue(is_con('"hello"'))
        self.assertFalse(is_con("hello"))
        self.assertFalse(is_con("[]"))
        self.assertFalse(is_con("{}"))

    def test_is_string(self):
        self.assertTrue(is_string('"hello"'))
        self.assertFalse(is_string("hello"))
        self.assertFalse(is_string("123"))

    def test_is_boolean(self):
        self.assertTrue(is_boolean("True"))
        self.assertTrue(is_boolean("TRUE"))
        self.assertTrue(is_boolean("False"))
        self.assertTrue(is_boolean("FALSE"))
        self.assertFalse(is_boolean("true"))
        self.assertFalse(is_boolean("false"))
        self.assertFalse(is_boolean("123"))

    def test_is_number(self):
        self.assertTrue(is_number("123"))
        self.assertTrue(is_number("123.45"))
        self.assertFalse(is_number("True"))
        self.assertFalse(is_number("hello"))

    def test_is_float(self):
        self.assertTrue(is_float("123.45"))
        self.assertTrue(is_float("-123.45"))
        self.assertTrue(is_float("123"))
        self.assertFalse(is_float("hello"))

    def test_is_int(self):
        self.assertTrue(is_int("123"))
        self.assertTrue(is_int("-123"))
        self.assertFalse(is_int("123.45"))
        self.assertFalse(is_int("hello"))

    def test_is_attribute_access(self):
        self.assertTrue(is_attribute_access("x.y.z"))
        self.assertFalse(is_attribute_access("hello"))
        self.assertFalse(is_attribute_access("123"))
        self.assertFalse(is_attribute_access("'hello'"))
        self.assertFalse(is_attribute_access("x"))

    def test_substitute_parameter_in_expression(self):
        subs = {"x": 5, "y": 3, "z": 2}

        # test string substitution
        self.assertEqual(substitute_parameter_in_expression("x", subs), 5)
        self.assertEqual(substitute_parameter_in_expression("y", subs), 3)
        self.assertEqual(substitute_parameter_in_expression("z", subs), 2)
        self.assertEqual(substitute_parameter_in_expression("hello", subs), "hello")

        # test dict substitution
        d = {"left": "x", "binOp": ">", "right": "y"}
        self.assertEqual(
            substitute_parameter_in_expression(d, subs), {"left": 5, "binOp": ">", "right": 3}
        )
        d = {"left": "(", "binOp": {"left": "x", "binOp": ">", "right": "y"}, "right": ")"}
        self.assertEqual(
            substitute_parameter_in_expression(d, subs),
            {"left": "(", "binOp": {"left": 5, "binOp": ">", "right": 3}, "right": ")"},
        )

    def test_evaluate_expression(self):
        instances = {}
        rules = {}

        self.assertEqual(evaluate_expression(True, instances, rules), True)
        self.assertEqual(evaluate_expression(False, instances, rules), False)
        self.assertEqual(evaluate_expression("true", instances, rules), "true")
        self.assertEqual(evaluate_expression("false", instances, rules), "false")
        self.assertEqual(evaluate_expression("True", instances, rules), "True")
        self.assertEqual(evaluate_expression("False", instances, rules), "False")
        self.assertEqual(evaluate_expression("a_string", instances, rules), "a_string")
        self.assertEqual(evaluate_expression("5", instances, rules), "5")
        self.assertEqual(evaluate_expression(5, instances, rules), 5)

        expression = {"left": 10, "binOp": "+", "right": 5}
        self.assertEqual(evaluate_expression(expression, instances, rules), 15)

        expression = {"left": 10, "binOp": "-", "right": 5}
        self.assertEqual(evaluate_expression(expression, instances, rules), 5)

        expression = {"left": 10, "binOp": "*", "right": 5}
        self.assertEqual(evaluate_expression(expression, instances, rules), 50)

        expression = {"left": 10, "binOp": "/", "right": 5}
        self.assertEqual(evaluate_expression(expression, instances, rules), 2)

        expression = {"left": {"left": 10, "binOp": "+", "right": 5}, "binOp": "==", "right": 15}
        self.assertEqual(evaluate_expression(expression, instances, rules), True)

        expression = {"left": {"left": 10, "binOp": "+", "right": 5}, "binOp": "==", "right": 20}
        self.assertEqual(evaluate_expression(expression, instances, rules), False)

        expression = {"left": {"left": 10, "binOp": "*", "right": 5}, "binOp": "-", "right": 20}
        self.assertEqual(evaluate_expression(expression, instances, rules), 30)

    def test_get_attribute_access_value(self):
        instances = {
            "instance1": Instance(attributes={"attribute1": 10}),
            "instance2": Instance(attributes={"attribute2": 3.14}),
            "instance3": Instance(attributes={"attribute3": True}),
            "instance4": Instance(attributes={"attribute4": "Hello World"}),
        }

        attribute_accesses = [
            ("instance1.attribute1", 10),
            ("instance2.attribute2", 3.14),
            ("instance3.attribute3", True),
            ("instance4.attribute4", "Hello World"),
        ]

        for attribute_access, expected_result in attribute_accesses:
            with self.subTest(attribute_access=attribute_access):
                result = get_attribute_access_value(attribute_access, instances)
                self.assertEqual(result, expected_result)

    def test_evaluate_rule(self):
        instances = {"instance1": {}, "instance2": {}, "instance3": {}}

        rules = {
            "rule1": Rule(parameters={}, expressions=[True, True, True]),
            "rule2": Rule(parameters={}, expressions=[True, False, True]),
        }

        rule_calls = [(("rule1", []), True), (("rule1", []), True), (("rule2", []), False)]

        for rule_call, expected_result in rule_calls:
            with self.subTest(rule_call=rule_call):
                result = evaluate_rule(rule_call, instances, rules)
                self.assertEqual(result, expected_result)


if __name__ == "__main__":
    unittest.main()
