# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the Scheduler class."""

# standard libraries
import unittest
from unittest.mock import MagicMock, patch

# local sources
from mfdl_scheduler.api.orderstep_api import OrderStepAPI
from mfdl_scheduler.model.instance import Instance

from mfdl_scheduler.model.mfdl_program import MFDLProgram
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.transportorderstep import TransportOrderStep
from mfdl_scheduler.scheduling.event import Event

from mfdl_scheduler.scheduling.scheduler import Scheduler, get_startable_tasks


class TestScheduler(unittest.TestCase):
    """Testcase containing unit tests for the Scheduler class

    Attributes:
        scheduler: The Scheduler object that is used for testing.
    """

    def setUp(self):
        self.scheduler = Scheduler("")

    def test_init(self):
        mfdl_string = "valid_mfdl_string"
        scheduler_id = "scheduler_id"
        file_path = "file_path"
        draw_petri_net = True
        use_test_ids = False

        with patch("mfdl_scheduler.scheduling.scheduler.parse_string", return_value=MFDLProgram()):
            scheduler = Scheduler(
                mfdl_string, scheduler_id, file_path, draw_petri_net, use_test_ids
            )

        # Check if the object is initialized correctly
        self.assertEqual(scheduler.scheduler_id, scheduler_id)
        self.assertFalse(scheduler._is_running)
        self.assertIsNotNone(scheduler.mfdl_program)
        self.assertIsNotNone(scheduler.callbacks)
        self.assertEqual(scheduler.active_ordersteps, [])
        self.assertEqual(scheduler.active_tasks, [])
        self.assertEqual(scheduler.awaited_events, {"general": []})
        self.assertIsNotNone(scheduler.petri_net_logic)

    def test_register_for_petrinet_callbacks(self):
        callbacks = self.scheduler.petri_net_generator.callbacks
        callbacks.materialflow_started_callback = None
        callbacks.started_by_callback = None
        callbacks.task_started_callback = None
        callbacks.order_started_callback = None
        callbacks.waiting_for_move_callback = None
        callbacks.moved_to_location_callback = None
        callbacks.waiting_for_action_callback = None
        callbacks.action_executed_callback = None
        callbacks.order_finished_callback = None
        callbacks.task_finished_callback = None
        callbacks.finished_by_callback = None
        callbacks.materialflow_finished_callback = None

        self.scheduler.on_materialflow_started = MagicMock()
        self.scheduler.on_started_by = MagicMock()
        self.scheduler.on_task_started = MagicMock()
        self.scheduler.on_order_started = MagicMock()
        self.scheduler.on_waiting_for_move = MagicMock()
        self.scheduler.on_moved_to_location = MagicMock()
        self.scheduler.on_waiting_for_action = MagicMock()
        self.scheduler.on_action_executed = MagicMock()
        self.scheduler.on_order_finished = MagicMock()
        self.scheduler.on_task_finished = MagicMock()
        self.scheduler.on_finished_by = MagicMock()
        self.scheduler.on_materialflow_finished = MagicMock()

        self.scheduler.register_for_petrinet_callbacks()

        self.assertEqual(
            callbacks.materialflow_started_callback, self.scheduler.on_materialflow_started
        )
        self.assertEqual(callbacks.started_by_callback, self.scheduler.on_started_by)
        self.assertEqual(callbacks.task_started_callback, self.scheduler.on_task_started)
        self.assertEqual(callbacks.order_started_callback, self.scheduler.on_order_started)
        self.assertEqual(callbacks.waiting_for_move_callback, self.scheduler.on_waiting_for_move)
        self.assertEqual(callbacks.moved_to_location_callback, self.scheduler.on_moved_to_location)
        self.assertEqual(
            callbacks.waiting_for_action_callback, self.scheduler.on_waiting_for_action
        )
        self.assertEqual(callbacks.action_executed_callback, self.scheduler.on_action_executed)
        self.assertEqual(callbacks.order_finished_callback, self.scheduler.on_order_finished)
        self.assertEqual(callbacks.task_finished_callback, self.scheduler.on_task_finished)
        self.assertEqual(callbacks.finished_by_callback, self.scheduler.on_finished_by)
        self.assertEqual(
            callbacks.materialflow_finished_callback, self.scheduler.on_materialflow_finished
        )

    def test_set_timer_for_time_instances(self):
        time_instance_1 = Instance("time_1", {"timing": "30 08 * * *"})
        time_instance_2 = Instance("time_2", {"timing": "30 08 * * *"})
        time_instance_3 = Instance("time_3", {"timing": "30 08 * * *"})
        time_instances = [
            time_instance_1,
            time_instance_2,
            time_instance_3,
        ]
        mock_get_instances = MagicMock(return_value=time_instances)
        self.scheduler.mfdl_program.get_instances = mock_get_instances

        mock_timer = MagicMock()
        with patch("threading.Timer", mock_timer):
            self.scheduler.set_timer_for_time_instances()

        self.assertEqual(mock_get_instances.call_count, 1)

        def Any(cls):
            class Any(cls):
                def __eq__(self, other):
                    return isinstance(other, cls)

            return Any()

        mock_timer.assert_called_with(
            Any(float),
            self.scheduler.update_time_instance,
            [time_instance_3.name],
        )
        self.assertEqual(mock_timer.call_count, len(time_instances))

    def test_update_time_instance(self):
        instance_name = "time_instance"
        new_values = {"value": True}

        self.scheduler.update_instance = MagicMock()

        self.scheduler.update_time_instance(instance_name)

        self.scheduler.update_instance.assert_called_with(instance_name, new_values)

    def test_mfdl_program_valid(self):
        self.scheduler.mfdl_program = None
        self.assertFalse(self.scheduler.mfdl_program_valid())

        self.scheduler.mfdl_program = MagicMock()
        self.assertTrue(self.scheduler.mfdl_program_valid())

    def test_start(self):
        self.scheduler.mfdl_program = None
        self.scheduler._is_running = False

        dummy_callback = MagicMock()
        self.scheduler.callbacks.materialflow_started_callbacks = [dummy_callback]

        with patch.object(self.scheduler, "fire_event") as mock:
            self.scheduler.start()

        self.assertFalse(self.scheduler._is_running)
        self.assertEqual(self.scheduler.awaited_events["general"], [])
        self.assertEqual(mock.call_count, 0)

        self.scheduler.mfdl_program = MagicMock()
        self.scheduler._is_running = False

        with patch("mfdl_scheduler.petri_net.logic.PetriNetLogic.evaluate_petri_net"):
            with patch.object(self.scheduler, "fire_event") as mock:
                self.scheduler.start()

        self.assertTrue(self.scheduler._is_running)
        mock.assert_called()

        self.assertEqual(dummy_callback.call_count, 1)
        self.assertEqual(dummy_callback.call_count, 1)
        dummy_callback.assert_called_with(
            self.scheduler.mfdl_program.tasks, self.scheduler.scheduler_id
        )
        dummy_callback.assert_called_with(
            self.scheduler.mfdl_program.tasks, self.scheduler.scheduler_id
        )

    def test_is_running(self):
        self.scheduler._is_running = True
        self.assertTrue(self.scheduler.is_running())

        self.scheduler._is_running = False
        self.assertFalse(self.scheduler.is_running())

    def test_fire_event(self):
        event_1 = Event(
            event_type="instance_update", data={"instance_name": "example", "new_values": {}}
        )
        self.scheduler.update_instance = MagicMock(return_value=True)

        result_1 = self.scheduler.fire_event(event_1)

        self.assertTrue(result_1)
        self.scheduler.update_instance.assert_called_once_with("example", {})

        event_2 = Event(event_type="some_event_type", data={"task": "example"})
        self.scheduler.awaited_events = {"example": [event_2]}
        self.scheduler.petri_net_logic.fire_event = MagicMock()

        result_2 = self.scheduler.fire_event(event_2)

        self.assertTrue(result_2)
        self.scheduler.petri_net_logic.fire_event.assert_called_once_with(event_2)
        self.assertNotIn(event_2, self.scheduler.awaited_events["example"])

        event_3 = Event(event_type="some_event_type", data={"task": "example"})
        self.scheduler.awaited_events = {"other_task": []}

        result_3 = self.scheduler.fire_event(event_3)

        self.assertFalse(result_3)

    def test_update_instance_existing_instance(self):
        instance_name = "instance"
        new_values = {"attribute1": "value1", "attribute2": "value2"}

        self.scheduler.mfdl_program.instances = {"instance": Instance()}

        task_1 = Task("task_1")
        task_1.started_by_expr = True
        self.scheduler.mfdl_program.tasks = {"task_1": task_1}
        self.scheduler.active_tasks = ["task_1"]

        self.scheduler.fire_event = MagicMock()
        self.scheduler.on_instance_updated = MagicMock()

        with patch(
            "mfdl_scheduler.scheduling.scheduler.evaluate_expression", return_value=True
        ) as mock:
            result = self.scheduler.update_instance(instance_name, new_values)

        self.assertTrue(result)
        self.assertEqual(
            self.scheduler.mfdl_program.instances[instance_name].attributes["attribute1"], "value1"
        )
        self.assertEqual(
            self.scheduler.mfdl_program.instances[instance_name].attributes["attribute2"], "value2"
        )
        mock.assert_called()
        self.scheduler.fire_event.assert_called()
        self.scheduler.on_instance_updated.assert_called_with(instance_name, new_values)

    def test_update_instance_non_existing_instance(self):
        instance_name = "instance"
        new_values = {"attribute1": "value1", "attribute2": "value2"}

        self.scheduler.mfdl_program.instances = {}

        result = self.scheduler.update_instance(instance_name, new_values)

        self.assertFalse(result)

    def test_on_materialflow_started(self):
        task_1 = Task()
        task_2 = Task()
        task_3 = Task()
        tasks = [task_1, task_2, task_3]

        self.scheduler.on_task_started = MagicMock()
        self.scheduler.on_materialflow_started(tasks)

        self.assertEqual(self.scheduler.on_task_started.call_count, len(tasks))
        self.scheduler.on_task_started.assert_called_with(task_3)

    def test_on_task_started(self):
        task = Task()

        callback_1 = MagicMock()
        callback_2 = MagicMock()

        self.scheduler.callbacks.task_started_callbacks = [callback_1, callback_2]

        self.scheduler.on_task_started(task)

        self.assertEqual(self.scheduler.active_tasks, [task.name])
        self.assertEqual(callback_1.call_count, 1)
        self.assertEqual(callback_2.call_count, 1)
        callback_1.assert_called_with(task, self.scheduler.scheduler_id)
        callback_2.assert_called_with(task, self.scheduler.scheduler_id)

    def test_on_order_started(self):
        order = TransportOrder()
        task_name = "task_1"

        callback_1 = MagicMock()
        callback_2 = MagicMock()

        self.scheduler.callbacks.order_started_callback = [callback_1, callback_2]

        self.scheduler.on_order_started(order, task_name)

        self.assertEqual(callback_1.call_count, 1)
        self.assertEqual(callback_2.call_count, 1)
        callback_1.assert_called_with(order.uuid_, order, self.scheduler.scheduler_id)
        callback_2.assert_called_with(order.uuid_, order, self.scheduler.scheduler_id)

    def test_on_started_by_task(self):
        task = Task("task_1")
        orderstep_api = None
        self.scheduler.fire_event = MagicMock()
        self.scheduler.awaited_events["task_1"] = []

        with patch(
            "mfdl_scheduler.scheduling.scheduler.evaluate_expression", return_value=True
        ) as mock:
            self.scheduler.on_started_by(task, orderstep_api)

        self.assertEqual(len(self.scheduler.awaited_events["task_1"]), 1)
        self.scheduler.fire_event.assert_called_with(Event("started_by", {"task": task.name}))

        callback_method = MagicMock()

        self.scheduler.callbacks.started_by_callbacks = [callback_method]

        with patch("mfdl_scheduler.helpers.evaluate_expression", return_value=False) as mock:
            self.scheduler.on_started_by(task, orderstep_api)

        self.assertEqual(len(self.scheduler.awaited_events["task_1"]), 2)
        self.scheduler.fire_event.assert_not_called
        self.assertEqual(callback_method.call_count, 1)
        callback_method.assert_called_with(self.scheduler.scheduler_id)

    def test_on_started_by_orderstep(self):
        task = Task("task_1")
        orderstep_api = OrderStepAPI(TransportOrderStep())

        callback_method = MagicMock()

        self.scheduler.awaited_events = {"task_1": []}
        self.scheduler.fire_event = MagicMock()

        with patch(
            "mfdl_scheduler.scheduling.scheduler.evaluate_expression", return_value=True
        ) as mock:
            self.scheduler.on_started_by(task, orderstep_api)

        self.assertEqual(len(self.scheduler.awaited_events["task_1"]), 1)
        self.scheduler.fire_event.assert_called_with(
            Event("started_by", {"task": task.name, "orderstep": orderstep_api.id})
        )
        self.assertEqual(callback_method.call_count, 0)

        self.scheduler.callbacks.started_by_callbacks = [callback_method]

        with patch(
            "mfdl_scheduler.scheduling.scheduler.evaluate_expression", return_value=False
        ) as mock:
            self.scheduler.on_started_by(task, orderstep_api)

        self.assertEqual(len(self.scheduler.awaited_events["task_1"]), 2)
        self.scheduler.fire_event.assert_called_with(
            Event("started_by", {"task": task.name, "orderstep": orderstep_api.id})
        )
        self.assertEqual(callback_method.call_count, 1)
        callback_method.assert_called_with(self.scheduler.scheduler_id)

    def test_on_finished_by(self):
        order = TransportOrder()
        orderstep_api = OrderStepAPI(TransportOrderStep())
        task = Task("task_1")

        callback_method = MagicMock()

        self.scheduler.awaited_events = {"task_1": []}
        self.scheduler.fire_event = MagicMock()

        with patch(
            "mfdl_scheduler.scheduling.scheduler.evaluate_expression", return_value=True
        ) as mock:
            self.scheduler.on_finished_by(order, orderstep_api, task)

        self.assertEqual(len(self.scheduler.awaited_events["task_1"]), 1)
        self.scheduler.fire_event.assert_called_with(
            Event("finished_by", {"task": task.name, "orderstep": orderstep_api.id})
        )
        self.assertEqual(callback_method.call_count, 0)

        self.scheduler.callbacks.finished_by_callbacks = [callback_method]

        self.scheduler.on_finished_by(order, orderstep_api, task)

        self.assertEqual(len(self.scheduler.awaited_events["task_1"]), 2)
        self.scheduler.fire_event.assert_called_with(
            Event("finished_by", {"task": task.name, "orderstep": orderstep_api.id})
        )
        self.assertEqual(callback_method.call_count, 1)
        callback_method.assert_called_with(order, task, self.scheduler.scheduler_id)

    def test_on_waiting_for_move(self):
        order = TransportOrder()
        orderstep = OrderStepAPI(TransportOrderStep())
        task = Task("task_1")

        callback_method = MagicMock()

        self.scheduler.awaited_events = {"task_1": []}
        self.scheduler.callbacks.waiting_for_move_callbacks = [callback_method]

        self.scheduler.on_waiting_for_move(order, orderstep, task)

        self.assertEqual(len(self.scheduler.awaited_events["task_1"]), 1)
        self.assertEqual(callback_method.call_count, 1)
        callback_method.assert_called_with(orderstep.id, orderstep)

    def test_on_moved_to_location(self):
        order = TransportOrder()
        orderstep_api = OrderStepAPI(TransportOrderStep())
        task = Task("task_1")

        callback_method = MagicMock()

        self.scheduler.callbacks.moved_to_location_callbacks = [callback_method]

        self.scheduler.on_moved_to_location(order, orderstep_api, task)

        self.assertEqual(callback_method.call_count, 1)
        callback_method.assert_called_with(
            order.uuid_, orderstep_api.orderstep.location_name, self.scheduler.scheduler_id
        )

    def test_on_waiting_for_action(self):
        order = TransportOrder()
        orderstep_api = OrderStepAPI(TransportOrderStep())
        task = Task("task_1")

        callback_method = MagicMock()

        self.scheduler.awaited_events = {"task_1": []}
        self.scheduler.callbacks.waiting_for_action_callbacks = [callback_method]

        self.scheduler.on_waiting_for_action(order, orderstep_api, task)

        self.assertEqual(len(self.scheduler.awaited_events["task_1"]), 1)
        self.assertEqual(callback_method.call_count, 1)
        callback_method.assert_called_with(orderstep_api.id, orderstep_api)

    def test_on_action_executed(self):
        order = TransportOrder()
        orderstep_api = OrderStepAPI(TransportOrderStep())
        task = Task("task_1")

        callback_1 = MagicMock()
        callback_2 = MagicMock()

        self.scheduler.callbacks.action_executed_callbacks = [callback_1, callback_2]

        self.scheduler.on_action_executed(order, orderstep_api, task)

        self.assertEqual(callback_1.call_count, 1)
        self.assertEqual(callback_2.call_count, 1)
        callback_1.assert_called_with(order.uuid_, self.scheduler.scheduler_id)
        callback_2.assert_called_with(order.uuid_, self.scheduler.scheduler_id)

    def test_on_order_finished(self):
        task = Task("task_1")

        callback_1 = MagicMock()
        callback_2 = MagicMock()

        self.scheduler.callbacks.order_finished_callbacks = [callback_1, callback_2]
        self.scheduler.awaited_events = {"task_1": []}
        self.scheduler.fire_event = MagicMock()

        uuid = "id"
        last_order = True

        self.scheduler.on_order_finished(uuid, last_order, task)

        self.assertEqual(callback_1.call_count, 1)
        self.assertEqual(callback_2.call_count, 1)
        callback_1.assert_called_with(uuid, self.scheduler.scheduler_id)
        callback_2.assert_called_with(uuid, self.scheduler.scheduler_id)
        self.assertEqual(self.scheduler.fire_event.call_count, 0)

        task.finished_by_expr = True

        with patch(
            "mfdl_scheduler.scheduling.scheduler.evaluate_expression", return_value=True
        ) as mock:
            self.scheduler.on_order_finished(uuid, last_order, task)

        self.assertEqual(callback_1.call_count, 2)
        self.assertEqual(callback_2.call_count, 2)
        callback_1.assert_called_with(uuid, self.scheduler.scheduler_id)
        callback_2.assert_called_with(uuid, self.scheduler.scheduler_id)
        self.assertEqual(self.scheduler.fire_event.call_count, 1)
        self.scheduler.fire_event.assert_called_with(
            Event(event_type="finished_by", data={"task": task.name})
        )

    def test_on_instance_updated(self):
        callback_1 = MagicMock()
        callback_2 = MagicMock()

        instance_name = "instance"
        data = {"key": "value"}

        self.scheduler.callbacks.instance_updated_callbacks = [callback_1, callback_2]

        self.scheduler.on_instance_updated(instance_name, data)

        self.assertEqual(callback_1.call_count, 1)
        self.assertEqual(callback_2.call_count, 1)
        callback_1.assert_called_with(instance_name, data, self.scheduler.scheduler_id)
        callback_2.assert_called_with(instance_name, data, self.scheduler.scheduler_id)

    def test_on_task_finished(self):
        task_name = "task_1"
        callback_1 = MagicMock()
        callback_2 = MagicMock()

        self.scheduler.active_tasks = ["task_1", "task_2", "task_3"]
        self.scheduler.callbacks.task_finished_callbacks = [callback_1, callback_2]

        self.scheduler.on_task_finished(task_name)

        self.assertEqual(self.scheduler.active_tasks, ["task_2", "task_3"])
        self.assertEqual(callback_1.call_count, 1)
        self.assertEqual(callback_2.call_count, 1)
        callback_1.assert_called_with(task_name, self.scheduler.scheduler_id)
        callback_2.assert_called_with(task_name, self.scheduler.scheduler_id)

    def test_on_materialflow_finished(self):
        callback_1 = MagicMock()
        callback_2 = MagicMock()

        self.scheduler.callbacks.materialflow_finished_callbacks = [callback_1, callback_2]

        self.scheduler.on_materialflow_finished()

        self.assertEqual(callback_1.call_count, 1)
        self.assertEqual(callback_2.call_count, 1)
        callback_1.assert_called_with(self.scheduler.scheduler_id)
        callback_2.assert_called_with(self.scheduler.scheduler_id)
        self.assertFalse(self.scheduler.is_running())

    def test_get_startable_tasks(self):
        mfdl_program = MFDLProgram()
        task_1 = Task("task_1")
        task_2 = Task("task_2")
        task_3 = Task("task_3")
        task_4 = Task("task_4")
        task_5 = Task("task_5")
        task_6 = Task("task_6")
        task_7 = Task("task_7")
        task_1.follow_up_task_names = ["task_2", "task_3"]
        task_2.follow_up_task_names = ["task_4"]
        task_3.follow_up_task_names = ["task_4"]
        task_4.follow_up_task_names = ["task_5", "task_6"]

        mfdl_program.tasks = {
            "task_1": task_1,
            "task_2": task_2,
            "task_3": task_3,
            "task_4": task_4,
            "task_5": task_5,
            "task_6": task_6,
            "task_7": task_7,
        }

        startable_tasks = get_startable_tasks(mfdl_program)

        self.assertEqual(len(startable_tasks), 2)
        self.assertIn(task_1, startable_tasks)
        self.assertIn(task_7, startable_tasks)
