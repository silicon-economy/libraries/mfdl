# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3


"""Contains unit tests for the Event class."""

# standard libraries
import unittest

# local sources
from mfdl_scheduler.scheduling.event import Event


class TestEvent(unittest.TestCase):
    """Testcase containing unit tests for the Event class."""

    def test_init(self):
        event_type = "event_type"
        data = {"key": "value"}

        event = Event(event_type, data)

        self.assertEqual(event.event_type, event_type)
        self.assertEqual(event.data, data)

    def test_eq(self):
        event_1 = Event("event_type", {"key": "value"})
        event_2 = Event("event_type", {"key": "value"})
        event_3 = Event("other_event_type", {"key": "value"})
        event_4 = Event("event_type", {"key": "other_value"})

        self.assertEqual(event_1, event_2)
        self.assertNotEqual(event_1, event_3)
        self.assertNotEqual(event_1, event_4)

    def test_from_json(self):
        json_string = '{"event_type": "event_type", "data": {"key": "value"}}'

        event = Event.from_json(json_string)

        self.assertIsNotNone(event)
        self.assertEqual(event.event_type, "event_type")
        self.assertEqual(event.data, {"key": "value"})
