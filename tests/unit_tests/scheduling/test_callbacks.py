# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the MaterialFlowCallbacks class."""

# standard libraries
import unittest

# local sources
from mfdl_scheduler.scheduling.callbacks import MaterialFlowCallbacks


class TestMaterialFlowCallbacks(unittest.TestCase):
    """Testcase containing unit tests for the MaterialFlowCallbacks class."""

    def test_register_callback_materialflow_started(self):
        def callback(tasks):
            pass

        callbacks = MaterialFlowCallbacks()
        callbacks.register_callback_materialflow_started(callback)

        self.assertEqual(len(callbacks.materialflow_started_callbacks), 1)
        self.assertEqual(callbacks.materialflow_started_callbacks[0], callback)

    def test_register_callback_task_started(self):
        def callback(task):
            pass

        callbacks = MaterialFlowCallbacks()
        callbacks.register_callback_task_started(callback)

        self.assertEqual(len(callbacks.task_started_callbacks), 1)
        self.assertEqual(callbacks.task_started_callbacks[0], callback)

    def test_register_callback_order_started(self):
        def callback(name, order, task_name):
            pass

        callbacks = MaterialFlowCallbacks()
        callbacks.register_callback_order_started(callback)

        self.assertEqual(len(callbacks.order_started_callback), 1)
        self.assertEqual(callbacks.order_started_callback[0], callback)

    def test_register_callback_started_by(self):
        def callback():
            pass

        callbacks = MaterialFlowCallbacks()
        callbacks.register_callback_started_by(callback)

        self.assertEqual(len(callbacks.started_by_callbacks), 1)
        self.assertEqual(callbacks.started_by_callbacks[0], callback)

    def test_register_callback_waiting_for_move(self):
        def callback():
            pass

        callbacks = MaterialFlowCallbacks()
        callbacks.register_callback_waiting_for_move(callback)

        self.assertEqual(len(callbacks.waiting_for_move_callbacks), 1)
        self.assertEqual(callbacks.waiting_for_move_callbacks[0], callback)

    def test_register_callback_moved_to_location(self):
        def callback():
            pass

        callbacks = MaterialFlowCallbacks()
        callbacks.register_callback_moved_to_location(callback)

        self.assertEqual(len(callbacks.moved_to_location_callbacks), 1)
        self.assertEqual(callbacks.moved_to_location_callbacks[0], callback)

    def test_register_callback_waiting_for_action(self):
        def callback():
            pass

        callbacks = MaterialFlowCallbacks()
        callbacks.register_callback_waiting_for_action(callback)

        self.assertEqual(len(callbacks.waiting_for_action_callbacks), 1)
        self.assertEqual(callbacks.waiting_for_action_callbacks[0], callback)

    def test_register_callback_action_executed(self):
        def callback():
            pass

        callbacks = MaterialFlowCallbacks()
        callbacks.register_callback_action_executed(callback)

        self.assertEqual(len(callbacks.action_executed_callbacks), 1)
        self.assertEqual(callbacks.action_executed_callbacks[0], callback)

    def test_register_callback_order_finished(self):
        def callback(name, order):
            pass

        callbacks = MaterialFlowCallbacks()
        callbacks.register_callback_order_finished(callback)

        self.assertEqual(len(callbacks.order_finished_callbacks), 1)
        self.assertEqual(callbacks.order_finished_callbacks[0], callback)

    def test_register_callback_finished_by(self):
        def callback():
            pass

        callbacks = MaterialFlowCallbacks()
        callbacks.register_callback_finished_by(callback)

        self.assertEqual(len(callbacks.finished_by_callbacks), 1)
        self.assertEqual(callbacks.finished_by_callbacks[0], callback)

    def test_register_callback_instance_updated(self):
        def callback():
            pass

        callbacks = MaterialFlowCallbacks()
        callbacks.register_callback_instance_updated(callback)

        self.assertEqual(len(callbacks.instance_updated_callbacks), 1)
        self.assertEqual(callbacks.instance_updated_callbacks[0], callback)

    def test_register_callback_task_finished(self):
        def callback(task):
            pass

        callbacks = MaterialFlowCallbacks()
        callbacks.register_callback_task_finished(callback)

        self.assertEqual(len(callbacks.task_finished_callbacks), 1)
        self.assertEqual(callbacks.task_finished_callbacks[0], callback)

    def test_register_callback_materialflow_finished(self):
        def callback():
            pass

        callbacks = MaterialFlowCallbacks()
        callbacks.register_callback_materialflow_finished(callback)

        self.assertEqual(len(callbacks.materialflow_finished_callbacks), 1)
        self.assertEqual(callbacks.materialflow_finished_callbacks[0], callback)
