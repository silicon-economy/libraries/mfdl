# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the SemanticErrorChecker class."""

# standard libraries
import unittest
from unittest.mock import MagicMock, patch
from mfdl_scheduler.model.actionorder import ActionOrder
from mfdl_scheduler.model.actionorderstep import ActionOrderStep
from mfdl_scheduler.model.instance import Instance

# local sources
from mfdl_scheduler.model.mfdl_program import MFDLProgram
from mfdl_scheduler.model.module import Module
from mfdl_scheduler.model.moveorder import MoveOrder
from mfdl_scheduler.model.rule import Rule
from mfdl_scheduler.model.struct import Struct
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.transportorderstep import TransportOrderStep
from mfdl_scheduler.validation.error_handler import ErrorHandler
from mfdl_scheduler.validation.semantic_error_checker import SemanticErrorChecker


class TestSemanticErrorChecker(unittest.TestCase):
    """Testcase containing unit tests for the SemanticErrorChecker class.

    Attributes:
        mfdl_program: The MFDL program that is validated for the test.
        checker: The SemanticErrorChecker object used for testing.
    """

    def setUp(self):
        self.mfdl_program = MFDLProgram()

        struct = Struct(
            name="Person",
            attributes={"name": "string", "age": "number"},
            parent_struct_name="",
            context="",
        )
        struct.attribute_contexts = {"name": None, "age": None, "id": None, "time": None}
        self.mfdl_program.structs = {"Person": struct}

        self.checker = SemanticErrorChecker("", self.mfdl_program, ErrorHandler(""))

    def test_mfdl_program_valid(self):
        self.assertTrue(self.checker.mfdl_program_valid())
        self.mfdl_program.structs["person"] = Struct(
            name="person",
            attributes={},
            parent_struct_name="",
            context="",
        )
        self.assertFalse(self.checker.mfdl_program_valid())

    def test_check_structs(self):
        self.assertTrue(self.checker.check_structs())

        self.mfdl_program.structs["person"] = Struct(
            name="person",
            attributes={},
            parent_struct_name="",
            context="",
        )
        self.assertFalse(self.checker.check_structs())

        self.mfdl_program.structs["Child"] = Struct(
            name="Child",
            attributes={},
            parent_struct_name="UnknownStruct",
            context="",
        )
        self.assertFalse(self.checker.check_structs())

        self.mfdl_program.structs["Person"].attributes["age"] = "ListUnknownType"
        self.assertFalse(self.checker.check_structs())

    def test_check_for_invalid_struct_name(self):
        struct = Struct(
            name="Person",
            attributes={},
            parent_struct_name="",
            context="",
        )
        self.assertTrue(self.checker.check_for_invalid_struct_name(struct))

        struct = Struct(
            name="person",
            attributes={},
            parent_struct_name="",
            context="",
        )
        self.assertFalse(self.checker.check_for_invalid_struct_name(struct))

    def test_check_for_unknown_parent_struct(self):
        struct = Struct(
            name="Child",
            attributes={},
            parent_struct_name="Person",
            context="",
        )
        self.assertTrue(self.checker.check_for_unknown_parent_struct(struct))

        struct = Struct(
            name="Child",
            attributes={},
            parent_struct_name="UnknownStruct",
            context="",
        )
        self.assertFalse(self.checker.check_for_unknown_parent_struct(struct))

    def test_check_for_unknown_datatypes_in_struct_definition(self):
        struct = Struct(
            name="Person",
            attributes={
                "name": "string",
                "age": "number",
            },
            parent_struct_name="",
            context="",
        )

        struct.attribute_contexts = {"name": None, "age": None, "id": None, "time": None}
        self.assertTrue(self.checker.check_for_unknown_datatypes_in_struct_definition(struct))

        struct = Struct(
            name="Person",
            attributes={
                "name": "string",
                "age": "int",
                "unknown": "ListUnknownType",
            },
            parent_struct_name="",
            context="",
        )
        struct.attribute_contexts = {
            "name": None,
            "age": None,
            "id": None,
            "time": None,
            "unknown": None,
        }

        self.assertFalse(self.checker.check_for_unknown_datatypes_in_struct_definition(struct))

    def test_check_instances(self):
        instance = Instance(name="instance", struct_name="struct")
        self.checker.mfdl_program.instances["instance"] = instance
        self.checker.mfdl_program.structs["struct"] = Struct()

        result = self.checker.check_instances()
        self.assertTrue(result)

    def test_check_for_invalid_instance_name(self):
        instance = Instance(name="instance")
        result = self.checker.check_for_invalid_instance_name(instance)
        self.assertTrue(result)

        instance = Instance(name="Invalid")
        result = self.checker.check_for_invalid_instance_name(instance)
        self.assertFalse(result)

    def test_check_if_instance_attributes_exist_in_struct(self):
        struct = Struct()
        struct.attributes = {
            "attribute1": "type_1",
            "attribute2": "type_2",
        }

        instance = Instance(name="instance")
        instance.attributes = {
            "attribute1": "value_1",
            "attribute2": "value_2",
        }
        self.checker.mfdl_program.structs["struct"] = struct

        result = self.checker.check_if_instance_attributes_exist_in_struct(struct, instance)
        self.assertTrue(result)

    def test_check_attribute_access(self):
        struct = Struct()
        struct.attributes = {
            "attribute1": "type_1",
            "nested_struct": "nested_struct",
        }

        nested_struct = Struct()
        nested_struct.attributes = {
            "attribute2": "type_2",
        }
        self.checker.mfdl_program.structs["struct"] = struct
        self.checker.mfdl_program.structs["nested_struct"] = nested_struct

        result = self.checker.check_attribute_access(struct, "nested_struct.attribute2")

        self.assertTrue(result)

    def test_check_if_struct_attributes_are_assigned(self):
        struct = Struct()
        struct.attributes = {
            "attribute1": "type_1",
            "attribute2": "type_2",
        }

        instance = Instance(name="instance")
        instance.attributes = {
            "attribute1": "value_1",
            "attribute2": "value_2",
        }

        result = self.checker.check_if_struct_attributes_are_assigned(struct, instance)

        self.assertTrue(result)

    def test_check_if_value_matches_with_defined_type(self):
        struct = Struct()
        struct.attributes = {
            "attribute1": "type_1",
            "attribute2": "type_2",
        }

        instance = Instance(name="instance")
        instance.attributes = {
            "attribute1": "value_1",
            "attribute2": "value_2",
        }
        result = self.checker.check_if_value_matches_with_defined_type(struct, instance)

        self.assertTrue(result)

    def test_get_last_type_and_name_of_attribute_access(self):
        struct = Struct()
        struct.attributes = {
            "attribute1": "type_1",
            "nested_struct": "NestedStruct",
        }
        result = self.checker.get_last_type_and_name_of_attribute_access(
            "nested_struct.attribute2", struct
        )

        self.assertEqual(result, ("attribute2", "NestedStruct"))

    def test_check_for_invalid_module_name(self):
        module = Module(name="module")
        result = self.checker.check_for_invalid_module_name(module)
        self.assertTrue(result)

        module = Module(name="Invalid")
        result = self.checker.check_for_invalid_module_name(module)
        self.assertFalse(result)

    def test_check_rules(self):
        rule = Rule(name="rule")

        self.checker.mfdl_program.rules["rule"] = rule
        result = self.checker.check_rules()
        self.assertTrue(result)

    def test_check_for_invalid_rule_name(self):
        rule = Rule(name="rule")
        result = self.checker.check_for_invalid_rule_name(rule)
        self.assertTrue(result)

        rule = Rule(name="Invalid")
        result = self.checker.check_for_invalid_rule_name(rule)
        self.assertFalse(result)

    def test_check_for_invalid_task_name(self):
        task = Task(name="task")

        result = self.checker.check_for_invalid_task_name(task)
        self.assertTrue(result)

        task = Task(name="Invalid")
        result = self.checker.check_for_invalid_task_name(task)
        self.assertFalse(result)

    def test_check_task_statements(self):
        task = Task(name="task")
        task.statements = [TransportOrder()]

        with patch.object(self.checker, "check_transport_order", return_value=True):
            result = self.checker.check_task_statements(task)
            self.assertTrue(result)

        with patch.object(self.checker, "check_transport_order", return_value=False):
            result = self.checker.check_task_statements(task)
            self.assertFalse(result)

        task.statements = [TransportOrder(), MoveOrder()]

        with patch.object(self.checker, "check_transport_order", return_value=True):
            with patch.object(self.checker, "check_move_order", return_value=True):
                result = self.checker.check_task_statements(task)
                self.assertTrue(result)

        with patch.object(self.checker, "check_transport_order", return_value=True):
            with patch.object(self.checker, "check_move_order", return_value=False):
                result = self.checker.check_task_statements(task)
                self.assertFalse(result)

        task.statements = [TransportOrder(), ActionOrder()]

        with patch.object(self.checker, "check_transport_order", return_value=True):
            with patch.object(self.checker, "check_action_order", return_value=True):
                result = self.checker.check_task_statements(task)
                self.assertTrue(result)

        with patch.object(self.checker, "check_transport_order", return_value=True):
            with patch.object(self.checker, "check_action_order", return_value=False):
                result = self.checker.check_task_statements(task)
                self.assertFalse(result)

        task.statements = [ActionOrder()]

        with patch.object(self.checker, "check_transport_order", return_value=True):
            with patch.object(self.checker, "check_action_order", return_value=True):
                result = self.checker.check_task_statements(task)
                self.assertFalse(result)

    def test_check_transport_order_steps(self):
        tos_dict = {
            "tos_1": TransportOrderStep("tos_1"),
            "tos_2": TransportOrderStep("tos_2"),
            "tos_3": TransportOrderStep("tos_3"),
        }

        self.checker.check_for_invalid_tos_name = MagicMock(return_value=True)
        self.checker.check_location = MagicMock(return_value=True)
        self.checker.check_on_done = MagicMock(return_value=True)
        self.checker.check_started_by = MagicMock(return_value=True)
        self.checker.check_finished_by = MagicMock(return_value=True)

        self.mfdl_program.transportordersteps = tos_dict

        result = self.checker.check_transport_order_steps()
        self.assertTrue(result)

        self.checker.check_for_invalid_tos_name.assert_called_with(tos_dict["tos_3"])
        self.checker.check_location.assert_called_with(tos_dict["tos_3"])
        self.checker.check_on_done.assert_called_with(tos_dict["tos_3"])
        self.checker.check_started_by.assert_called_with(tos_dict["tos_3"])
        self.checker.check_finished_by.assert_called_with(tos_dict["tos_3"])

    def test_check_transport_order(self):
        task = Task()
        transport_order = TransportOrder()
        transport_order.pickup_tos_names = ["tos_1", "tos_2"]
        transport_order.delivery_tos_name = "tos_3"

        self.checker.check_for_invalid_tos_name = MagicMock(side_effect=[True, False, True])
        self.checker.check_location = MagicMock(side_effect=[True, False, True])
        self.checker.check_on_done = MagicMock(side_effect=[True, False, True])

        result = self.checker.check_transport_order(task, transport_order)

        self.assertFalse(result)

    def test_check_move_order(self):
        task = Task()
        move_order = MoveOrder()
        move_order.moveorderstep_name = "mos1"

        self.mos_exists = MagicMock(side_effect=[True, False, True])

        error_handler_mock = MagicMock()
        self.error_handler = error_handler_mock
        error_handler_mock.print_error = MagicMock()

        result = self.checker.check_move_order(task, move_order)

        self.assertFalse(result)

    def test_check_on_done(self):
        task = Task()
        tos = TransportOrderStep()
        aos = ActionOrderStep()
        self.checker.task_exists = MagicMock(side_effect=[True, False, True])

        task.follow_up_task_names = ["task_1"]
        tos.follow_up_task_names = ["task_2"]
        aos.follow_up_task_names = ["task_3"]

        result_task = self.checker.check_on_done(task)
        self.assertTrue(result_task)

        result_tos = self.checker.check_on_done(tos)
        self.assertFalse(result_tos)

        result_aos = self.checker.check_on_done(aos)
        self.assertTrue(result_aos)

    def test_check_rule_call_with_parameters(self):
        rule_call = ("rule", {"param1": "value_1", "param2": "value_2"})

        rule = Rule()
        rule.parameters = {"param1": None, "param2": None}

        self.checker.mfdl_program.rules["rule"] = rule

        result = self.checker.check_rule_call(rule_call)

        self.assertTrue(result)

    def test_get_last_type_and_name_of_attribute_access(self):
        my_struct = Struct("MyStruct")
        my_struct_2 = Struct("MyStruct2")

        my_struct.attributes = {
            "attr1": "MyStruct2",
        }
        my_struct_2.attributes = {"attr2": "string"}

        self.mfdl_program.structs = {"MyStruct": my_struct, "MyStruct2": my_struct_2}

        attr_list = "attr1.attr2"
        result = self.checker.get_last_type_and_name_of_attribute_access(attr_list, my_struct)

        self.assertEqual(result, ("attr2", "string"))

    def test_variable_type_exists(self):
        struct_1 = Struct("Struct1")
        struct_2 = Struct("Struct2")
        self.mfdl_program.structs = {"Struct1": struct_1, "Struct2": struct_2}

        self.assertTrue(self.checker.variable_type_exists("Struct1"))
        self.assertTrue(self.checker.variable_type_exists("Struct2"))
        self.assertTrue(self.checker.variable_type_exists("number"))
        self.assertTrue(self.checker.variable_type_exists("string"))
        self.assertTrue(self.checker.variable_type_exists("boolean"))

        self.assertFalse(self.checker.variable_type_exists("Struct3"))
        self.assertFalse(self.checker.variable_type_exists("float"))
        self.assertFalse(self.checker.variable_type_exists("list"))

    def test_check_expression(self):
        struct = Struct(
            "struct",
            {"attribute": "number", "attribute_2": "string", "attribute_3": "boolean"},
        )
        task = Task()
        task.variables = {"variable": "struct"}
        self.checker.structs = {"struct": struct}

        self.assertTrue(self.checker.check_expression(5, None))
        self.assertTrue(self.checker.check_expression(5.0, None))
        self.assertTrue(self.checker.check_expression(True, None))
        self.assertTrue(self.checker.check_expression(False, None))
        self.assertTrue(self.checker.check_expression({"left": 5, "binOp": "<", "right": 10}, None))
        self.assertTrue(self.checker.check_expression({"left": 5, "binOp": ">", "right": 10}, None))
        self.assertTrue(
            self.checker.check_expression({"left": 5, "binOp": "<=", "right": 10}, None)
        )
        self.assertTrue(
            self.checker.check_expression({"left": 5, "binOp": ">=", "right": 10}, None)
        )
        self.assertTrue(
            self.checker.check_expression({"left": True, "binOp": "==", "right": True}, None)
        )
        self.assertTrue(
            self.checker.check_expression({"left": True, "binOp": "==", "right": 1}, None)
        )
        self.assertTrue(
            self.checker.check_expression({"left": True, "binOp": "And", "right": False}, None)
        )
        self.assertFalse(
            self.checker.check_expression({"left": 5, "binOp": "<", "right": "a_string"}, None)
        )

    def test_check_single_expression(self):
        struct = Struct()
        struct.name = "struct"
        struct.attributes = {
            "attribute": "number",
            "attribute_2": "string",
            "attribute_3": "boolean",
        }
        task = Task()
        task.variables = {"variable": "struct"}
        self.checker.structs = {"struct": struct}

        self.assertTrue(self.checker.check_single_expression(5, None))
        self.assertTrue(self.checker.check_single_expression(5.0, None))
        self.assertTrue(self.checker.check_single_expression(True, None))
        self.assertTrue(self.checker.check_single_expression(False, None))

    def test_check_unary_operation(self):
        struct = Struct()
        struct.name = "struct"
        struct.attributes = {
            "attribute": "number",
            "attribute_2": "string",
            "attribute_3": "boolean",
        }
        task = Task()
        task.variables = {"variable": "struct"}
        self.checker.structs = {"struct": struct}

        expression = {"left": "(", "binOp": "true", "right": ")"}
        self.assertFalse(
            self.checker.check_unary_operation({"unop": "!", "value": expression}, None)
        )
        expression = {
            "left": "(",
            "binOp": {"left": 5, "binOp": "<", "right": 10},
            "right": ")",
        }
        self.assertTrue(
            self.checker.check_unary_operation({"unop": "!", "value": expression}, None)
        )

    def test_check_binary_operation(self):
        struct = Struct()
        struct.name = "struct"
        struct.attributes = {
            "attribute": "number",
            "attribute_2": "string",
            "attribute_3": "boolean",
        }
        task = Task()
        task.variables = {"variable": "struct"}
        self.checker.structs = {"struct": struct}
        self.assertTrue(
            self.checker.check_binary_operation({"left": 5, "binOp": "<", "right": 10}, None)
        )
        self.assertTrue(
            self.checker.check_binary_operation({"left": 5, "binOp": ">", "right": 10}, None)
        )
        self.assertTrue(
            self.checker.check_binary_operation({"left": 5, "binOp": "<=", "right": 10}, None)
        )
        self.assertTrue(
            self.checker.check_binary_operation({"left": 5, "binOp": ">=", "right": 10}, None)
        )
        self.assertTrue(
            self.checker.check_binary_operation({"left": 5, "binOp": "+", "right": 10}, None)
        )
        self.assertTrue(
            self.checker.check_binary_operation({"left": 5, "binOp": "-", "right": 10}, None)
        )
        self.assertTrue(
            self.checker.check_binary_operation({"left": 5, "binOp": "/", "right": 10}, None)
        )
        self.assertTrue(
            self.checker.check_binary_operation({"left": 5, "binOp": "*", "right": 10}, None)
        )
        self.assertTrue(
            self.checker.check_binary_operation({"left": True, "binOp": "==", "right": True}, None)
        )
        self.assertTrue(
            self.checker.check_binary_operation({"left": True, "binOp": "==", "right": 1}, None)
        )

        self.assertTrue(
            self.checker.check_binary_operation(
                {"left": True, "binOp": "And", "right": False}, None
            )
        )

        self.assertFalse(
            self.checker.check_binary_operation(
                {"left": 5, "binOp": "+", "right": "a_string"}, None
            )
        )
        self.assertFalse(
            self.checker.check_binary_operation(
                {"left": "a_string", "binOp": "+", "right": "5"}, None
            )
        )
        self.assertFalse(
            self.checker.check_binary_operation(
                {"left": 5, "binOp": "-", "right": "a_string"}, None
            )
        )
        self.assertFalse(
            self.checker.check_binary_operation(
                {"left": 5, "binOp": "/", "right": "a_string"}, None
            )
        )
        self.assertFalse(
            self.checker.check_binary_operation(
                {"left": 5, "binOp": "*", "right": "a_string"}, None
            )
        )
        self.assertFalse(
            self.checker.check_binary_operation(
                {"left": 5, "binOp": "<", "right": "a_string"}, None
            )
        )

    def test_expression_is_number(self):
        struct = Struct()
        struct.name = "struct"
        struct.attributes = {"attribute": "number", "attribute_2": "string"}
        task = Task()
        task.variables = {"variable": "struct"}
        self.checker.structs = {"struct": struct}

        self.assertTrue(self.checker.expression_is_number(5, task))
        self.assertTrue(self.checker.expression_is_number(10.0, task))
        self.assertTrue(self.checker.expression_is_number(True, task))

        self.assertFalse(self.checker.expression_is_number("a string", task))

        self.assertTrue(
            self.checker.expression_is_number({"left": 5, "binOp": "<", "right": 10}, task)
        )
        self.assertFalse(
            self.checker.expression_is_number(
                {"left": 5, "binOp": "<", "right": "not a number"}, task
            )
        )

        expression = {
            "left": "(",
            "binOp": {"left": 5, "binOp": "<", "right": 10},
            "right": ")",
        }
        self.assertTrue(self.checker.expression_is_number(expression, task))

        expression = {
            "left": "(",
            "binOp": {"left": 5, "binOp": "<", "right": "not a number"},
            "right": ")",
        }
        self.assertFalse(self.checker.expression_is_number(expression, task))

    def test_expression_is_string(self):
        struct = Struct()
        struct.name = "struct"
        struct.attributes = {"attribute": "string", "attribute_2": "number"}
        task = Task()
        task.variables = {"variable": "struct"}
        self.checker.structs = {"struct": struct}

        self.assertTrue(self.checker.expression_is_string("a string", None))
        self.assertTrue(self.checker.expression_is_string("6", None))
        self.assertTrue(self.checker.expression_is_string("True", None))

        self.assertFalse(self.checker.expression_is_string(5, None))
        self.assertFalse(self.checker.expression_is_string(True, None))


if __name__ == "__main__":
    unittest.main()
