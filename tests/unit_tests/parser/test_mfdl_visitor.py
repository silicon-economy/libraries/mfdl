# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the MFDLVisitor.

The TestMFDLVisitor class contains unit tests which tests each method of the
MFDLVisitor class. A syntax tree with every language component in it is build up
from a MFDL program in the setUp method for testing the Visitor methods.
"""

# standard libraries
from typing import Dict, List
import unittest
from unittest.mock import MagicMock, Mock
from unittest.mock import patch

# 3rd party
from antlr4.Token import Token
from antlr4 import ParserRuleContext


# local sources
from mfdl_scheduler.model.struct import Struct
from mfdl_scheduler.model.instance import Instance
from mfdl_scheduler.model.module import Module
from mfdl_scheduler.model.rule import Rule
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.moveorder import MoveOrder
from mfdl_scheduler.model.actionorder import ActionOrder
from mfdl_scheduler.model.transportorderstep import TransportOrderStep
from mfdl_scheduler.model.actionorderstep import ActionOrderStep

from mfdl_scheduler.parser.MFDLParser import MFDLParser
from mfdl_scheduler.parser.mfdl_visitor import MFDLVisitor

from mfdl_scheduler.validation.error_handler import ErrorHandler


class TestMFDLVisitor(unittest.TestCase):
    """Testcase containing unit tests for the MFDLVisitor.

    Attributes:
        mfdl_visitor: A MFDLVisitor object which contains the methods who should be tested.
    """

    def setUp(self) -> None:
        self.mfdl_visitor = MFDLVisitor("", error_handler=ErrorHandler(""))

    def test_visitProgram(self):
        """Checks if a MFDLProgram object is returned and
        all components are in the different dicts."""
        mfdl_program = None

        program_context = MFDLParser.ProgramContext(None)
        program_context.children = [
            MFDLParser.StructContext(None),
            MFDLParser.StructContext(None),
            MFDLParser.StructContext(None),
        ]
        with patch.object(
            MFDLVisitor,
            "visitStruct",
            MagicMock(
                side_effect=[
                    Struct(name="struct_1"),
                    Struct(name="struct_1"),
                    Struct(name="struct_2"),
                ]
            ),
        ):
            mfdl_program = self.mfdl_visitor.visitProgram(program_context)

        self.assertIsNotNone(mfdl_program)
        self.assertEqual(len(mfdl_program.structs), 2)

        program_context.children = [
            MFDLParser.InstanceContext(None),
            MFDLParser.InstanceContext(None),
            MFDLParser.InstanceContext(None),
        ]

        with patch.object(
            MFDLVisitor,
            "visitInstance",
            MagicMock(
                side_effect=[
                    Instance(name="instance_1"),
                    Instance(name="instance_1"),
                    Instance(name="instance_2"),
                ]
            ),
        ):
            mfdl_program = self.mfdl_visitor.visitProgram(program_context)

        self.assertIsNotNone(mfdl_program)
        self.assertEqual(len(mfdl_program.instances), 2)

        program_context.children = [
            MFDLParser.ModuleContext(None),
            MFDLParser.ModuleContext(None),
            MFDLParser.ModuleContext(None),
        ]

        with patch.object(
            MFDLVisitor,
            "visitModule",
            MagicMock(
                side_effect=[
                    Module(name="module_1"),
                    Module(name="module_1"),
                    Module(name="module_2"),
                ]
            ),
        ):
            mfdl_program = self.mfdl_visitor.visitProgram(program_context)

        self.assertIsNotNone(mfdl_program)
        self.assertEqual(len(mfdl_program.modules), 2)

        program_context.children = [
            MFDLParser.Rule_Context(None),
            MFDLParser.Rule_Context(None),
            MFDLParser.Rule_Context(None),
        ]

        with patch.object(
            MFDLVisitor,
            "visitRule_",
            MagicMock(
                side_effect=[
                    Rule(name="rule_1"),
                    Rule(name="rule_1"),
                    Rule(name="rule_2"),
                ]
            ),
        ):
            mfdl_program = self.mfdl_visitor.visitProgram(program_context)

        self.assertIsNotNone(mfdl_program)
        self.assertEqual(len(mfdl_program.rules), 2)

        program_context.children = [
            MFDLParser.TaskContext(None),
            MFDLParser.TaskContext(None),
            MFDLParser.TaskContext(None),
        ]

        with patch.object(
            MFDLVisitor,
            "visitTask",
            MagicMock(
                side_effect=[
                    Task(name="task_1"),
                    Task(name="task_1"),
                    Task(name="task_2"),
                ]
            ),
        ):
            mfdl_program = self.mfdl_visitor.visitProgram(program_context)

        self.assertIsNotNone(mfdl_program)
        self.assertEqual(len(mfdl_program.tasks), 2)

        program_context.children = [
            MFDLParser.TransportOrderStepContext(None),
            MFDLParser.TransportOrderStepContext(None),
            MFDLParser.TransportOrderStepContext(None),
        ]

        with patch.object(
            MFDLVisitor,
            "visitTransportOrderStep",
            MagicMock(
                side_effect=[
                    TransportOrderStep(name="tos_1"),
                    TransportOrderStep(name="tos_1"),
                    TransportOrderStep(name="tos_2"),
                ]
            ),
        ):
            mfdl_program = self.mfdl_visitor.visitProgram(program_context)

        self.assertIsNotNone(mfdl_program)
        self.assertEqual(len(mfdl_program.transportordersteps), 2)

        program_context.children = [
            MFDLParser.ActionOrderStepContext(None),
            MFDLParser.ActionOrderStepContext(None),
            MFDLParser.ActionOrderStepContext(None),
        ]

        with patch.object(
            MFDLVisitor,
            "visitActionOrderStep",
            MagicMock(
                side_effect=[
                    ActionOrderStep(name="aos_1"),
                    ActionOrderStep(name="aos_1"),
                    ActionOrderStep(name="aos_2"),
                ]
            ),
        ):
            mfdl_program = self.mfdl_visitor.visitProgram(program_context)

    def test_visitImport_stmt(self):
        pass

    def test_visitStruct(self):
        struct_context = MFDLParser.StructContext(None)
        struct_context.children = [
            MFDLParser.Struct_idContext(None),
        ]
        with patch.object(
            MFDLVisitor,
            "visitStruct_id",
            MagicMock(side_effect=["struct_id"]),
        ):
            struct = self.mfdl_visitor.visitStruct(struct_context)

        self.assertIsNotNone(struct)
        self.assertEqual(struct.name, "struct_id")
        self.assertEqual(struct.parent_struct_name, "")

        struct_context = MFDLParser.StructContext(None)
        struct_context.children = [
            MFDLParser.Struct_idContext(None),
            MFDLParser.Struct_idContext(None),
            MFDLParser.Attribute_definitionContext(None),
        ]
        with patch.object(
            MFDLVisitor,
            "visitStruct_id",
            MagicMock(side_effect=["struct_id", "parent_struct"]),
        ):
            with patch.object(
                MFDLVisitor,
                "visitAttribute_definition",
                MagicMock(side_effect=[("attr", "type")]),
            ):
                struct = self.mfdl_visitor.visitStruct(struct_context)

        self.assertIsNotNone(struct)
        self.assertEqual(struct.name, "struct_id")
        self.assertEqual(struct.parent_struct_name, "parent_struct")

        # 3, because id and time are the default attributes
        self.assertEqual(len(struct.attributes), 3)
        self.assertTrue("attr" in struct.attributes)
        self.assertEqual(struct.attributes["attr"], "type")

    def test_visitStruct_id(self):
        struct_id_context = MFDLParser.Struct_idContext(None)
        create_and_add_token(MFDLParser.LOCATION, "Location", struct_id_context)
        struct_id = self.mfdl_visitor.visitStruct_id(struct_id_context)
        self.assertEqual(struct_id, "Location")

        struct_id_context = MFDLParser.Struct_idContext(None)
        create_and_add_token(MFDLParser.EVENT, "Event", struct_id_context)
        struct_id = self.mfdl_visitor.visitStruct_id(struct_id_context)
        self.assertEqual(struct_id, "Event")

        struct_id_context = MFDLParser.Struct_idContext(None)
        create_and_add_token(MFDLParser.TIME, "Time", struct_id_context)
        struct_id = self.mfdl_visitor.visitStruct_id(struct_id_context)
        self.assertEqual(struct_id, "Time")

        struct_id_context = MFDLParser.Struct_idContext(None)
        create_and_add_token(MFDLParser.CONSTRAINT, "Constraint", struct_id_context)
        struct_id = self.mfdl_visitor.visitStruct_id(struct_id_context)
        self.assertEqual(struct_id, "Constraint")

        struct_id_context = MFDLParser.Struct_idContext(None)
        create_and_add_token(MFDLParser.ID, "struct_1", struct_id_context)
        struct_id = self.mfdl_visitor.visitStruct_id(struct_id_context)
        self.assertEqual(struct_id, "struct_1")

    def test_visitInstance(self):
        instance_context = MFDLParser.InstanceContext(None)

        instance_context.children = [
            MFDLParser.Struct_idContext(None),
            MFDLParser.Attribute_assignmentContext(None),
            MFDLParser.Attribute_assignmentContext(None),
        ]
        create_and_add_token(MFDLParser.ID, "instance_id", instance_context)
        with patch.object(
            MFDLVisitor,
            "visitStruct_id",
            MagicMock(side_effect=["struct_id"]),
        ):
            with patch.object(
                MFDLVisitor,
                "visitAttribute_assignment",
                MagicMock(side_effect=[("attr", "value"), ("attr_2", {"id": "value"})]),
            ):
                instance = self.mfdl_visitor.visitInstance(instance_context)

        self.assertIsNotNone(instance)
        self.assertEqual(instance.name, "instance_id")
        self.assertEqual(instance.struct_name, "struct_id")
        self.assertEqual(len(instance.attributes), 4)  # id and time are set as default

    def test_visitRule_(self):
        rule_context = MFDLParser.Rule_Context(None)
        rule_context.children = [
            MFDLParser.Rule_callContext(None),
            MFDLParser.ExpressionContext(None),
            MFDLParser.ExpressionContext(None),
        ]

        rule_call = ("rule_name", {"arg_1": "5", "arg_2": "str"})
        expression_1 = {"unOp": "!", "value": "event.a_bool"}
        expression_2 = {"left": "event.an_int", "binOp": "==", "right": 10}

        with patch.object(
            MFDLVisitor,
            "visitRule_call",
            MagicMock(side_effect=[rule_call]),
        ):
            with patch.object(
                MFDLVisitor,
                "visitExpression",
                MagicMock(side_effect=[expression_1, expression_2]),
            ):
                rule = self.mfdl_visitor.visitRule_(rule_context)

        self.assertEqual(rule.name, "rule_name")
        self.assertEqual(len(rule.parameters), 2)
        self.assertTrue("arg_1" in rule.parameters)
        self.assertTrue("arg_2" in rule.parameters)
        self.assertEqual(rule.parameters["arg_1"], "5")
        self.assertEqual(rule.parameters["arg_2"], "str")
        self.assertEqual(len(rule.expressions), 2)
        self.assertEqual(len(rule.expressions[0]), 2)
        self.assertEqual(len(rule.expressions[1]), 3)
        self.assertEqual(rule.expressions[0]["unOp"], "!")
        self.assertEqual(rule.expressions[0]["value"], "event.a_bool")
        self.assertEqual(rule.expressions[1]["left"], "event.an_int")
        self.assertEqual(rule.expressions[1]["binOp"], "==")
        self.assertEqual(rule.expressions[1]["right"], 10)

    def test_visitRule_call(self):
        rule_call_context = MFDLParser.Rule_callContext(None)

        rule_call_context.children = [MFDLParser.Rule_parameterContext(None)]
        create_and_add_token(MFDLParser.ID, "rule_id", rule_call_context)

        with patch.object(
            MFDLVisitor,
            "visitRule_parameter",
            MagicMock(side_effect=[("arg", "False")]),
        ):
            rule_id, parameters = self.mfdl_visitor.visitRule_call(rule_call_context)

        self.assertEqual(rule_id, "rule_id")
        self.assertEqual(len(parameters), 1)
        self.assertTrue("arg" in parameters)
        self.assertEqual(parameters["arg"], "False")

        # multiple parameters
        rule_call_context.children = [
            MFDLParser.Rule_parameterContext(None),
            MFDLParser.Rule_parameterContext(None),
            MFDLParser.Rule_parameterContext(None),
        ]
        create_and_add_token(MFDLParser.ID, "rule_id", rule_call_context)

        with patch.object(
            MFDLVisitor,
            "visitRule_parameter",
            MagicMock(side_effect=[("arg", "5"), ("bool", "True"), ("float", "5.0")]),
        ):
            rule_id, parameters = self.mfdl_visitor.visitRule_call(rule_call_context)

        self.assertEqual(rule_id, "rule_id")
        self.assertEqual(len(parameters), 3)
        self.assertTrue("arg" in parameters)
        self.assertEqual(parameters["arg"], "5")
        self.assertTrue("bool" in parameters)
        self.assertEqual(parameters["bool"], "True")
        self.assertTrue("float" in parameters)
        self.assertEqual(parameters["float"], "5.0")

    def test_visitRule_parameter(self):
        rule_parameter_context = MFDLParser.Rule_parameterContext(None)
        rule_parameter_context.children = [MFDLParser.ValueContext(None)]

        arg = value = ""
        # used as rule definition parameters
        with patch.object(
            MFDLVisitor,
            "visitValue",
            MagicMock(side_effect=["arg"]),
        ):
            arg, value = self.mfdl_visitor.visitRule_parameter(rule_parameter_context)

        self.assertEqual(arg, "arg")
        self.assertEqual(value, None)

        arg = value = ""

        rule_parameter_context = MFDLParser.Rule_parameterContext(None)
        rule_parameter_context.children = [MFDLParser.ValueContext(None)]
        create_and_add_token(MFDLParser.EQUAL, "=", rule_parameter_context)
        rule_parameter_context.children.append(MFDLParser.ValueContext(None))

        with patch.object(
            MFDLVisitor,
            "visitValue",
            MagicMock(side_effect=["arg", 5]),
        ):
            arg, value = self.mfdl_visitor.visitRule_parameter(rule_parameter_context)

        self.assertEqual(arg, "arg")
        self.assertEqual(value, 5)

        arg = value = ""
        # used as parameters in a rule call
        rule_parameter_context = MFDLParser.Rule_parameterContext(None)
        rule_parameter_context.children = [MFDLParser.ValueContext(None)]
        create_and_add_token(MFDLParser.EQUAL, "=", rule_parameter_context)
        rule_parameter_context.children.append(MFDLParser.ValueContext(None))

        with patch.object(
            MFDLVisitor,
            "visitValue",
            MagicMock(side_effect=["True", 5.0]),
        ):
            arg, value = self.mfdl_visitor.visitRule_parameter(rule_parameter_context)

        self.assertEqual(arg, "True")
        self.assertEqual(value, 5.0)

    def test_visitModule(self):
        module = None
        module_context = MFDLParser.ModuleContext(None)
        module_context.children = [
            MFDLParser.StructContext(None),
            MFDLParser.StructContext(None),
            MFDLParser.StructContext(None),
        ]
        create_and_add_token(MFDLParser.ID, "module_name", module_context)

        with patch.object(
            MFDLVisitor,
            "visitStruct",
            MagicMock(
                side_effect=[
                    Struct(name="struct_1"),
                    Struct(name="struct_1"),
                    Struct(name="struct_2"),
                ]
            ),
        ):
            module = self.mfdl_visitor.visitModule(module_context)

        self.assertIsNotNone(module)
        self.assertEqual(module.name, "module_name")
        self.assertEqual(len(module.structs), 2)

        module_context.children = [
            MFDLParser.InstanceContext(None),
            MFDLParser.InstanceContext(None),
            MFDLParser.InstanceContext(None),
        ]
        create_and_add_token(MFDLParser.ID, "module_name", module_context)

        with patch.object(
            MFDLVisitor,
            "visitInstance",
            MagicMock(
                side_effect=[
                    Instance(name="instance_1"),
                    Instance(name="instance_1"),
                    Instance(name="instance_2"),
                ]
            ),
        ):
            module = self.mfdl_visitor.visitModule(module_context)

        self.assertIsNotNone(module)
        self.assertEqual(len(module.instances), 2)

        module_context.children = [
            MFDLParser.Rule_Context(None),
            MFDLParser.Rule_Context(None),
            MFDLParser.Rule_Context(None),
        ]
        create_and_add_token(MFDLParser.ID, "module_name", module_context)

        with patch.object(
            MFDLVisitor,
            "visitRule_",
            MagicMock(
                side_effect=[
                    Rule(name="rule_1"),
                    Rule(name="rule_1"),
                    Rule(name="rule_2"),
                ]
            ),
        ):
            module = self.mfdl_visitor.visitModule(module_context)

        self.assertIsNotNone(module)
        self.assertEqual(len(module.rules), 2)

        module_context.children = [
            MFDLParser.TaskContext(None),
            MFDLParser.TaskContext(None),
            MFDLParser.TaskContext(None),
        ]
        create_and_add_token(MFDLParser.ID, "module_name", module_context)

        with patch.object(
            MFDLVisitor,
            "visitTask",
            MagicMock(
                side_effect=[
                    Task(name="task_1"),
                    Task(name="task_1"),
                    Task(name="task_2"),
                ]
            ),
        ):
            module = self.mfdl_visitor.visitModule(module_context)

        self.assertIsNotNone(module)
        self.assertEqual(len(module.tasks), 2)

        module_context.children = [
            MFDLParser.TransportOrderStepContext(None),
            MFDLParser.TransportOrderStepContext(None),
            MFDLParser.TransportOrderStepContext(None),
        ]
        create_and_add_token(MFDLParser.ID, "module_name", module_context)

        with patch.object(
            MFDLVisitor,
            "visitTransportOrderStep",
            MagicMock(
                side_effect=[
                    TransportOrderStep(name="tos_1"),
                    TransportOrderStep(name="tos_1"),
                    TransportOrderStep(name="tos_2"),
                ]
            ),
        ):
            module = self.mfdl_visitor.visitModule(module_context)

        self.assertIsNotNone(module)
        self.assertEqual(len(module.transportordersteps), 2)

        module_context.children = [
            MFDLParser.ActionOrderStepContext(None),
            MFDLParser.ActionOrderStepContext(None),
            MFDLParser.ActionOrderStepContext(None),
        ]
        create_and_add_token(MFDLParser.ID, "module_name", module_context)

        with patch.object(
            MFDLVisitor,
            "visitActionOrderStep",
            MagicMock(
                side_effect=[
                    ActionOrderStep(name="aos_1"),
                    ActionOrderStep(name="aos_1"),
                    ActionOrderStep(name="aos_2"),
                ]
            ),
        ):
            module = self.mfdl_visitor.visitModule(module_context)

    def test_visitTask(self):
        task_context = MFDLParser.TaskContext(None)
        task_statement_context = MFDLParser.TaskStatementContext(None)
        task_context.children = [task_statement_context]

        create_and_add_token(MFDLParser.ID, "task_id", task_context)

        # check if tosstatement method is called
        with patch.object(MFDLVisitor, "visitTaskStatement", return_value=None) as mock:
            task = self.mfdl_visitor.visitTask(task_context)

        mock.assert_called_with(task_statement_context)
        self.assertIsNotNone(task)
        self.assertEqual(task.name, "task_id")

    def test_visitTaskStatement(self):
        task_stmt_context = MFDLParser.TaskStatementContext(None)
        current_task = Task()
        self.mfdl_visitor.current_program_component = current_task

        task_stmt_context.children = [MFDLParser.TransportStatementContext(None)]
        transport_order = TransportOrder()
        with patch.object(
            MFDLVisitor,
            "visitTransportStatement",
            MagicMock(side_effect=[transport_order]),
        ):
            self.mfdl_visitor.visitTaskStatement(task_stmt_context)

        self.assertEqual(len(current_task.statements), 1)
        self.assertEqual(current_task.statements[0], transport_order)

        task_stmt_context.children = [MFDLParser.MoveStatementContext(None)]
        move_order = MoveOrder()
        with patch.object(
            MFDLVisitor,
            "visitMoveStatement",
            MagicMock(side_effect=[move_order]),
        ):
            self.mfdl_visitor.visitTaskStatement(task_stmt_context)

        self.assertEqual(len(current_task.statements), 2)
        self.assertEqual(current_task.statements[1], move_order)

        task_stmt_context.children = [MFDLParser.ActionStatementContext(None)]
        action_order = ActionOrder()
        with patch.object(
            MFDLVisitor,
            "visitActionStatement",
            MagicMock(side_effect=[action_order]),
        ):
            self.mfdl_visitor.visitTaskStatement(task_stmt_context)

        self.assertEqual(len(current_task.statements), 3)
        self.assertEqual(current_task.statements[2], action_order)

        opt_statement_context = MFDLParser.OptStatementContext(None)
        task_stmt_context.children = [opt_statement_context]
        with patch.object(MFDLVisitor, "visitOptStatement", return_value=None) as mock:
            self.mfdl_visitor.visitTaskStatement(task_stmt_context)

        mock.assert_called_with(opt_statement_context)

    def test_visitTransportStatement(self):
        transport_stmt_context = MFDLParser.TransportStatementContext(None)
        transport_stmt_context.children = [
            MFDLParser.FromStatementContext(None),
            MFDLParser.ToStatementContext(None),
        ]

        pickup_tos = ["tos_1", "tos_2"]
        with patch.object(
            MFDLVisitor,
            "visitFromStatement",
            MagicMock(side_effect=[pickup_tos]),
        ):
            with patch.object(
                MFDLVisitor,
                "visitToStatement",
                MagicMock(side_effect=["tos_3"]),
            ):
                transport_order = self.mfdl_visitor.visitTransportStatement(transport_stmt_context)
        self.assertEqual(transport_order.pickup_tos_names, pickup_tos)
        self.assertEqual(transport_order.delivery_tos_name, "tos_3")

    def test_visitFromStatement(self):
        from_stmt_context = MFDLParser.FromStatementContext(None)
        create_and_add_token(MFDLParser.ID, "tos_1", from_stmt_context)
        create_and_add_token(MFDLParser.ID, "tos_2", from_stmt_context)
        create_and_add_token(MFDLParser.ID, "tos_3", from_stmt_context)
        tos_names = self.mfdl_visitor.visitFromStatement(from_stmt_context)
        self.assertEqual(len(tos_names), 3)
        self.assertEqual(tos_names[0], "tos_1")
        self.assertEqual(tos_names[1], "tos_2")
        self.assertEqual(tos_names[2], "tos_3")

    def test_visitToStatement(self):
        to_stmt_context = MFDLParser.ToStatementContext(None)
        create_and_add_token(MFDLParser.ID, "tos_id", to_stmt_context)
        self.assertEqual(self.mfdl_visitor.visitToStatement(to_stmt_context), "tos_id")

    def test_visitMoveStatement(self):
        move_stmt_context = MFDLParser.MoveStatementContext(None)
        create_and_add_token(MFDLParser.ID, "tos_id", move_stmt_context)
        move_order = self.mfdl_visitor.visitMoveStatement(move_stmt_context)
        self.assertEqual(move_order.moveorderstep_name, "tos_id")

    def test_visitActionStatement(self):
        action_stmt_context = MFDLParser.ActionStatementContext(None)
        create_and_add_token(MFDLParser.ID, "aos_id", action_stmt_context)
        action_order = self.mfdl_visitor.visitActionStatement(action_stmt_context)
        self.assertEqual(action_order.actionorderstepname, "aos_id")

    def test_visitConstraintStatement(self):
        constraint_stmt_context = MFDLParser.ConstraintStatementContext(None)
        constraint_stmt_context.children = [MFDLParser.ExpressionContext(None)]

        current_task = Task()
        self.mfdl_visitor.current_program_component = current_task

        expression = {"unOp": "!", "value": "True"}
        json_string = '{ "name":"John", "age":30, "city":"New York"}'

        with patch.object(
            MFDLVisitor,
            "visitExpression",
            MagicMock(side_effect=[expression]),
        ):
            constraint, constraint_string = self.mfdl_visitor.visitConstraintStatement(
                constraint_stmt_context
            )

        self.assertEqual(constraint, expression)

        constraint_stmt_context.children = [MFDLParser.Json_objectContext(None)]

        with patch.object(
            MFDLVisitor,
            "visitJson_object",
            MagicMock(side_effect=[json_string]),
        ):
            constraint, constraint_string = self.mfdl_visitor.visitConstraintStatement(
                constraint_stmt_context
            )

        self.assertEqual(constraint, json_string)

    def test_visitRepeatStatement(self):
        repeat_stmt_context = MFDLParser.RepeatStatementContext(None)
        create_and_add_token(MFDLParser.INTEGER, "5", repeat_stmt_context)
        repeat = self.mfdl_visitor.visitRepeatStatement(repeat_stmt_context)
        self.assertEqual(repeat, "5")

    def test_visitOrderStep(self):
        order_step_context = MFDLParser.OrderStepContext(None)
        order_step_context.children = [MFDLParser.TransportOrderStepContext(None)]
        with patch.object(
            MFDLVisitor,
            "visitTransportOrderStep",
            MagicMock(side_effect=[TransportOrderStep(name="tos_name")]),
        ):
            transport_order_step = self.mfdl_visitor.visitOrderStep(order_step_context)

        self.assertTrue(isinstance(transport_order_step, TransportOrderStep))
        self.assertEqual(transport_order_step.name, "tos_name")

        order_step_context.children = [MFDLParser.ActionOrderStepContext(None)]
        with patch.object(
            MFDLVisitor,
            "visitActionOrderStep",
            MagicMock(side_effect=[ActionOrderStep(name="aos_name")]),
        ):
            action_order_step = self.mfdl_visitor.visitOrderStep(order_step_context)
        self.assertTrue(isinstance(action_order_step, ActionOrderStep))
        self.assertEqual(action_order_step.name, "aos_name")

    def test_visitTransportOrderStep(self):
        transport_order_step_context = MFDLParser.TransportOrderStepContext(None)

        tos_statement_context = MFDLParser.TosStatementContext(None)
        transport_order_step_context.children = [tos_statement_context]
        create_and_add_token(MFDLParser.ID, "tos_id", transport_order_step_context)

        # check if tosstatement method is called
        with patch.object(MFDLVisitor, "visitTosStatement", return_value=None) as mock:
            transport_order_step = self.mfdl_visitor.visitTransportOrderStep(
                transport_order_step_context
            )

        mock.assert_called_with(tos_statement_context)
        self.assertIsNotNone(transport_order_step)
        self.assertEqual(transport_order_step.name, "tos_id")

    def test_visitTosStatement(self):
        tos_statement_context = MFDLParser.TosStatementContext(None)
        tos_statement_context.children = [MFDLParser.LocationStatementContext(None)]

        current_tos = TransportOrderStep()
        self.mfdl_visitor.current_program_component = current_tos
        with patch.object(
            MFDLVisitor,
            "visitLocationStatement",
            MagicMock(side_effect=["location_name"]),
        ):
            self.mfdl_visitor.visitTosStatement(tos_statement_context)

        self.assertEqual(current_tos.location_name, "location_name")

        tos_statement_context.children = [MFDLParser.ParameterStatementContext(None)]
        with patch.object(
            MFDLVisitor,
            "visitParameterStatement",
            MagicMock(side_effect=["parameter"]),
        ):
            self.mfdl_visitor.visitTosStatement(tos_statement_context)

        self.assertEqual(current_tos.parameters, "parameter")

        opt_statement_context = MFDLParser.OptStatementContext(None)
        tos_statement_context.children = [opt_statement_context]
        with patch.object(MFDLVisitor, "visitOptStatement", return_value=None) as mock:
            self.mfdl_visitor.visitTosStatement(tos_statement_context)

        mock.assert_called_with(opt_statement_context)

    def test_visitLocationStatement(self):
        self.mfdl_visitor.current_program_component = TransportOrderStep()

        location_context = MFDLParser.LocationStatementContext(None)
        create_and_add_token(MFDLParser.ID, "location_id", location_context)
        location_name = self.mfdl_visitor.visitLocationStatement(location_context)
        self.assertEqual(location_name, "location_id")

        location_context = MFDLParser.LocationStatementContext(None)
        create_and_add_token(MFDLParser.ID, "_another_id", location_context)
        location_name = self.mfdl_visitor.visitLocationStatement(location_context)
        self.assertEqual(location_name, "_another_id")

    def test_visitParameterStatement(self):
        self.mfdl_visitor.current_program_component = TransportOrderStep()

        parameter_stmt_context = MFDLParser.ParameterStatementContext(None)
        parameter_stmt_context.children = [MFDLParser.ValueContext(None)]

        with patch.object(
            MFDLVisitor,
            "visitValue",
            MagicMock(side_effect=["value"]),
        ):
            parameter = self.mfdl_visitor.visitParameterStatement(parameter_stmt_context)
        self.assertEqual(parameter, "value")

        parameter_stmt_context.children = [MFDLParser.Json_objectContext(None)]
        with patch.object(
            MFDLVisitor,
            "visitJson_object",
            MagicMock(side_effect=[{"id": "test"}]),
        ):
            parameter = self.mfdl_visitor.visitParameterStatement(parameter_stmt_context)
        self.assertTrue(isinstance(parameter, Dict))

    def test_visitOptStatement(self):
        opt_statement_context = MFDLParser.OptStatementContext(None)
        # check if event statement is called

        event_context = MFDLParser.EventStatementContext(None)
        opt_statement_context.children = [event_context]

        with patch.object(MFDLVisitor, "visitEventStatement", return_value=None) as mock:
            self.mfdl_visitor.visitOptStatement(opt_statement_context)

        mock.assert_called_with(event_context)

        current_tos = TransportOrderStep()
        self.mfdl_visitor.current_program_component = current_tos
        on_done_context = MFDLParser.OnDoneStatementContext(None)
        opt_statement_context.children = [on_done_context]

        with patch.object(
            MFDLVisitor, "visitOnDoneStatement", return_value=["on_done_task_name"]
        ) as mock:
            self.mfdl_visitor.visitOptStatement(opt_statement_context)

        mock.assert_called_with(on_done_context)
        self.assertEqual(len(current_tos.follow_up_task_names), 1)
        self.assertEqual(current_tos.follow_up_task_names[0], "on_done_task_name")

    def test_visitEventStatement(self):
        event_stmt_context = MFDLParser.EventStatementContext(None)
        event_stmt_context.children = [MFDLParser.ExpressionContext(None)]
        create_and_add_token(MFDLParser.STARTED_BY, "StartedBy", event_stmt_context)

        current_tos = TransportOrderStep()
        self.mfdl_visitor.current_program_component = current_tos

        expression = {"left": "event.an_int", "binOp": "==", "right": 10}
        with patch.object(
            MFDLVisitor,
            "visitExpression",
            MagicMock(side_effect=[expression]),
        ):
            self.mfdl_visitor.visitEventStatement(event_stmt_context)

        self.assertIsNotNone(current_tos.started_by_expr)
        self.assertEqual(len(current_tos.started_by_expr), 3)

        event_stmt_context.children = [MFDLParser.ExpressionContext(None)]
        create_and_add_token(MFDLParser.FINISHED_BY, "FinishedBy", event_stmt_context)
        with patch.object(
            MFDLVisitor,
            "visitExpression",
            MagicMock(side_effect=[expression]),
        ):
            self.mfdl_visitor.visitEventStatement(event_stmt_context)

        self.assertIsNotNone(current_tos.finished_by_expr)
        self.assertEqual(len(current_tos.finished_by_expr), 3)

    def test_visitOnDoneStatement(self):
        self.mfdl_visitor.current_program_component = Task()

        on_done_stmt_context = MFDLParser.OnDoneStatementContext(None)
        create_and_add_token(MFDLParser.ID, "task_id", on_done_stmt_context)
        task_name = self.mfdl_visitor.visitOnDoneStatement(on_done_stmt_context)
        self.assertEqual(task_name, ["task_id"])

        on_done_stmt_context = MFDLParser.OnDoneStatementContext(None)
        create_and_add_token(MFDLParser.ID, "_another_id", on_done_stmt_context)
        task_name = self.mfdl_visitor.visitOnDoneStatement(on_done_stmt_context)
        self.assertEqual(task_name, ["_another_id"])

    def test_visitActionOrderStep(self):
        action_order_step_context = MFDLParser.ActionOrderStepContext(None)
        aos_statement_context = MFDLParser.AosStatementContext(None)
        action_order_step_context.children = [aos_statement_context]

        create_and_add_token(MFDLParser.ID, "aos_id", action_order_step_context)

        # check if tosstatement method is called
        with patch.object(MFDLVisitor, "visitAosStatement", return_value=None) as mock:
            action_order_step = self.mfdl_visitor.visitActionOrderStep(action_order_step_context)

        mock.assert_called_with(aos_statement_context)
        self.assertIsNotNone(action_order_step)
        self.assertEqual(action_order_step.name, "aos_id")

    def test_visitAosStatement(self):
        aos_stmt_context = MFDLParser.AosStatementContext(None)

        current_aos = ActionOrderStep()
        self.mfdl_visitor.current_program_component = current_aos

        aos_stmt_context.children = [MFDLParser.ParameterStatementContext(None)]
        with patch.object(
            MFDLVisitor,
            "visitParameterStatement",
            MagicMock(side_effect=["parameter"]),
        ):
            self.mfdl_visitor.visitAosStatement(aos_stmt_context)

        self.assertEqual(current_aos.parameters, "parameter")

        opt_statement_context = MFDLParser.OptStatementContext(None)
        aos_stmt_context.children = [opt_statement_context]
        with patch.object(MFDLVisitor, "visitOptStatement", return_value=None) as mock:
            self.mfdl_visitor.visitAosStatement(aos_stmt_context)

        mock.assert_called_with(opt_statement_context)

    def test_visitAttribute_definition(self):
        attribute_definition_context = MFDLParser.Attribute_definitionContext(None)

        create_and_add_token(MFDLParser.ID, "attribute_id", attribute_definition_context)
        attribute_definition_context.children.append(MFDLParser.PrimitiveContext(None))

        attribute_id = ""
        primitive = ""

        with patch.object(
            MFDLVisitor,
            "visitPrimitive",
            MagicMock(side_effect=["Location"]),
        ):
            attribute_id, primitive = self.mfdl_visitor.visitAttribute_definition(
                attribute_definition_context
            )

        self.assertEqual(attribute_id, "attribute_id")
        self.assertEqual(primitive, "Location")

    def test_visitAttribute_assignment(self):
        attribute_assignment_ctx = MFDLParser.Attribute_assignmentContext(None)
        attribute_assignment_ctx.children = [
            MFDLParser.Attribute_accessContext(None),
            MFDLParser.ValueContext(None),
        ]

        # value is simple string
        with patch.object(
            MFDLVisitor,
            "visitAttribute_access",
            MagicMock(side_effect=[["attr_1", "attr_2", "attr_3"]]),
        ):
            with patch.object(
                MFDLVisitor,
                "visitValue",
                MagicMock(side_effect=["test"]),
            ):
                attribute_access, value = self.mfdl_visitor.visitAttribute_assignment(
                    attribute_assignment_ctx
                )

        self.assertEqual(len(attribute_access), 3)
        self.assertEqual(attribute_access[0], "attr_1")
        self.assertEqual(attribute_access[1], "attr_2")
        self.assertEqual(attribute_access[2], "attr_3")
        self.assertEqual(value, "test")

        attribute_assignment_ctx.children = [
            MFDLParser.Attribute_accessContext(None),
            MFDLParser.Json_objectContext(None),
        ]

        # value is json
        with patch.object(
            MFDLVisitor,
            "visitAttribute_access",
            MagicMock(side_effect=[["attr_1"]]),
        ):
            with patch.object(
                MFDLVisitor,
                "visitJson_object",
                MagicMock(side_effect=[{"json": "value", "id": "test"}]),
            ):
                attribute_access, value = self.mfdl_visitor.visitAttribute_assignment(
                    attribute_assignment_ctx
                )

        self.assertEqual(len(attribute_access), 1)
        self.assertEqual(attribute_access[0], "attr_1")
        self.assertTrue("json" in value)
        self.assertTrue("id" in value)
        self.assertEqual(value["json"], "value")
        self.assertEqual(value["id"], "test")

    def test_visitAttribute_access(self):
        attribute_access_context = MFDLParser.Attribute_accessContext(None)
        create_and_add_token(MFDLParser.ID, "attr_1", attribute_access_context)
        attr_list = self.mfdl_visitor.visitAttribute_access(attribute_access_context)
        self.assertEqual(attr_list, "attr_1")

        attribute_access_context = MFDLParser.Attribute_accessContext(None)
        create_and_add_token(MFDLParser.ID, "attr_1", attribute_access_context)
        create_and_add_token(MFDLParser.DOT, ".", attribute_access_context)
        create_and_add_token(MFDLParser.ID, "attr_2", attribute_access_context)
        create_and_add_token(MFDLParser.DOT, ".", attribute_access_context)
        create_and_add_token(MFDLParser.ID, "attr_3", attribute_access_context)
        attr_list = self.mfdl_visitor.visitAttribute_access(attribute_access_context)
        self.assertEqual(attr_list, "attr_1.attr_2.attr_3")

    def test_visitPrimitive(self):
        primitive_context = MFDLParser.PrimitiveContext(None)
        create_and_add_token(MFDLParser.NUMBER_P, "number", primitive_context)
        struct_id = self.mfdl_visitor.visitPrimitive(primitive_context)
        self.assertEqual(struct_id, "number")

        primitive_context = MFDLParser.PrimitiveContext(None)
        create_and_add_token(MFDLParser.STRING_P, "string", primitive_context)
        struct_id = self.mfdl_visitor.visitPrimitive(primitive_context)
        self.assertEqual(struct_id, "string")

        primitive_context = MFDLParser.PrimitiveContext(None)
        create_and_add_token(MFDLParser.BOOLEAN_P, "boolean", primitive_context)
        struct_id = self.mfdl_visitor.visitPrimitive(primitive_context)
        self.assertEqual(struct_id, "boolean")

        primitive_context = MFDLParser.PrimitiveContext(None)
        create_and_add_token(MFDLParser.LOCATION, "Location", primitive_context)
        struct_id = self.mfdl_visitor.visitPrimitive(primitive_context)
        self.assertEqual(struct_id, "Location")

        primitive_context = MFDLParser.PrimitiveContext(None)
        create_and_add_token(MFDLParser.EVENT, "Event", primitive_context)
        struct_id = self.mfdl_visitor.visitPrimitive(primitive_context)
        self.assertEqual(struct_id, "Event")

        primitive_context = MFDLParser.PrimitiveContext(None)
        create_and_add_token(MFDLParser.TIME, "Time", primitive_context)
        struct_id = self.mfdl_visitor.visitPrimitive(primitive_context)
        self.assertEqual(struct_id, "Time")

        primitive_context = MFDLParser.PrimitiveContext(None)
        create_and_add_token(MFDLParser.CONSTRAINT, "Constraint", primitive_context)
        struct_id = self.mfdl_visitor.visitPrimitive(primitive_context)
        self.assertEqual(struct_id, "Constraint")

        primitive_context = MFDLParser.PrimitiveContext(None)
        create_and_add_token(MFDLParser.ID, "an_id", primitive_context)
        struct_id = self.mfdl_visitor.visitPrimitive(primitive_context)
        self.assertEqual(struct_id, "an_id")

    def test_visitValue(self):
        value_context = MFDLParser.ValueContext(None)
        create_and_add_token(MFDLParser.TRUE, "True", value_context)
        value = self.mfdl_visitor.visitValue(value_context)
        self.assertEqual(value, True)

        value_context = MFDLParser.ValueContext(None)
        create_and_add_token(MFDLParser.FALSE, "False", value_context)
        value = self.mfdl_visitor.visitValue(value_context)
        self.assertEqual(value, False)

        value_context = MFDLParser.ValueContext(None)
        create_and_add_token(MFDLParser.INTEGER, "5", value_context)
        value = self.mfdl_visitor.visitValue(value_context)
        self.assertEqual(value, 5)

        value_context = MFDLParser.ValueContext(None)
        create_and_add_token(MFDLParser.FLOAT, "5.0", value_context)
        value = self.mfdl_visitor.visitValue(value_context)
        self.assertEqual(value, 5.0)

        value_context = MFDLParser.ValueContext(None)
        create_and_add_token(MFDLParser.STRING, '"A MFDL string."', value_context)
        value = self.mfdl_visitor.visitValue(value_context)
        self.assertEqual(value, "A MFDL string.")

        value_context = MFDLParser.ValueContext(None)
        create_and_add_token(MFDLParser.ID, "an_id", value_context)
        value = self.mfdl_visitor.visitValue(value_context)
        self.assertEqual(value, "an_id")

    def test_visitExpression(self):
        # case: a and b
        expression_context = MFDLParser.ExpressionContext(None)
        expression_context.children = []
        expression_context.children = [MFDLParser.ExpressionContext(None)]

        with patch.object(
            MFDLVisitor,
            "get_content",
            MagicMock(side_effect=["single expression"]),
        ):
            expression = self.mfdl_visitor.visitExpression(expression_context)

        self.assertEqual(expression, "single expression")

        expression_context.children = [
            MFDLParser.ExpressionContext(None),
            MFDLParser.ExpressionContext(None),
        ]

        with patch.object(
            MFDLVisitor,
            "get_content",
            MagicMock(side_effect=["!", "Unop"]),
        ):
            expression = self.mfdl_visitor.visitExpression(expression_context)

        self.assertEqual(len(expression), 2)
        self.assertTrue("unOp" in expression)
        self.assertTrue("value" in expression)
        self.assertEqual(expression["unOp"], "!")
        self.assertEqual(expression["value"], "Unop")

        expression_context.children = [
            MFDLParser.ExpressionContext(None),
            MFDLParser.ExpressionContext(None),
            MFDLParser.ExpressionContext(None),
        ]

        with patch.object(
            MFDLVisitor,
            "get_content",
            MagicMock(side_effect=["a", "And", "b"]),
        ):
            expression = self.mfdl_visitor.visitExpression(expression_context)

        self.assertTrue("left" in expression)
        self.assertTrue("binOp" in expression)
        self.assertTrue("right" in expression)
        self.assertEqual(expression["left"], "a")
        self.assertEqual(expression["binOp"], "And")
        self.assertEqual(expression["right"], "b")

    def test_visitBinOperation(self):
        bin_op_context = MFDLParser.BinOperationContext(None)
        create_and_add_token(MFDLParser.LESS_THAN, "<", bin_op_context)
        bin_op = self.mfdl_visitor.visitBinOperation(bin_op_context)
        self.assertEqual(bin_op, "<")

        bin_op_context = MFDLParser.BinOperationContext(None)
        create_and_add_token(MFDLParser.LESS_THAN_OR_EQUAL, "<=", bin_op_context)
        bin_op = self.mfdl_visitor.visitBinOperation(bin_op_context)
        self.assertEqual(bin_op, "<=")

        bin_op_context = MFDLParser.BinOperationContext(None)
        create_and_add_token(MFDLParser.GREATER_THAN, ">", bin_op_context)
        bin_op = self.mfdl_visitor.visitBinOperation(bin_op_context)
        self.assertEqual(bin_op, ">")

        bin_op_context = MFDLParser.BinOperationContext(None)
        create_and_add_token(MFDLParser.GREATER_THAN_OR_EQUAL, ">=", bin_op_context)
        bin_op = self.mfdl_visitor.visitBinOperation(bin_op_context)
        self.assertEqual(bin_op, ">=")

        bin_op_context = MFDLParser.BinOperationContext(None)
        create_and_add_token(MFDLParser.EQUAL_B, "==", bin_op_context)
        bin_op = self.mfdl_visitor.visitBinOperation(bin_op_context)
        self.assertEqual(bin_op, "==")

        bin_op_context = MFDLParser.BinOperationContext(None)
        create_and_add_token(MFDLParser.NOT_EQUAL_B, "!=", bin_op_context)
        bin_op = self.mfdl_visitor.visitBinOperation(bin_op_context)
        self.assertEqual(bin_op, "!=")

        bin_op_context = MFDLParser.BinOperationContext(None)
        create_and_add_token(MFDLParser.STAR, "*", bin_op_context)
        bin_op = self.mfdl_visitor.visitBinOperation(bin_op_context)
        self.assertEqual(bin_op, "*")

        bin_op_context = MFDLParser.BinOperationContext(None)
        create_and_add_token(MFDLParser.SLASH, "/", bin_op_context)
        bin_op = self.mfdl_visitor.visitBinOperation(bin_op_context)
        self.assertEqual(bin_op, "/")

        bin_op_context = MFDLParser.BinOperationContext(None)
        create_and_add_token(MFDLParser.MINUS, "-", bin_op_context)
        bin_op = self.mfdl_visitor.visitBinOperation(bin_op_context)
        self.assertEqual(bin_op, "-")

        bin_op_context = MFDLParser.BinOperationContext(None)
        create_and_add_token(MFDLParser.PLUS, "+", bin_op_context)
        bin_op = self.mfdl_visitor.visitBinOperation(bin_op_context)
        self.assertEqual(bin_op, "+")

    def test_visitUnOperation(self):
        un_op_context = MFDLParser.UnOperationContext(None)
        create_and_add_token(MFDLParser.BOOLEAN_NOT, "!", un_op_context)
        un_op = self.mfdl_visitor.visitBinOperation(un_op_context)
        self.assertEqual(un_op, "!")

    def test_visitJson_object(self):
        json_string = '{ "name":"John", "age":30, "city":"New York"}'
        json_object_context = MFDLParser.Json_objectContext(None)
        with patch.object(
            MFDLParser.Json_objectContext,
            "getText",
            MagicMock(side_effect=[json_string]),
        ):
            json_object = self.mfdl_visitor.visitJson_object(json_object_context)

        self.assertEqual(len(json_object), 3)
        self.assertTrue("name" in json_object)
        self.assertTrue("age" in json_object)
        self.assertTrue("city" in json_object)
        self.assertEqual(json_object["name"], "John")
        self.assertEqual(json_object["age"], 30)
        self.assertEqual(json_object["city"], "New York")


def create_and_add_token(
    token_type: int, token_text: str, antlr_context: ParserRuleContext
) -> None:
    """Helper function to create a ANTLR Token object and to add
    it to the context object in one row."""
    token = Token()
    token.type = token_type
    token.text = token_text
    antlr_context.addTokenNode(token)
