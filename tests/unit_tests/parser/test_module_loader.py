# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the ModuleLoader class."""

# standard libraries
import os
import unittest

# local sources
from mfdl_scheduler.parser.module_loader import ModuleLoader
from mfdl_scheduler.validation.error_handler import ErrorHandler


class TestModuleLoader(unittest.TestCase):
    """Testcase containing unit tests for the ModuleLoader class."""

    def setUp(self):
        self.error_handler = ErrorHandler("test.mfdl")
        self.module_loader = ModuleLoader("test.mfdl", self.error_handler)

    def test_init(self):
        self.assertEqual(self.module_loader.folder_path, "")
        self.assertEqual(self.module_loader.error_handler, self.error_handler)

        module_loader_2 = ModuleLoader("path/to/test.mfdl", self.error_handler)
        self.assertEqual(module_loader_2.folder_path, "path/to/")

    def test_load_module_file(self):
        # test loading existing file with absolute path
        with open("test_file.mfdl", "w") as f:
            f.write("module TestModule\nend TestModule")
        module_content = self.module_loader.load_module_file("test_file")
        self.assertEqual(module_content, "module TestModule\nend TestModule")

        # test loading non-existing file with absolute path
        module_content = self.module_loader.load_module_file("non_existing_file")
        self.assertIsNone(module_content)

        # test loading existing file with relative path
        with open("test_relative.mfdl", "w") as f:
            f.write("module TestModule\nend TestModule")
        module_loader_2 = ModuleLoader("path/to/test.mfdl", self.error_handler)
        module_content = module_loader_2.load_module_file("test_relative")
        self.assertEqual(module_content, "module TestModule\nend TestModule")

        # test loading non-existing file with relative path
        module_content = module_loader_2.load_module_file("non_existing_file")
        self.assertIsNone(module_content)

        # clean up
        os.remove("test_file.mfdl")
        os.remove("test_relative.mfdl")

    def test_load_modules(self):
        # test loading one valid module
        with open("test_module_1.mfdl", "w") as f:
            module_string = """
Module testModule
    Struct Test
        test: number
    End
End
            """
            f.write(module_string)
        paths_and_names = [("test_module_1", "testModule")]
        context = None
        modules = self.module_loader.load_modules(paths_and_names, context)
        self.assertEqual(len(modules), 1)
        self.assertEqual(modules[0].name, "testModule")

        module_string = """
Module testModule
    Struct Test
        test: number
    End
End

Module testModule2
    Struct Test
        test: number
    End
End
            """
        # test loading multiple valid modules from same file
        with open("test_module_2.mfdl", "w") as f:
            f.write(module_string)
        paths_and_names = ("test_module_2", "testModule"), ("test_module_2", "testModule2")
        context = None
        modules = self.module_loader.load_modules(paths_and_names, context)
        self.assertEqual(len(modules), 2)
        self.assertEqual(modules[0].name, "testModule")
        self.assertEqual(modules[1].name, "testModule2")

        # test loading multiple valid modules from different files
        paths_and_names = ("test_module_1", "testModule"), ("test_module_2", "testModule2")
        context = None
        modules = self.module_loader.load_modules(paths_and_names, context)
        self.assertEqual(len(modules), 2)
        self.assertEqual(modules[0].name, "testModule")
        self.assertEqual(modules[1].name, "testModule2")

        # test loading invalid module

        module_string = """
Module testModule
    Struct Test
        test: not_a_primitive
    End
End
        """
        with open("test_module_3.mfdl", "w") as f:
            f.write(module_string)
        paths_and_names = [("test_module_3", "TestModule")]
        context = None
        modules = self.module_loader.load_modules(paths_and_names, context)
        self.assertEqual(len(modules), 0)
        self.assertEqual(self.error_handler.total_error_count, 1)

        # clean up
        os.remove("test_module_1.mfdl")
        os.remove("test_module_2.mfdl")
        os.remove("test_module_3.mfdl")


if __name__ == "__main__":
    unittest.main()
