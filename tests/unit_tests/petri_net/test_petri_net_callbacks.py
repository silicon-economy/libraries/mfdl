# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the PetriNetCallbacks class."""

# standard libraries
import unittest
from unittest.mock import MagicMock

# local sources
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.petri_net.callbacks import PetriNetCallbacks


class TestPetriNetCallbacks(unittest.TestCase):
    """Testcase containing unit tests for the PetriNetCallbacks class."""

    def test_init(self):
        callbacks = PetriNetCallbacks()
        self.assertIsNone(callbacks.materialflow_started_callback)
        self.assertIsNone(callbacks.task_started_callback)
        self.assertIsNone(callbacks.order_started_callback)
        self.assertIsNone(callbacks.started_by_callback)
        self.assertIsNone(callbacks.waiting_for_move_callback)
        self.assertIsNone(callbacks.moved_to_location_callback)
        self.assertIsNone(callbacks.waiting_for_action_callback)
        self.assertIsNone(callbacks.action_executed_callback)
        self.assertIsNone(callbacks.order_finished_callback)
        self.assertIsNone(callbacks.orders_finished_callback)
        self.assertIsNone(callbacks.finished_by_callback)
        self.assertIsNone(callbacks.instance_updated_callback)
        self.assertIsNone(callbacks.task_finished_callback)
        self.assertIsNone(callbacks.materialflow_finished_callback)

    def test_materialflow_started_callback(self):
        callbacks = PetriNetCallbacks()
        callbacks.materialflow_started_callback = MagicMock()

        tasks = Task("task1"), Task("task2")
        callbacks.materialflow_started_callback(tasks)

        callbacks.materialflow_started_callback.assert_called_once_with(tasks)

    def test_task_started_callback(self):
        callbacks = PetriNetCallbacks()
        callbacks.task_started_callback = MagicMock()

        task = Task("task1")
        callbacks.task_started_callback(task)

        callbacks.task_started_callback.assert_called_once_with(task)

    def test_order_started_callback(self):
        callbacks = PetriNetCallbacks()
        callbacks.order_started_callback = MagicMock()

        transport_order = TransportOrder("order1")
        callbacks.order_started_callback("source", transport_order, "target")

        callbacks.order_started_callback.assert_called_once_with(
            "source", transport_order, "target"
        )

    def test_started_by_callback(self):
        callbacks = PetriNetCallbacks()
        callbacks.started_by_callback = MagicMock()

        callbacks.started_by_callback()

        callbacks.started_by_callback.assert_called_once()

    def test_waiting_for_move_callback(self):
        callbacks = PetriNetCallbacks()
        callbacks.waiting_for_move_callback = MagicMock()

        callbacks.waiting_for_move_callback()

        callbacks.waiting_for_move_callback.assert_called_once()

    def test_moved_to_location_callback(self):
        callbacks = PetriNetCallbacks()
        callbacks.moved_to_location_callback = MagicMock()

        callbacks.moved_to_location_callback()

        callbacks.moved_to_location_callback.assert_called_once()

    def test_waiting_for_action_callback(self):
        callbacks = PetriNetCallbacks()
        callbacks.waiting_for_action_callback = MagicMock()

        callbacks.waiting_for_action_callback()

        callbacks.waiting_for_action_callback.assert_called_once()

    def test_action_executed_callback(self):
        callbacks = PetriNetCallbacks()
        callbacks.action_executed_callback = MagicMock()

        callbacks.action_executed_callback()

        callbacks.action_executed_callback.assert_called_once()

    def test_order_finished_callback(self):
        callbacks = PetriNetCallbacks()
        callbacks.order_finished_callback = MagicMock()

        transport_order = TransportOrder("order1")
        callbacks.order_finished_callback("source", transport_order)

        callbacks.order_finished_callback.assert_called_once_with("source", transport_order)

    def test_orders_finished_callback(self):
        callbacks = PetriNetCallbacks()
        callbacks.orders_finished_callback = MagicMock()

        callbacks.orders_finished_callback()

        callbacks.orders_finished_callback.assert_called_once()

    def test_finished_by_callback(self):
        callbacks = PetriNetCallbacks()
        callbacks.finished_by_callback = MagicMock()

        callbacks.finished_by_callback()

        callbacks.finished_by_callback.assert_called_once()

    def test_instance_updated_callback(self):
        callbacks = PetriNetCallbacks()
        callbacks.instance_updated_callback = MagicMock()

        callbacks.instance_updated_callback()

        callbacks.instance_updated_callback.assert_called_once()

    def test_task_finished_callback(self):
        callbacks = PetriNetCallbacks()
        callbacks.task_finished_callback = MagicMock()

        task = Task("task1")
        callbacks.task_finished_callback(task)

        callbacks.task_finished_callback.assert_called_once_with(task)

    def test_materialflow_finished_callback(self):
        callbacks = PetriNetCallbacks()
        callbacks.materialflow_finished_callback = MagicMock()

        callbacks.materialflow_finished_callback()

        callbacks.materialflow_finished_callback.assert_called_once()
