# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the PetriNetLogic class."""

# standard libraries
import unittest
from unittest.mock import MagicMock
from unittest.mock import patch

# 3rd party packages
from snakes import plugins

# local sources
from mfdl_scheduler.petri_net.logic import PetriNetLogic
from mfdl_scheduler.scheduling.event import Event

from nets import PetriNet, Value, Place, Transition, Marking, MultiSet

plugins.load(["labels", "gv"], "snakes.nets", "nets")


class TestPetriNetLogic(unittest.TestCase):
    """Testcase containing unit tests for the PetriNetLogic class."""

    def setUp(self):
        self.petri_net = PetriNet("")
        self.transition_dict = {}
        self.task_ids = {}
        self.logic = PetriNetLogic(self.petri_net, self.transition_dict, self.task_ids, False)

    def test_evaluate_petri_net(self):
        self.petri_net.add_place(Place("p1", []))
        self.petri_net.add_place(Place("p2", []))
        self.petri_net.add_transition(Transition("t1"))
        self.petri_net.add_input("p1", "t1", Value(1))
        self.petri_net.add_output("p2", "t1", Value(1))

        callback = MagicMock()
        self.logic.transition_dict["t1"] = [callback]

        self.petri_net.place("p1").add(1)

        self.logic.evaluate_petri_net()

        final_marking = Marking(p2=MultiSet([1]))
        self.assertEqual(self.petri_net.get_marking(), final_marking)
        callback.assert_called_once()

    def test_fire_event(self):
        self.petri_net.add_place(Place("mf_started"))
        self.petri_net.add_place(Place("task1_orderstep1_started_by"))
        self.petri_net.add_place(Place("task1_orderstep1_finished_by"))
        self.petri_net.add_place(Place("task1_orderstep1_agv1_pickup"))

        self.logic.task_ids["task1"] = {
            "started_by": "task1_orderstep1_started_by",
            "finished_by": "task1_orderstep1_finished_by",
            "agv1_pickup": "task1_orderstep1_agv1_pickup",
            "orderstep1": {"agv1_pickup": "task1_orderstep1_agv1_pickup"},
        }

        event1 = Event("mf_started")
        self.logic.fire_event(event1)
        self.assertEqual(self.petri_net.place("mf_started").tokens, Marking(MultiSet([1])))

        event2 = Event("started_by", {"task": "task1"})
        self.logic.fire_event(event2)
        self.assertEqual(
            self.petri_net.place("task1_orderstep1_started_by").tokens, Marking(MultiSet([1]))
        )

        event3 = Event("finished_by", {"task": "task1"})
        self.logic.fire_event(event3)
        self.assertEqual(
            self.petri_net.place("task1_orderstep1_finished_by").tokens, Marking(MultiSet([1]))
        )

        event4 = Event(
            "orderstep_update",
            {"task": "task1", "orderstep_id": "orderstep1", "status": "agv1_pickup"},
        )
        self.logic.fire_event(event4)
        self.assertEqual(
            self.petri_net.place("task1_orderstep1_agv1_pickup").tokens, Marking(MultiSet([1]))
        )
