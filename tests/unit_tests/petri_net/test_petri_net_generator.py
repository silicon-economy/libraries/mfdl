# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the PetriNetGenerator."""

# standard libraries
from typing import OrderedDict
import unittest
from unittest.mock import patch

# 3rd party libraries
from snakes import plugins

# local sources
from mfdl_scheduler.model.mfdl_program import MFDLProgram
from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.transportorderstep import TransportOrderStep
from mfdl_scheduler.model.moveorder import MoveOrder
from mfdl_scheduler.model.moveorderstep import MoveOrderStep
from mfdl_scheduler.model.actionorder import ActionOrder
from mfdl_scheduler.model.actionorderstep import ActionOrderStep
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.api.orderstep_api import OrderStepAPI
from mfdl_scheduler.petri_net.callbacks import PetriNetCallbacks
from mfdl_scheduler.petri_net.generator import PetriNetGenerator
from mfdl_scheduler.petri_net.generator import create_place, create_transition

plugins.load(["labels", "gv"], "snakes.nets", "nets")
from nets import PetriNet, Place, Transition, Value


def dummy_function():
    pass


class TestPetriNetGenerator(unittest.TestCase):
    """Testcase containing unit tests for the PetriNetGenerator

    Attributes:
        generator: The PetriNetGenerator object that is used for testing.
        mfdl_program: A dummy MFDL program from which the PetriNet should be generated.
    """

    def setUp(self) -> None:
        self.generator = PetriNetGenerator()
        self.generator.test_id_counter = 0

        self.setup_dummy_callbacks()

        mfdl_program = MFDLProgram()

        transport_order_step_1 = TransportOrderStep("tos_1")
        transport_order_step_2 = TransportOrderStep("tos_2")

        transport_order = TransportOrder(
            ["tos_1", "tos_2"],
            [transport_order_step_1, transport_order_step_2],
            "tos_2",
            transport_order_step_2,
        )
        transport_order_2 = TransportOrder(
            ["tos_1"], [transport_order_step_1], "tos_2", transport_order_step_2
        )

        action_order_step = ActionOrderStep("aos_1")
        action_order = ActionOrder("aos_1", action_order_step)

        move_order_step = MoveOrderStep("mos_1")
        move_order = MoveOrder("mos_1", move_order_step)

        mfdl_program.transportordersteps["tos_1"] = transport_order_step_1
        mfdl_program.transportordersteps["tos_2"] = transport_order_step_2

        mfdl_program.moveordersteps["mos_1"] = move_order_step

        mfdl_program.actionordersteps["aos_1"] = action_order_step

        task_1 = Task("task_1")
        task_1.statements = [transport_order, move_order]
        task_2 = Task("task_2")
        task_2.statements = [move_order]
        task_3 = Task("task_3")
        task_3.statements = [action_order]

        follow_up_task = Task("follow_up_task")
        follow_up_task.statements = [transport_order_2]

        task_1.follow_up_task_names = ["follow_up_task"]
        task_2.started_by_expr = True
        task_3.finished_by_expr = True

        mfdl_program.tasks = {
            "task_1": task_1,
            "task_2": task_2,
            "task_3": task_3,
            "follow_up_task": follow_up_task,
        }
        self.mfdl_program = mfdl_program
        self.generator.mfdl_program = mfdl_program

    def setup_dummy_callbacks(self):
        for callback in vars(self.generator.callbacks):
            vars(self.generator.callbacks)[callback] = dummy_function

    def test_init(self):
        generator = PetriNetGenerator()

        self.assertIsNone(generator.mfdl_program)
        self.assertEqual(generator.petri_net.name, "Materialflow")
        self.assertIsInstance(generator.transition_dict, OrderedDict)
        self.assertIsInstance(generator.task_ids, dict)
        self.assertIsInstance(generator.name_to_finish_transition_id, dict)
        self.assertIsInstance(generator.orderstep_to_finish_transitions, dict)
        self.assertIsInstance(generator.on_done_transition_dict, dict)
        self.assertIsInstance(generator.callbacks, PetriNetCallbacks)
        self.assertTrue(generator.draw_petri_net)
        self.assertIsInstance(generator.ordersteps, list)
        self.assertEqual(generator.test_id_counter, -1)

    def test_add_callback(self):
        transition_id = "test_transition"
        self.generator.transition_dict = {}

        def callback_function(arg1, arg2):
            return arg1 + arg2

        self.generator.add_callback(transition_id, callback_function, 1, 2)

        self.assertEqual(len(self.generator.transition_dict), 1)
        self.assertEqual(self.generator.transition_dict[transition_id][0](), 3)

    def test_generate_petri_net(self):
        net, transition_dict, task_ids = self.generator.generate_petri_net(
            self.mfdl_program, list(self.mfdl_program.tasks.values())
        )
        self.assertIsNotNone(net)
        self.assertIsNotNone(transition_dict)
        self.assertIsNotNone(task_ids)

    def test_generate_task(self):
        self.generator.generate_task(self.mfdl_program.tasks["task_1"])
        self.generator.generate_task(self.mfdl_program.tasks["task_2"])
        self.generator.generate_task(self.mfdl_program.tasks["task_3"])
        self.generator.generate_task(self.mfdl_program.tasks["follow_up_task"])
        self.assertTrue("task_1" in self.generator.task_ids)
        self.assertTrue("task_2" in self.generator.task_ids)
        self.assertTrue("task_3" in self.generator.task_ids)
        self.assertTrue("follow_up_task" in self.generator.task_ids)

    def test_generate_orders(self):
        self.generator.task_ids["task_1"] = {"0": ""}
        task_1 = self.mfdl_program.tasks["task_1"]
        task_2 = self.mfdl_program.tasks["task_2"]
        task_3 = self.mfdl_program.tasks["task_3"]

        self.generator.petri_net.add_place(Place("0", []))
        self.generator.petri_net.add_place(Place("1", []))

        with patch.object(
            self.generator, "generate_transport_order", return_value=["0", "1"]
        ) as mock:
            with patch.object(
                self.generator, "generate_move_order", return_value=["0", "1"]
            ) as mock_2:
                self.generator.generate_orders(task_1, task_1.statements, [], [])

                mock.assert_called_with(task_1.statements[0], task_1, False)
                mock_2.assert_called_with(task_1.statements[1], task_1, True)

        with patch.object(self.generator, "generate_move_order", return_value=["0", "1"]) as mock:
            self.generator.generate_orders(task_2, task_2.statements, [], [])

            mock.assert_called_with(task_2.statements[0], task_2, True)

        with patch.object(self.generator, "generate_action_order", return_value=["0", "1"]) as mock:
            self.generator.generate_orders(task_3, task_3.statements, [], [])

            mock.assert_called_with(task_3.statements[0], task_3, True)

    def test_generate_transport_order(self):
        task_1 = self.mfdl_program.tasks["task_1"]
        transport_order = task_1.statements[0]

        self.generator.petri_net.add_place(Place("0", []))
        self.generator.petri_net.add_place(Place("1", []))
        self.generator.petri_net.add_place(Place("2", []))
        self.generator.petri_net.add_place(Place("3", []))
        self.generator.petri_net.add_place(Place("4", []))
        self.generator.petri_net.add_place(Place("5", []))
        self.generator.petri_net.add_place(Place("6", []))
        self.generator.petri_net.add_place(Place("7", []))

        with patch("mfdl_scheduler.petri_net.generator.create_place", side_effect=["0"]):
            with patch.object(
                self.generator,
                "generate_transport_order_step",
                side_effect=[("2", "3", "0"), ("4", "5", "1"), ("6", "7", "1")],
            ):
                transport_started, tos_finished = self.generator.generate_transport_order(
                    transport_order, task_1, False
                )
                self.assertEqual(transport_started, "0")
                self.assertEqual(tos_finished, "7")

    def test_generate_transport_order_step(self):
        task_1 = self.mfdl_program.tasks["task_1"]
        transport_order = task_1.statements[0]
        transport_orderstep = transport_order.pickup_tos[0]
        tos_api = OrderStepAPI(transport_orderstep)

        self.generator.petri_net.add_place(Place("0", []))
        self.generator.petri_net.add_place(Place("1", []))
        self.generator.petri_net.add_place(Place("2", []))
        self.generator.petri_net.add_place(Place("3", []))
        self.generator.petri_net.add_place(Place("4", []))
        self.generator.petri_net.add_place(Place("5", []))

        self.generator.task_ids[task_1.name] = {tos_api.id: {}}
        with patch("mfdl_scheduler.petri_net.generator.create_place", side_effect=["0", "1", "2"]):
            self.generator.generate_transport_order_step(tos_api, transport_order, task_1)

            task_id_dict = {
                "moved_to_location": "2",
                "action_executed": "",
                "started_by": "",
                "finished_by": "",
            }
            self.assertEqual(self.generator.task_ids[task_1.name][tos_api.id], task_id_dict)

        with patch(
            "mfdl_scheduler.petri_net.generator.create_place", side_effect=["0", "1", "2", "3", "4"]
        ):
            transport_orderstep.parameters = {"test", "test"}
            self.generator.generate_transport_order_step(tos_api, transport_order, task_1)

            task_id_dict = {
                "moved_to_location": "2",
                "action_executed": "4",
                "started_by": "",
                "finished_by": "",
            }
            self.assertEqual(self.generator.task_ids[task_1.name][tos_api.id], task_id_dict)

        with patch("mfdl_scheduler.petri_net.generator.create_place", side_effect=["0", "1", "2"]):
            with patch.object(
                self.generator,
                "generate_started_by",
                side_effect=[("3", "0", "1")],
            ):
                with patch.object(
                    self.generator,
                    "generate_finished_by",
                    side_effect=[("4", "0")],
                ):
                    transport_orderstep.parameters = ""
                    transport_orderstep.started_by_expr = True
                    transport_orderstep.finished_by_expr = True
                    self.generator.generate_transport_order_step(tos_api, transport_order, task_1)

                    task_id_dict = {
                        "moved_to_location": "2",
                        "action_executed": "",
                        "started_by": "3",
                        "finished_by": "4",
                    }
                    self.assertEqual(self.generator.task_ids[task_1.name][tos_api.id], task_id_dict)

    def test_generate_move_order(self):
        task_2 = self.mfdl_program.tasks["task_2"]
        move_order = task_2.statements[0]

        self.generator.petri_net.add_place(Place("0", []))
        self.generator.petri_net.add_place(Place("1", []))
        self.generator.petri_net.add_place(Place("2", []))
        self.generator.petri_net.add_place(Place("3", []))
        self.generator.petri_net.add_place(Place("4", []))
        self.generator.petri_net.add_place(Place("5", []))
        self.generator.petri_net.add_place(Place("6", []))
        self.generator.petri_net.add_place(Place("7", []))

        with patch("mfdl_scheduler.petri_net.generator.create_place", side_effect=["0"]):
            with patch.object(
                self.generator,
                "generate_move_order_step",
                side_effect=[("1", "2", "0")],
            ):
                move_started, mos_finished = self.generator.generate_move_order(
                    move_order, task_2, True
                )
                self.assertEqual(move_started, "0")
                self.assertEqual(mos_finished, "2")

    def test_generate_move_order_step(self):
        task_2 = self.mfdl_program.tasks["task_2"]
        move_order = task_2.statements[0]
        move_orderstep = move_order.moveorderstep
        mos_api = OrderStepAPI(move_orderstep)

        self.generator.petri_net.add_place(Place("0", []))
        self.generator.petri_net.add_place(Place("1", []))
        self.generator.petri_net.add_place(Place("2", []))
        self.generator.petri_net.add_place(Place("3", []))
        self.generator.petri_net.add_place(Place("4", []))

        self.generator.task_ids[task_2.name] = {mos_api.id: {}}
        with patch("mfdl_scheduler.petri_net.generator.create_place", side_effect=["0", "1", "2"]):
            self.generator.generate_move_order_step(mos_api, move_order, task_2)

            task_id_dict = {
                "moved_to_location": "2",
                "started_by": "",
                "finished_by": "",
            }
            self.assertEqual(self.generator.task_ids[task_2.name][mos_api.id], task_id_dict)

        with patch("mfdl_scheduler.petri_net.generator.create_place", side_effect=["0", "1", "2"]):
            with patch.object(
                self.generator,
                "generate_started_by",
                side_effect=[("3", "0", "1")],
            ):
                with patch.object(
                    self.generator,
                    "generate_finished_by",
                    side_effect=[("4", "0")],
                ):
                    move_orderstep.started_by_expr = True
                    move_orderstep.finished_by_expr = True
                    self.generator.generate_move_order_step(mos_api, move_order, task_2)

                    task_id_dict = {
                        "moved_to_location": "2",
                        "started_by": "3",
                        "finished_by": "4",
                    }
                    self.assertEqual(self.generator.task_ids[task_2.name][mos_api.id], task_id_dict)

    def test_generate_action_order(self):
        task_3 = self.mfdl_program.tasks["task_3"]
        action_order = task_3.statements[0]

        self.generator.petri_net.add_place(Place("0", []))
        self.generator.petri_net.add_place(Place("1", []))
        self.generator.petri_net.add_place(Place("2", []))

        with patch("mfdl_scheduler.petri_net.generator.create_place", side_effect=["0"]):
            with patch.object(
                self.generator,
                "generate_action_order_step",
                side_effect=[("1", "2", "0")],
            ):
                action_started, aos_finished = self.generator.generate_action_order(
                    action_order, task_3, True
                )
                self.assertEqual(action_started, "0")
                self.assertEqual(aos_finished, "2")

    def test_generate_action_order_step(self):
        task_3 = self.mfdl_program.tasks["task_3"]
        action_order = task_3.statements[0]
        action_orderstep = action_order.actionorderstep
        aos_api = OrderStepAPI(action_orderstep)

        self.generator.petri_net.add_place(Place("0", []))
        self.generator.petri_net.add_place(Place("1", []))
        self.generator.petri_net.add_place(Place("2", []))
        self.generator.petri_net.add_place(Place("3", []))
        self.generator.petri_net.add_place(Place("4", []))

        self.generator.task_ids[task_3.name] = {aos_api.id: {}}
        with patch("mfdl_scheduler.petri_net.generator.create_place", side_effect=["0", "1", "2"]):
            self.generator.generate_action_order_step(aos_api, action_order, task_3)

            task_id_dict = {
                "action_executed": "2",
                "started_by": "",
                "finished_by": "",
            }
            self.assertEqual(self.generator.task_ids[task_3.name][aos_api.id], task_id_dict)

        with patch("mfdl_scheduler.petri_net.generator.create_place", side_effect=["0", "1", "2"]):
            with patch.object(
                self.generator,
                "generate_started_by",
                side_effect=[("3", "0", "1")],
            ):
                with patch.object(
                    self.generator,
                    "generate_finished_by",
                    side_effect=[("4", "0")],
                ):
                    action_orderstep.started_by_expr = True
                    action_orderstep.finished_by_expr = True
                    self.generator.generate_action_order_step(aos_api, action_order, task_3)

                    task_id_dict = {
                        "action_executed": "2",
                        "started_by": "3",
                        "finished_by": "4",
                    }
                    self.assertEqual(self.generator.task_ids[task_3.name][aos_api.id], task_id_dict)

    def test_generate_started_by(self):
        self.generator.petri_net.add_place(Place("0", []))
        self.generator.petri_net.add_place(Place("1", []))
        self.generator.petri_net.add_transition(Transition("2"))

        with patch("mfdl_scheduler.petri_net.generator.create_place", side_effect=["0", "1"]):
            with patch("mfdl_scheduler.petri_net.generator.create_transition", side_effect=["2"]):
                (
                    started_by_id,
                    start_place_id,
                    start_transition_id,
                ) = self.generator.generate_started_by("0")
                self.assertEqual(started_by_id, "1")
                self.assertEqual(start_place_id, "0")
                self.assertEqual(start_transition_id, "2")

    def test_generate_finished_by(self):
        self.generator.petri_net.add_place(Place("0", []))
        self.generator.petri_net.add_place(Place("1", []))
        self.generator.petri_net.add_transition(Transition("2"))

        with patch("mfdl_scheduler.petri_net.generator.create_place", side_effect=["0", "1"]):
            with patch("mfdl_scheduler.petri_net.generator.create_transition", side_effect=["2"]):
                (finished_by_id, finished_by_transition_id) = self.generator.generate_finished_by(
                    "2", "1"
                )
                self.assertEqual(finished_by_id, "1")
                self.assertEqual(finished_by_transition_id, "2")

    def test_create_place(self):
        petri_net = PetriNet("")
        create_place("place_1", petri_net, "id_1")

        with patch("uuid.uuid4", side_effect=["id_2"]):
            create_place("place_2", petri_net)

        self.assertTrue(petri_net.has_place("id_1"))
        self.assertTrue(petri_net.has_place("id_2"))

        self.assertEqual(petri_net.place("id_1").label("name"), "place_1")
        self.assertEqual(petri_net.place("id_2").label("name"), "place_2")

    def test_create_transition(self):
        petri_net = PetriNet("")

        with patch("uuid.uuid4", side_effect=["id_1"]):
            create_transition("transition", "type", petri_net)

        self.assertTrue(petri_net.has_transition("id_1"))
        self.assertEqual(
            petri_net.transition("id_1").label("name", "transitionType"), ("transition", "type")
        )
