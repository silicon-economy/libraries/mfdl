# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the ActionOrderStep class."""

# standard libraries
import unittest
from unittest.mock import MagicMock

# 3rd party libraries
from antlr4 import ParserRuleContext

# local sources
from mfdl_scheduler.model.actionorderstep import ActionOrderStep


class TestActionOrderStep(unittest.TestCase):
    """Testcase containing unit tests for the ActionOrderStep class."""

    def test_action_order_step(self):
        parser_context = MagicMock(spec=ParserRuleContext)

        action_order_step = ActionOrderStep(
            name="step_name",
            started_by_expr="started_by_expr",
            finished_by_expr="finished_by_expr",
            parameters={"param1": "value1", "param2": "value2"},
            follow_up_task_names=["task1", "task2"],
            context=parser_context,
        )

        self.assertEqual(action_order_step.name, "step_name")
        self.assertEqual(action_order_step.started_by_expr, "started_by_expr")
        self.assertEqual(action_order_step.finished_by_expr, "finished_by_expr")
        self.assertEqual(action_order_step.parameters, {"param1": "value1", "param2": "value2"})
        self.assertEqual(action_order_step.follow_up_task_names, ["task1", "task2"])
        self.assertEqual(action_order_step.context, parser_context)
