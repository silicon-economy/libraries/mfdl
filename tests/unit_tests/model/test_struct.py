# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the Struct class."""

# standard libraries
import unittest

# 3rd party libs
from antlr4.ParserRuleContext import ParserRuleContext

# local sources
from mfdl_scheduler.model.struct import Struct


class TestStruct(unittest.TestCase):
    """Testcase containing unit tests for the Struct class."""

    def test_init(self):
        name = "my_struct"
        attributes = {"attr_1": "string", "attr_2": "number"}
        parent_struct_name = "parent_struct"
        context = ParserRuleContext()

        struct = Struct(name, attributes, parent_struct_name, context)

        self.assertEqual(struct.name, name)
        self.assertEqual(struct.attributes, attributes)
        self.assertEqual(struct.parent_struct_name, parent_struct_name)
        self.assertEqual(struct.context, context)
        self.assertEqual(struct.attribute_contexts, {"id": None, "time": None})
