# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the Rule class."""

# standard libraries
import unittest

# 3rd party libs
from antlr4 import ParserRuleContext

# local sources
from mfdl_scheduler.model.rule import Rule


class TestRule(unittest.TestCase):
    """Testcase containing unit tests for the Rule class."""

    def test_rule(self):
        rule = Rule(
            name="rule_name",
            parameters={"param_1": "value_1", "param_2": "value_2"},
            expressions=[{"expr_1": "value_1"}, {"expr_2": "value_2"}],
            context=ParserRuleContext(),
        )

        self.assertEqual(rule.name, "rule_name")
        self.assertEqual(rule.parameters, {"param_1": "value_1", "param_2": "value_2"})
        self.assertEqual(rule.expressions, [{"expr_1": "value_1"}, {"expr_2": "value_2"}])
        self.assertIsInstance(rule.context, ParserRuleContext)
