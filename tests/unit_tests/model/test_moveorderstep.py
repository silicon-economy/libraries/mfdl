# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the MoveOrderStep class."""

# standard libraries
import unittest
from unittest.mock import MagicMock

# 3rd party libs
from antlr4 import ParserRuleContext

# local sources
from mfdl_scheduler.model.moveorderstep import MoveOrderStep
from mfdl_scheduler.model.instance import Instance


class TestMoveOrderStep(unittest.TestCase):
    """Testcase containing unit tests for the MoveOrderStep class."""

    def test_move_order_step(self):
        instance = MagicMock(spec=Instance)
        parser_context = MagicMock(spec=ParserRuleContext)

        move_order_step = MoveOrderStep(
            name="step_name",
            started_by_expr=True,
            finished_by_expr=False,
            parameters={"param_1": "value_1", "param_2": "value_2"},
            location_name="location_name",
            location=instance,
            follow_up_task_names=["task_1", "task_2"],
            context=parser_context,
        )

        self.assertEqual(move_order_step.name, "step_name")
        self.assertEqual(move_order_step.started_by_expr, True)
        self.assertEqual(move_order_step.finished_by_expr, False)
        self.assertEqual(move_order_step.parameters, {"param_1": "value_1", "param_2": "value_2"})
        self.assertEqual(move_order_step.location_name, "location_name")
        self.assertEqual(move_order_step.location, instance)
        self.assertEqual(move_order_step.follow_up_task_names, ["task_1", "task_2"])
        self.assertEqual(move_order_step.context, parser_context)
        self.assertEqual(move_order_step.context_dict, {})
