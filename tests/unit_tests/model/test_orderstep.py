# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the OrderStep class."""

# standard libraries
import unittest

# 3rd party libs
from antlr4 import ParserRuleContext

# local sources
from mfdl_scheduler.model.orderstep import OrderStep


class TestOrderStep(unittest.TestCase):
    """Testcase containing unit tests for the OrderStep class."""

    def test_order_step(self):
        order_step = OrderStep(
            name="step_name",
            started_by_expr="started_by_expr",
            finished_by_expr="finished_by_expr",
            follow_up_task_names=["task_1", "task_2"],
            context=ParserRuleContext(),
        )

        self.assertEqual(order_step.name, "step_name")
        self.assertEqual(order_step.started_by_expr, "started_by_expr")
        self.assertEqual(order_step.finished_by_expr, "finished_by_expr")
        self.assertEqual(order_step.follow_up_task_names, ["task_1", "task_2"])
        self.assertIsInstance(order_step.context, ParserRuleContext)
