# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the Instance class."""

# standard libraries
import unittest
from unittest.mock import MagicMock

# 3rd party libs
from antlr4 import ParserRuleContext

# local sources
from mfdl_scheduler.model.instance import Instance, parse_json
from mfdl_scheduler.validation.error_handler import ErrorHandler


class TestInstance(unittest.TestCase):
    """Testcase containing unit tests for the Instance class."""

    def test_instance(self):
        error_handler = MagicMock(spec=ErrorHandler)
        struct_context = MagicMock(spec=ParserRuleContext)

        instance = Instance(
            name="instance_name",
            attributes={"attr_1": "value_1", "attr_2": 100, "attr_3": False},
            struct_name="struct_name",
            context=struct_context,
        )

        # Test attributes
        self.assertEqual(instance.name, "instance_name")
        self.assertEqual(instance.attributes, {"attr_1": "value_1", "attr_2": 100, "attr_3": False})
        self.assertEqual(instance.struct_name, "struct_name")
        self.assertEqual(instance.context, struct_context)
        self.assertEqual(instance.attribute_contexts, {})

        json_object = {
            "attr_1": "value_1",
            "attr_2": 100,
            "attr_3": False,
            "attr_4": {"inner_attr": "inner_value"},
        }
        instance_from_json = Instance.from_json(json_object, error_handler, struct_context)

        self.assertIsInstance(instance_from_json, Instance)
        self.assertEqual(instance_from_json.name, "")
        self.assertEqual(
            instance_from_json.attributes,
            {
                "attr_1": "value_1",
                "attr_2": 100,
                "attr_3": False,
                "attr_4": Instance(
                    name="", attributes={"inner_attr": "inner_value"}, struct_name="", context=None
                ),
            },
        )
        self.assertEqual(instance_from_json.struct_name, "")
        self.assertEqual(instance_from_json.context, struct_context)
        self.assertEqual(instance_from_json.attribute_contexts, {})

    def test_parse_json(self):
        error_handler = MagicMock(spec=ErrorHandler)
        instance_context = MagicMock(spec=ParserRuleContext)

        json_object = {
            "attr_1": "value_1",
            "attr_2": 100,
            "attr_3": False,
            "attr_4": {"inner_attr": "inner_value"},
        }
        instance = parse_json(json_object, error_handler, instance_context)

        self.assertIsInstance(instance, Instance)
        self.assertEqual(instance.name, "")
        self.assertEqual(
            instance.attributes,
            {
                "attr_1": "value_1",
                "attr_2": 100,
                "attr_3": False,
                "attr_4": Instance(
                    name="", attributes={"inner_attr": "inner_value"}, struct_name="", context=None
                ),
            },
        )
        self.assertEqual(instance.struct_name, "")
        self.assertEqual(instance.attribute_contexts, {})
