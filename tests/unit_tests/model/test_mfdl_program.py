# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the MFDLProgramm class."""

# standard libraries
import unittest

# local sources
from mfdl_scheduler.model.mfdl_program import MFDLProgram
from mfdl_scheduler.model.struct import Struct
from mfdl_scheduler.model.instance import Instance
from mfdl_scheduler.model.module import Module
from mfdl_scheduler.model.rule import Rule
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.transportorderstep import TransportOrderStep
from mfdl_scheduler.model.moveorderstep import MoveOrderStep
from mfdl_scheduler.model.actionorderstep import ActionOrderStep


class TestMFDLProgram(unittest.TestCase):
    """Tests for the methods of the MFDLProgram class."""

    def test_init(self):
        mfdl_program = MFDLProgram()
        self.assertEqual(mfdl_program.structs, {})
        self.assertEqual(mfdl_program.instances, {})
        self.assertEqual(mfdl_program.modules, {})
        self.assertEqual(mfdl_program.rules, {})
        self.assertEqual(mfdl_program.tasks, {})
        self.assertEqual(mfdl_program.transportordersteps, {})
        self.assertEqual(mfdl_program.moveordersteps, {})
        self.assertEqual(mfdl_program.actionordersteps, {})

        dummy_dict = {"test": "test"}
        mfdl_program = MFDLProgram(
            structs=dummy_dict,
            instances=dummy_dict,
            modules=dummy_dict,
            rules=dummy_dict,
            tasks=dummy_dict,
            transportordersteps=dummy_dict,
            moveordersteps=dummy_dict,
            actionordersteps=dummy_dict,
        )
        self.assertEqual(mfdl_program.structs, dummy_dict)
        self.assertEqual(mfdl_program.instances, dummy_dict)
        self.assertEqual(mfdl_program.modules, dummy_dict)
        self.assertEqual(mfdl_program.rules, dummy_dict)
        self.assertEqual(mfdl_program.tasks, dummy_dict)
        self.assertEqual(mfdl_program.transportordersteps, dummy_dict)
        self.assertEqual(mfdl_program.moveordersteps, dummy_dict)
        self.assertEqual(mfdl_program.actionordersteps, dummy_dict)

    def test_append_mfdl_program_or_module_content(self):
        program_1 = MFDLProgram(
            structs={"struct_1": Struct()},
            instances={"instance_1": Instance()},
            modules={"module_1": Module()},
            rules={"rule_1": Rule()},
            tasks={"task_1": Task()},
            transportordersteps={"transport_1": TransportOrderStep()},
            moveordersteps={"move_1": MoveOrderStep()},
            actionordersteps={"action_1": ActionOrderStep()},
        )

        program_2 = MFDLProgram(
            structs={"struct_2": Struct()},
            instances={"instance_2": Instance()},
            modules={"module_2": Module()},
            rules={"rule_2": Rule()},
            tasks={"task_2": Task()},
            transportordersteps={"transport_2": TransportOrderStep()},
            moveordersteps={"move_2": MoveOrderStep()},
            actionordersteps={"action_2": ActionOrderStep()},
        )

        program_1.append_mfdl_program_or_module_content(program_2)

        self.assertEqual(len(program_1.structs), 2)
        self.assertEqual(len(program_1.instances), 2)
        self.assertEqual(len(program_1.modules), 1)
        self.assertEqual(len(program_1.rules), 2)
        self.assertEqual(len(program_1.tasks), 2)
        self.assertEqual(len(program_1.transportordersteps), 2)
        self.assertEqual(len(program_1.moveordersteps), 2)
        self.assertEqual(len(program_1.actionordersteps), 2)

    def test_get_instances(self):
        program = MFDLProgram(
            structs={
                "struct_1": Struct(parent_struct_name=""),
                "struct_2": Struct(parent_struct_name="struct_1"),
                "struct_3": Struct(parent_struct_name="struct_2"),
                "struct_4": Struct(parent_struct_name="struct_1"),
            },
            instances={
                "instance_1": Instance(struct_name="struct_1"),
                "instance_2": Instance(struct_name="struct_2"),
                "instance_3": Instance(struct_name="struct_3"),
                "instance_4": Instance(struct_name="struct_4"),
            },
        )

        instances = program.get_instances("struct_1")
        self.assertEqual(len(instances), 4)

        instances = program.get_instances("struct_2")
        self.assertEqual(len(instances), 2)

        instances = program.get_instances("struct_3")
        self.assertEqual(len(instances), 1)

        instances = program.get_instances("struct_4")
        self.assertEqual(len(instances), 1)
