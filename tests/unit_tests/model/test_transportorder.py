# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the TransportOrder class."""

# standard libraries
import unittest

# 3rd party libs
from antlr4.ParserRuleContext import ParserRuleContext

# local sources
from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.transportorderstep import TransportOrderStep


class TestTransportOrder(unittest.TestCase):
    """Testcase containing unit tests for the TransportOrder class."""

    def test_init(self):
        pickup_tos_names = ["pickup_1", "pickup_2"]
        pickup_tos = [TransportOrderStep(), TransportOrderStep()]
        delivery_tos_name = "delivery"
        delivery_tos = TransportOrderStep()
        context = ParserRuleContext()

        transport_order = TransportOrder(
            pickup_tos_names, pickup_tos, delivery_tos_name, delivery_tos, context
        )

        self.assertIsNotNone(transport_order.uuid_)
        self.assertEqual(transport_order.pickup_tos_names, pickup_tos_names)
        self.assertEqual(transport_order.pickup_tos, pickup_tos)
        self.assertEqual(transport_order.delivery_tos_name, delivery_tos_name)
        self.assertEqual(transport_order.delivery_tos, delivery_tos)
        self.assertEqual(transport_order.context, context)
