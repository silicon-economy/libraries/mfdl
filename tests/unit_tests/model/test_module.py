# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the Module class."""

# standard libraries
import unittest
from unittest.mock import MagicMock

# 3rd party libs
from antlr4 import ParserRuleContext

# local sources
from mfdl_scheduler.model.module import Module
from mfdl_scheduler.model.struct import Struct
from mfdl_scheduler.model.instance import Instance
from mfdl_scheduler.model.rule import Rule
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.transportorderstep import TransportOrderStep
from mfdl_scheduler.model.moveorderstep import MoveOrderStep
from mfdl_scheduler.model.actionorderstep import ActionOrderStep


class TestModule(unittest.TestCase):
    """Testcase containing unit tests for the Module class."""

    def test_module(self):
        struct1 = MagicMock(spec=Struct)
        struct2 = MagicMock(spec=Struct)
        instance1 = MagicMock(spec=Instance)
        instance2 = MagicMock(spec=Instance)
        rule1 = MagicMock(spec=Rule)
        rule2 = MagicMock(spec=Rule)
        task1 = MagicMock(spec=Task)
        task2 = MagicMock(spec=Task)
        transport_order_step1 = MagicMock(spec=TransportOrderStep)
        transport_order_step2 = MagicMock(spec=TransportOrderStep)
        move_order_step1 = MagicMock(spec=MoveOrderStep)
        move_order_step2 = MagicMock(spec=MoveOrderStep)
        action_order_step1 = MagicMock(spec=ActionOrderStep)
        action_order_step2 = MagicMock(spec=ActionOrderStep)
        parser_context = MagicMock(spec=ParserRuleContext)

        module = Module(
            name="module_name",
            structs={"struct1": struct1, "struct2": struct2},
            instances={"instance1": instance1, "instance2": instance2},
            rules={"rule1": rule1, "rule2": rule2},
            tasks={"task1": task1, "task2": task2},
            transportordersteps={"step1": transport_order_step1, "step2": transport_order_step2},
            moveordersteps={"step1": move_order_step1, "step2": move_order_step2},
            actionordersteps={"step1": action_order_step1, "step2": action_order_step2},
            context=parser_context,
        )

        self.assertEqual(module.name, "module_name")
        self.assertEqual(module.structs, {"struct1": struct1, "struct2": struct2})
        self.assertEqual(module.instances, {"instance1": instance1, "instance2": instance2})
        self.assertEqual(module.rules, {"rule1": rule1, "rule2": rule2})
        self.assertEqual(module.tasks, {"task1": task1, "task2": task2})
        self.assertEqual(
            module.transportordersteps,
            {"step1": transport_order_step1, "step2": transport_order_step2},
        )
        self.assertEqual(
            module.moveordersteps, {"step1": move_order_step1, "step2": move_order_step2}
        )
        self.assertEqual(
            module.actionordersteps, {"step1": action_order_step1, "step2": action_order_step2}
        )
        self.assertEqual(module.context, parser_context)
