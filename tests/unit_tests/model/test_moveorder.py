# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the MoveOrder class."""

# standard libraries
import unittest
from unittest.mock import MagicMock

# 3rd party libs
from antlr4 import ParserRuleContext

# local sources
from mfdl_scheduler.model.moveorder import MoveOrder
from mfdl_scheduler.model.moveorderstep import MoveOrderStep


class TestMoveOrder(unittest.TestCase):
    """Testcase containing unit tests for the MoveOrder class."""

    def test_move_order(self):
        move_order_step = MoveOrderStep()
        parser_context = MagicMock(spec=ParserRuleContext)

        move_order = MoveOrder("step_name", move_order_step, parser_context)

        self.assertIsInstance(move_order.uuid_, str)
        self.assertNotEqual(move_order.uuid_, "")
        self.assertIsNotNone(move_order.uuid_)

        self.assertEqual(move_order.moveorderstep_name, "step_name")
        self.assertEqual(move_order.moveorderstep, move_order_step)
        self.assertEqual(move_order.context, parser_context)
