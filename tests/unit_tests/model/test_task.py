# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the Task class."""

# standard libraries
import unittest

# 3rd party libs
from antlr4.ParserRuleContext import ParserRuleContext

# local sources
from mfdl_scheduler.model.actionorder import ActionOrder
from mfdl_scheduler.model.actionorderstep import ActionOrderStep
from mfdl_scheduler.model.moveorder import MoveOrder
from mfdl_scheduler.model.moveorderstep import MoveOrderStep
from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.transportorderstep import TransportOrderStep
from mfdl_scheduler.model.task import Task


class TestTask(unittest.TestCase):
    """Testcase containing unit tests for the Task class."""

    def test_init(self):
        name = "my_task"
        statements = [TransportOrder(), MoveOrder(), ActionOrder()]
        started_by_expr = True
        finished_by_expr = True
        constraints = {"constraint": "constraints"}
        repeat = 5
        follow_up_task_names = ["task_1", "task_2"]
        context = ParserRuleContext()

        task = Task(
            name,
            statements,
            started_by_expr,
            finished_by_expr,
            constraints,
            "",
            repeat,
            follow_up_task_names,
            context,
        )

        self.assertEqual(task.name, name)
        self.assertEqual(task.statements, statements)
        self.assertEqual(task.started_by_expr, started_by_expr)
        self.assertEqual(task.finished_by_expr, finished_by_expr)
        self.assertEqual(task.constraints, constraints)
        self.assertEqual(task.repeat, repeat)
        self.assertEqual(task.follow_up_task_names, follow_up_task_names)
        self.assertEqual(task.context, context)

    def test_get_ordersteps(self):
        transport_order = TransportOrder()
        move_order = MoveOrder()
        action_order = ActionOrder()

        pickup_tos = [TransportOrderStep(), TransportOrderStep()]
        delivery_tos = TransportOrderStep()
        transport_order.pickup_tos = pickup_tos
        transport_order.delivery_tos = delivery_tos

        moveorderstep = MoveOrderStep()
        move_order.moveorderstep = moveorderstep

        actionorderstep = ActionOrderStep()
        action_order.actionorderstep = actionorderstep

        statements = [transport_order, move_order, action_order]

        task = Task(statements=statements)

        ordersteps = task.get_ordersteps()

        self.assertEqual(len(ordersteps), 5)
        self.assertEqual(ordersteps[0], pickup_tos[0])
        self.assertEqual(ordersteps[1], pickup_tos[1])
        self.assertEqual(ordersteps[2], delivery_tos)
        self.assertEqual(ordersteps[3], moveorderstep)
        self.assertEqual(ordersteps[4], actionorderstep)
