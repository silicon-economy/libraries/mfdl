# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the ActionOrder class."""

# standard libraries
import unittest

# 3rd party libraries
from antlr4.ParserRuleContext import ParserRuleContext

# local sources
from mfdl_scheduler.model.actionorder import ActionOrder
from mfdl_scheduler.model.actionorderstep import ActionOrderStep


class TestActionOrder(unittest.TestCase):
    """Testcase containing unit tests for the ActionOrder class."""

    def test_init(self):
        actionorderstep_name = "aos"
        actionorderstep = ActionOrderStep()
        context = ParserRuleContext()

        action_order = ActionOrder(actionorderstep_name, actionorderstep, context)

        self.assertIsNotNone(action_order.uuid_)
        self.assertEqual(action_order.actionorderstepname, actionorderstep_name)
        self.assertEqual(action_order.actionorderstep, actionorderstep)
        self.assertEqual(action_order.context, context)
