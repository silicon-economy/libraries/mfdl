# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the TransportOrderStep class."""

# standard libraries
import unittest

# 3rd party libs
from antlr4.ParserRuleContext import ParserRuleContext

# local sources
from mfdl_scheduler.model.instance import Instance
from mfdl_scheduler.model.transportorderstep import TransportOrderStep


class TestTransportOrderStep(unittest.TestCase):
    """Testcase containing unit tests for the TransportOrderStep class."""

    def test_init(self):
        name = "tos"
        started_by_expr = {"condition": "started_by_expr"}
        finished_by_expr = {"condition": "finished_by_expr"}
        parameters = {"param_1": "value_1", "param_2": "value_2"}
        location_name = "Location"
        location = Instance()
        follow_up_task_names = ["task_1", "task_2"]
        context = ParserRuleContext()

        transport_order_step = TransportOrderStep(
            name,
            started_by_expr,
            finished_by_expr,
            parameters,
            location_name,
            location,
            follow_up_task_names,
            context,
        )

        self.assertEqual(transport_order_step.name, name)
        self.assertEqual(transport_order_step.started_by_expr, started_by_expr)
        self.assertEqual(transport_order_step.finished_by_expr, finished_by_expr)
        self.assertEqual(transport_order_step.parameters, parameters)
        self.assertEqual(transport_order_step.location_name, location_name)
        self.assertEqual(transport_order_step.location, location)
        self.assertEqual(transport_order_step.follow_up_task_names, follow_up_task_names)
        self.assertEqual(transport_order_step.context, context)
