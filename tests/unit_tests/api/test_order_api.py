# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the OrderAPI class."""

# standard libraries
import unittest

# local sources
from mfdl_scheduler.api.order_api import OrderAPI
from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.moveorder import MoveOrder
from mfdl_scheduler.model.actionorder import ActionOrder


class TestOrderAPI(unittest.TestCase):
    """Testcase containing unit tests for the OrderAPI class."""

    def test_order_api(self):
        transport_order = TransportOrder()
        order_api_1 = OrderAPI(transport_order)

        self.assertIsInstance(order_api_1.order, TransportOrder)
        self.assertNotEqual(order_api_1.id, "")
        self.assertIsNotNone(order_api_1.id)

        move_order = MoveOrder()
        order_api_2 = OrderAPI(move_order)

        self.assertIsInstance(order_api_2.order, MoveOrder)
        self.assertNotEqual(order_api_2.id, "")
        self.assertIsNotNone(order_api_2.id)

        action_order = ActionOrder()
        order_api_3 = OrderAPI(action_order)

        self.assertIsInstance(order_api_3.order, ActionOrder)
        self.assertNotEqual(order_api_3.id, "")
        self.assertIsNotNone(order_api_3.id)
