# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains unit tests for the OrderStepAPI class."""

# standard libraries
import unittest

# local sources
from mfdl_scheduler.api.orderstep_api import OrderStepAPI
from mfdl_scheduler.model.transportorderstep import TransportOrderStep
from mfdl_scheduler.model.moveorderstep import MoveOrderStep
from mfdl_scheduler.model.actionorderstep import ActionOrderStep


class TestOrderStepAPI(unittest.TestCase):
    """Testcase containing unit tests for the OrderStepAPI class."""

    def test_order_step_api(self):
        transport_order_step = TransportOrderStep()
        order_step_api_1 = OrderStepAPI(transport_order_step)

        self.assertIsInstance(order_step_api_1.orderstep, TransportOrderStep)
        self.assertNotEqual(order_step_api_1.id, "")
        self.assertIsNotNone(order_step_api_1.id)

        move_order_step = MoveOrderStep()
        order_step_api_2 = OrderStepAPI(move_order_step)

        self.assertIsInstance(order_step_api_2.orderstep, MoveOrderStep)
        self.assertNotEqual(order_step_api_2.id, "")
        self.assertIsNotNone(order_step_api_2.id)

        action_order_step = ActionOrderStep()
        order_step_api_3 = OrderStepAPI(action_order_step)

        self.assertIsInstance(order_step_api_3.orderstep, ActionOrderStep)
        self.assertNotEqual(order_step_api_3.id, "")
        self.assertIsNotNone(order_step_api_3.id)
