# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

from typing import Union
import uuid

from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.moveorder import MoveOrder
from mfdl_scheduler.model.actionorder import ActionOrder


class OrderAPI:
    def __init__(self, order: Union[TransportOrder, MoveOrder, ActionOrder]):
        self.id = str(uuid.uuid4())
        self.order: Union[TransportOrder, MoveOrder, ActionOrder] = order
