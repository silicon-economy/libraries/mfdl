# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

from typing import Union
import uuid

from mfdl_scheduler.model.transportorderstep import TransportOrderStep
from mfdl_scheduler.model.moveorderstep import MoveOrderStep
from mfdl_scheduler.model.actionorderstep import ActionOrderStep


class OrderStepAPI:
    def __init__(self, orderstep: Union[TransportOrderStep, MoveOrderStep, ActionOrderStep]):
        self.id = str(uuid.uuid4())
        self.orderstep: Union[TransportOrderStep, MoveOrderStep, ActionOrderStep] = orderstep
