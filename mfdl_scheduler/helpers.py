# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains helper functions used in the whole project."""

# standard libraries
from typing import Any, Dict, List, Tuple, Union

# local sources
from mfdl_scheduler.model.struct import Struct
from mfdl_scheduler.model.instance import Instance
from mfdl_scheduler.model.rule import Rule


def load_file(file_path: str) -> str:
    """Loads the content of the file from the given path.

    Returns:
        The content of the file as a string.
    """
    mfdl_string = ""
    with open(file_path, "r", encoding="utf-8") as file:
        mfdl_string = file.read()
    return mfdl_string


def import_list_to_path(path_list: List[str]) -> str:
    """Converts a file path in form of a list into a real path

    Returns:
        The real path of the corresponding list as string.
    """
    path = ""
    for element in path_list:
        path = path + element + "/"
    return path[:-1]


def check_type_of_value(value: Any, value_type: str) -> bool:
    """Checks if the given value is the given type in the DSL.

    Returns:
        True if the value is from the given value type.
    """
    if value_type == "number":
        # bool is a subclass of int so check it before
        if isinstance(value, bool):
            return False
        return isinstance(value, (int, float))
    if value_type == "boolean":
        return isinstance(value, bool)
    if value_type == "string":
        return isinstance(value, str)
    if isinstance(value, Struct):
        return value.name == value_type
    # value was a string
    return True


def is_con(string: str) -> bool:
    """Checks if the given string is a condition element in the MFDL.

    A condition element can be a MFDL string, boolean or number

    Returns:
        True if the given string is a condition element in the MFDL.
    """
    return is_string(string) or is_boolean(string) or is_number(string)


def is_string(string: str) -> bool:
    """Check if the given parameter is a string in the DSL: It should start and end with '"'.

    Returns:
        True if the given string is a string in the DSL.
    """
    if isinstance(string, str) and string.startswith('"') and string.endswith('"'):
        return True
    return False


def is_boolean(string: str) -> bool:
    """Checks if the given string can be casted to a boolean.

    Returns:
        True if the given string can be casted to a boolean.
    """
    if string in ("True", "TRUE", "False", "FALSE"):
        return True
    return False


def is_number(string: str) -> bool:
    """Checks if the given string can be casted to a number (int or float).

    Returns:
        True if the given string can be casted to a number.
    """
    if is_float(string) or is_int(string):
        return True
    return False


def is_float(string: str) -> bool:
    """Checks if the given string can be casted to a float.

    Returns:
        True if the given string can be casted to a float.
    """
    try:
        float(string)
    except (TypeError, ValueError):
        return False
    else:
        return True


def is_int(string: str) -> bool:
    """Checks if the given string can be casted to an integer.

    Returns:
        True if the given string can be casted to an integer.
    """
    try:
        int(string)
    except (TypeError, ValueError):
        return False
    else:
        return True


def is_attribute_access(attribute: str) -> bool:
    """Returns True if the given string is an attribute access.

    An attribute access in the MFDL has the following form: x.y.z...

    Returns:
        True if the given string is an attribute access
    """
    return attribute.find(".") != -1


def substitute_parameter_in_expression(expression, subs: Dict) -> Union[str, Dict]:
    """Substitutes instance names with the values in subs.

    Returns:
        The expression with the substituted values.
    """
    # if the expression is only a string
    if isinstance(expression, str):
        if is_attribute_access(expression):
            instance_name = expression.split(".")[0]
            if instance_name in subs:
                expression = subs[instance_name] + "." + expression[expression.find(".") + 1 :]
        else:
            if expression in subs:
                expression = subs[expression]
        return expression
    if isinstance(expression, Tuple):
        new_params = {}
        for old, new in subs.items():
            if old in expression[1]:
                new_params[new] = None
        return (expression[0], new_params)
    if isinstance(expression, Dict):
        if len(expression) == 2:
            sub_expr = substitute_parameter_in_expression(expression["value"], subs)
            return {"Unop": expression["unOp"], "value": sub_expr}
        if expression["left"] == "(" and expression["right"] == ")":
            sub_expr = substitute_parameter_in_expression(expression["binOp"], subs)
            return {"left": "(", "binOp": sub_expr, "right": ")"}

        new_left = substitute_parameter_in_expression(expression["left"], subs)
        new_right = substitute_parameter_in_expression(expression["right"], subs)
        return {
            "left": new_left,
            "binOp": expression["binOp"],
            "right": new_right,
        }
    return expression


def evaluate_expression(
    expression: Any, instances: Dict[str, Instance], rules: Dict[str, Rule]
) -> bool:
    """Executes the given MFDL expression as a Python expression.

    Args:
        expression: A dict representing the expression.
        instances: A Dict that contains all Instances of the MFDL program.
        rules: A Dict that contains all Rules of the MFDL program.

    Returns:
        The value of the expression executed in Python (type depends on specific expression).
    """
    if isinstance(expression, (int, float, bool)):
        return expression
    if isinstance(expression, str):
        if is_attribute_access(expression):
            return get_attribute_access_value(expression, instances)
        else:
            # MFDL strings are saved with the '"" so delete it here
            return expression.replace('"', "")
    if isinstance(expression, Tuple):
        return evaluate_rule(expression, instances, rules)

    if isinstance(expression, Dict):
        if len(expression) == 2:
            return not evaluate_expression(expression["value"], instances, rules)

        if expression["left"] == "(" and expression["right"] == ")":
            return evaluate_expression(expression["binOp"], instances, rules)

        if expression["binOp"] == ">":
            return evaluate_expression(expression["left"], instances, rules) > evaluate_expression(
                expression["right"], instances, rules
            )
        elif expression["binOp"] == "<":
            return evaluate_expression(expression["left"], instances, rules) < evaluate_expression(
                expression["right"], instances, rules
            )
        elif expression["binOp"] == "<=":
            return evaluate_expression(expression["left"], instances, rules) <= evaluate_expression(
                expression["right"], instances, rules
            )
        elif expression["binOp"] == ">=":
            return evaluate_expression(expression["left"], instances, rules) >= evaluate_expression(
                expression["right"], instances, rules
            )
        elif expression["binOp"] == "==":
            return evaluate_expression(expression["left"], instances, rules) == evaluate_expression(
                expression["right"], instances, rules
            )
        elif expression["binOp"] == "!=":
            return evaluate_expression(expression["left"], instances, rules) == evaluate_expression(
                expression["right"], instances, rules
            )
        elif expression["binOp"] == "And":
            return evaluate_expression(
                expression["left"], instances, rules
            ) and evaluate_expression(expression["right"], instances, rules)
        elif expression["binOp"] == "Or":
            return evaluate_expression(expression["left"], instances, rules) or evaluate_expression(
                expression["right"], instances, rules
            )
        elif expression["binOp"] == "+":
            return evaluate_expression(expression["left"], instances, rules) + evaluate_expression(
                expression["right"], instances, rules
            )
        elif expression["binOp"] == "-":
            return evaluate_expression(expression["left"], instances, rules) - evaluate_expression(
                expression["right"], instances, rules
            )
        elif expression["binOp"] == "*":
            return evaluate_expression(expression["left"], instances, rules) * evaluate_expression(
                expression["right"], instances, rules
            )
        elif expression["binOp"] == "/":
            return evaluate_expression(expression["left"], instances, rules) / evaluate_expression(
                expression["right"], instances, rules
            )


def get_attribute_access_value(
    attribute_access: str, instances: Dict[str, Instance]
) -> Union[int, float, bool, str]:
    """Get the exact value from a variable accessed through an attribute access.

    Args:
        attribute_access: The attribute access represented as a List of strings.
        instances: A Dict that contains all Instances of the MFDL program.

    Returns:
        The value (int, float, bool or str)
    """
    attributes = attribute_access.split(".")
    instance = instances[attributes[0]]
    value = None
    for i, attribute in enumerate(attributes[1:]):
        # last element in attribute list
        if i == len(attributes[1:]) - 1:
            value = instance.attributes[attribute]
        else:
            instance = instances[instance.attributes[attribute]]
    return value


def evaluate_rule(rule_call: Tuple, instances: Dict[str, Instance], rules: Dict[str, Rule]) -> bool:
    """Executes the given Rule call with the help of Python expressions

    Args:
        rule_call: The given Rule call that should be evaluated.
        instances: A Dict that contains all Instances of the MFDL program.
        rules: A Dict that contains all Rules of the MFDL program.

    Returns:
        True if all expressions inside the values are evaluated as true.
    """
    rule_name = rule_call[0]
    rule_call_parameters = rule_call[1]
    rule = rules[rule_name]
    subs = {}

    if len(rule.parameters) > 0:
        for i, (rule_parameter, default_value) in enumerate(rule.parameters.items()):
            if i >= len(rule_call_parameters):
                subs[rule_parameter] = default_value
            else:
                subs[rule_parameter] = list(rule_call_parameters)[i]

    for expression in rule.expressions:
        substituted_expr = substitute_parameter_in_expression(expression, subs)
        if not evaluate_expression(substituted_expr, instances, rules):
            return False
    return True
