# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains Scheduler class."""

# standard libraries
from typing import Dict, List, Union
import time
from datetime import datetime
import threading
from uuid import uuid4

# 3rd party packages

# Temporary fix because this module cannot be imported in
# the integration test
try:
    from croniter import croniter
except ImportError:
    pass

import networkx as nx

# local sources
from mfdl_scheduler.model.mfdl_program import MFDLProgram
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.moveorder import MoveOrder
from mfdl_scheduler.model.actionorder import ActionOrder

from mfdl_scheduler.scheduling.event import Event
from mfdl_scheduler.scheduling.callbacks import MaterialFlowCallbacks

from mfdl_scheduler.api.orderstep_api import OrderStepAPI

from mfdl_scheduler.petri_net.generator import PetriNetGenerator
from mfdl_scheduler.petri_net.logic import PetriNetLogic

from mfdl_scheduler.validation.validate import parse_string

from mfdl_scheduler.helpers import evaluate_expression


class Scheduler:
    """Schedules the Materialflow described by a given MFDL file.

    The scheduler comprises almost the complete execution of a Materialflow including
    the parsing of the MFDL description, model creation and validation and execution of
    the petri net. It informs the user about started or finished Tasks, Orders, and Ordersteps.


    Attributes:
        scheduler_id: A string representing the unique identifier of the Scheduler.
        is_running: A boolean indicating whether the Materialflow is still active / running.
        mfdl_program: The MFDLProgram instance from which the Petri Net is generated.
        callbacks: MaterialFlowCallbacks instance which holds the registered callbacks for the MF.
        active_ordersteps: A List containing all currently active / running OrderSteps.
        active_tasks: A List containing all currently active / running Tasks.
        awaited_events: A list of awaited `Event`s. Only these events can be passed to the net.
        starting_time: A float representing the time the Scheduler started (seconds since the epoch).
        petri_net_generator: A PetriNetGenerator instance for generating the petri net.
        petri_net_logic: A PetriNetLogic instance for execution of the petri net.
    """

    def __init__(
        self,
        mfdl_string: str,
        scheduler_id=str(uuid4()),
        file_path="",
        draw_petri_net: bool = True,
        use_test_ids: bool = False,
    ) -> None:
        """Initialize the object.

        If the given path leads to a valid MFDL file the parsing will be started. If no errors
        occur the model of the MFDL File will be transformed into a petri net and be drawn if
        the `draw_petri_net` flag is set. If `use_test_ids` is set the ids of the started
        ordersteps will be an enumeration starting at 0.

        Args:
            mfdl_string: The string of the MFDL file that should be parsed.
            scheduler_id: A string representing the unique identifier of the Scheduler.
            file_path: The path of the MFDL file (used for error messages).
            draw_petri_net: A boolean indicating whether the petri net should be drawn.
            use_test_ids: A boolean indicating whether test ids should be generated.
        """
        self.scheduler_id: str = scheduler_id
        self._is_running: bool = False
        self.mfdl_program: MFDLProgram = parse_string(mfdl_string, file_path)
        self.callbacks: MaterialFlowCallbacks = MaterialFlowCallbacks()

        self.active_ordersteps: List[OrderStepAPI] = []
        self.active_tasks: List[str] = []

        self.awaited_events: Dict[str, List[Event]] = {}
        self.starting_time: float = time.time()

        self.petri_net_generator: PetriNetGenerator = None
        self.petri_net_logic: PetriNetLogic = None

        if self.mfdl_program:
            self.petri_net_generator = PetriNetGenerator(draw_petri_net)
            if use_test_ids:
                self.petri_net_generator.test_id_counter = 0

            self.register_for_petrinet_callbacks()

            petri_net, transition_dict, task_ids = self.petri_net_generator.generate_petri_net(
                self.mfdl_program, get_startable_tasks(self.mfdl_program), ""
            )

            self.petri_net_logic: PetriNetLogic = PetriNetLogic(
                petri_net, transition_dict, task_ids, draw_petri_net
            )

            self.awaited_events["general"] = []
            for task in self.mfdl_program.tasks:
                self.awaited_events[task] = []

            self.set_timer_for_time_instances()
            self.active_ordersteps = self.petri_net_generator.ordersteps

    def register_for_petrinet_callbacks(self) -> None:
        callbacks = self.petri_net_generator.callbacks
        callbacks.materialflow_started_callback = self.on_materialflow_started
        callbacks.started_by_callback = self.on_started_by
        callbacks.task_started_callback = self.on_task_started
        callbacks.order_started_callback = self.on_order_started
        callbacks.waiting_for_move_callback = self.on_waiting_for_move
        callbacks.moved_to_location_callback = self.on_moved_to_location
        callbacks.waiting_for_action_callback = self.on_waiting_for_action
        callbacks.action_executed_callback = self.on_action_executed
        callbacks.order_finished_callback = self.on_order_finished
        callbacks.task_finished_callback = self.on_task_finished
        callbacks.finished_by_callback = self.on_finished_by
        callbacks.materialflow_finished_callback = self.on_materialflow_finished

    def set_timer_for_time_instances(self) -> None:
        time_instances = self.mfdl_program.get_instances("Time")
        for time_instance in time_instances:
            timing = time_instance.attributes["timing"]
            current_time = time.time()
            cron_time = croniter(timing, start_time=datetime.today()).get_next(datetime)
            next_execution = cron_time.timestamp() - current_time
            threading.Timer(next_execution, self.update_time_instance, [time_instance.name]).start()

    def update_time_instance(self, instance_name: str) -> None:
        new_values = {"value": True}
        self.update_instance(instance_name, new_values)

    def mfdl_program_valid(self) -> bool:
        return self.mfdl_program is not None

    def start(self):
        if self.mfdl_program:
            self._is_running = True
            for instance in self.mfdl_program.instances.values():
                instance.attributes["time"] = 0

            for callback_method in self.callbacks.materialflow_started_callbacks:
                callback_method(self.mfdl_program.tasks, self.scheduler_id)

            self.awaited_events["general"].append(Event(event_type="mf_started", data={}))
            self.fire_event(Event(event_type="mf_started", data={}))
        else:
            print("The MFDL file was not valid!")

    def is_running(self) -> bool:
        return self._is_running

    def fire_event(self, event: Union[str, Event]) -> bool:
        """Fires event to petri net"""

        if isinstance(event, str):
            event = Event.from_json(event)
            if not event:
                return False

        if event.event_type == "instance_update":
            return self.update_instance(event.data["instance_name"], event.data["new_values"])

        awaited_events_key = "general"
        if "task" in event.data:
            awaited_events_key = event.data["task"]

        if (
            awaited_events_key in self.awaited_events
            and event in self.awaited_events[awaited_events_key]
        ):
            self.awaited_events[awaited_events_key].remove(event)
            self.petri_net_logic.fire_event(event)
            return True
        return False

    def update_instance(self, instance_name: str, new_values: Dict) -> bool:
        """Updates the given instance with the given values.

        The new values should contain key value pairs (name of the attribute and the value).
        It is possible that the attribute refers to another instance so it is also possible
        that the value is the JSON definition of an instance.

        Returns:
            True if the instance_name refers to a known instance and the values where valid.
        """
        if instance_name in self.mfdl_program.instances:
            current_time = time.time()
            self.mfdl_program.instances[instance_name].attributes["time"] = (
                current_time - self.starting_time
            )
            for attribute, value in new_values.items():
                self.mfdl_program.instances[instance_name].attributes[attribute] = value

            # we updated an instance so it could be possible that an expression is now satisfied
            # in the following, reevaluate the expressions for all active tasks and ordersteps

            for task_name in self.active_tasks:
                task = self.mfdl_program.tasks[task_name]

                started_by_event = Event(event_type="started_by", data={"task": task.name})
                finished_by_event = Event(event_type="finished_by", data={"task": task.name})
                if task.started_by_expr and evaluate_expression(
                    task.started_by_expr, self.mfdl_program.instances, self.mfdl_program.rules
                ):
                    self.fire_event(started_by_event)
                if task.finished_by_expr and evaluate_expression(
                    task.finished_by_expr, self.mfdl_program.instances, self.mfdl_program.rules
                ):
                    self.fire_event(finished_by_event)

            for orderstep_api in self.active_ordersteps:
                if orderstep_api.orderstep.started_by_expr and evaluate_expression(
                    orderstep_api.orderstep.started_by_expr,
                    self.mfdl_program.instances,
                    self.mfdl_program.rules,
                ):
                    self.fire_event(
                        Event(
                            event_type="started_by",
                            data={"task": task.name, "orderstep": orderstep_api.id},
                        )
                    )
                if orderstep_api.orderstep.finished_by_expr and evaluate_expression(
                    orderstep_api.orderstep.finished_by_expr,
                    self.mfdl_program.instances,
                    self.mfdl_program.rules,
                ):
                    self.fire_event(
                        Event(
                            event_type="finished_by",
                            data={"task": task.name, "orderstep": orderstep_api.id},
                        )
                    )
            self.on_instance_updated(instance_name, new_values)
            return True
        else:
            return False

    def on_materialflow_started(self, tasks: List[Task]) -> None:
        for task in tasks:
            self.on_task_started(task)

    def on_task_started(self, task: Task) -> None:
        self.active_tasks.append(task.name)
        for callback_method in self.callbacks.task_started_callbacks:
            callback_method(task, self.scheduler_id)

    def on_order_started(
        self, order: Union[TransportOrder, MoveOrder, ActionOrder], task_name: str
    ) -> None:
        for callback_method in self.callbacks.order_started_callback:
            callback_method(order.uuid_, order, self.scheduler_id)

    def on_started_by(self, task: Task, orderstep_api: OrderStepAPI = None) -> None:
        # StartedBy for a Task
        if orderstep_api is None:
            awaited_event = Event("started_by", {"task": task.name})
            self.awaited_events[task.name].append(awaited_event)
            if evaluate_expression(
                task.started_by_expr, self.mfdl_program.instances, self.mfdl_program.rules
            ):
                self.fire_event(awaited_event)
            else:
                # only call cb functions if StartedBy is not satisfied
                for callback_method in self.callbacks.started_by_callbacks:
                    callback_method(self.scheduler_id)
        # StartedBy for an OrderStep
        else:
            awaited_event = Event("started_by", {"task": task.name, "orderstep": orderstep_api.id})
            self.awaited_events[task.name].append(awaited_event)
            if evaluate_expression(
                orderstep_api.orderstep.started_by_expr,
                self.mfdl_program.instances,
                self.mfdl_program.rules,
            ):
                self.fire_event(awaited_event)
            else:
                # only call functions if StartedBy is not satisfied
                for callback_method in self.callbacks.started_by_callbacks:
                    callback_method(self.scheduler_id)

    def on_finished_by(
        self,
        order: Union[TransportOrder, MoveOrder, ActionOrder],
        orderstep_api: OrderStepAPI,
        task: Task,
    ) -> None:
        awaited_event = Event("finished_by", {"task": task.name, "orderstep": orderstep_api.id})
        self.awaited_events[task.name].append(awaited_event)
        if evaluate_expression(
            orderstep_api.orderstep.finished_by_expr,
            self.mfdl_program.instances,
            self.mfdl_program.rules,
        ):
            self.fire_event(awaited_event)
        else:
            # only call cb functions if FinishedBy is not satisfied
            for callback_method in self.callbacks.finished_by_callbacks:
                callback_method(order, task, self.scheduler_id)

    def on_waiting_for_move(
        self,
        order: Union[TransportOrder, MoveOrder, ActionOrder],
        order_step: OrderStepAPI,
        task: Task,
    ) -> None:
        awaited_event = Event(
            "orderstep_update",
            {"orderstep_id": order_step.id, "task": task.name, "status": "moved_to_location"},
        )
        self.awaited_events[task.name].append(awaited_event)

        for callback_method in self.callbacks.waiting_for_move_callbacks:
            callback_method(order_step.id, order_step)

    def on_moved_to_location(
        self,
        order: Union[TransportOrder, MoveOrder, ActionOrder],
        orderstep_api: OrderStepAPI,
        task: Task,
    ) -> None:
        for callback_method in self.callbacks.moved_to_location_callbacks:
            callback_method(order.uuid_, orderstep_api.orderstep.location_name, self.scheduler_id)

    def on_waiting_for_action(
        self,
        order: Union[TransportOrder, MoveOrder, ActionOrder],
        orderstep_api: OrderStepAPI,
        task: Task,
    ) -> None:
        awaited_event = Event(
            "orderstep_update",
            {"orderstep_id": orderstep_api.id, "task": task.name, "status": "action_executed"},
        )
        self.awaited_events[task.name].append(awaited_event)

        for callback_method in self.callbacks.waiting_for_action_callbacks:
            callback_method(orderstep_api.id, orderstep_api)

    def on_action_executed(
        self,
        order: Union[TransportOrder, MoveOrder, ActionOrder],
        orderstep_api: OrderStepAPI,
        task: Task,
    ) -> None:
        for callback_method in self.callbacks.action_executed_callbacks:
            callback_method(order.uuid_, self.scheduler_id)

    def on_order_finished(self, uuid: str, last_order: bool, task: Task) -> None:
        for callback_method in self.callbacks.order_finished_callbacks:
            callback_method(uuid, self.scheduler_id)

        if last_order:
            awaited_event = Event(event_type="finished_by", data={"task": task.name})
            self.awaited_events[task.name].append(awaited_event)
            if task.finished_by_expr and evaluate_expression(
                task.finished_by_expr, self.mfdl_program.instances, self.mfdl_program.rules
            ):
                self.fire_event(awaited_event)

    def on_instance_updated(self, instance_name: str, data: Dict) -> None:
        for callback_method in self.callbacks.instance_updated_callbacks:
            callback_method(instance_name, data, self.scheduler_id)

    def on_task_finished(self, task_name: str) -> None:
        self.active_tasks.remove(task_name)
        for callback_method in self.callbacks.task_finished_callbacks:
            callback_method(task_name, self.scheduler_id)

    def on_materialflow_finished(self) -> None:
        for callback_method in self.callbacks.materialflow_finished_callbacks:
            callback_method(self.scheduler_id)
        self._is_running = False


def get_startable_tasks(mfdl_program: MFDLProgram) -> List[Task]:
    """Creates a graph where every node is a task  and a directed edge represents an onDone.

    Args:
        mfdl_program: The MFDLProgram for which the startable tasks should be identified.
    """

    tasks = mfdl_program.tasks

    call_graph = nx.DiGraph()
    for task in tasks.values():
        call_graph.add_node(task.name)
        for child in task.follow_up_task_names:
            call_graph.add_edge(task.name, child)

    for orderstep in (
        mfdl_program.transportordersteps.values()
        or mfdl_program.moveordersteps.values()
        or mfdl_program.actionordersteps.values()
    ):
        call_graph.add_node(orderstep.name)
        for child in orderstep.follow_up_task_names:
            call_graph.add_edge(orderstep.name, child)

    startable_tasks = []
    for task_name in tasks:
        incoming_edges = 0
        for u, v in call_graph.in_edges(task_name):
            # ignore self loops
            if u != v:
                incoming_edges = incoming_edges + 1
        if incoming_edges == 0:
            startable_tasks.append(tasks[task_name])

    return startable_tasks
