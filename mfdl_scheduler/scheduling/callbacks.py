# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains MaterialFlowCallbacks class."""

# standard libraries
from typing import Any, Callable, List, Union

# local sources
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.moveorder import MoveOrder
from mfdl_scheduler.model.actionorder import ActionOrder


class MaterialFlowCallbacks:
    """Contains lists of registered callback functions
    for different states in the scheduling process.

    Attributes:
        task_started_callbacks: A list containing registered callback functions called when
            a Task is started.
        order_started_callback: A list containing registered callback functions called when
            an Order is started.
        started_by_callbacks: A list containing registered callback functions called when
            a StartedBy expression is reached in a Task or an OrderStep.
        moved_to_location_callbacks: A list containing registered callback functions called when
            an AGV moved to a location in an Order.
        action_executed_callbacks: A list containing registered callback functions called when
            an AGV executed an action.
        order_finished_callbacks: A list containing registered callback functions called when
            an Order is finished.
        finished_by_callbacks: A list containing registered callback functions called when
            a FinishedBy expression is reached in a Task or an OrderStep.
        task_finished_cb: A list containing registered callback functions called when
            a Task is finished.
        materialflow_finished_cb: A list containing registered callback functions called when
            the Materiaflow is finished.
    """

    def __init__(self) -> None:
        self.materialflow_started_callbacks: List[Callable[[List[Task]], Any]] = []
        self.task_started_callbacks: List[Callable[[Task], Any]] = []
        self.order_started_callback: List[
            Callable[[str, Union[TransportOrder, MoveOrder, ActionOrder], str]]
        ] = []
        self.started_by_callbacks: List[Callable] = []
        self.waiting_for_move_callbacks: List[Callable] = []
        self.moved_to_location_callbacks: List[Callable] = []
        self.waiting_for_action_callbacks: List[Callable] = []
        self.action_executed_callbacks: List[Callable] = []
        self.order_finished_callbacks: List[
            Callable[[str, Union[TransportOrder, MoveOrder, ActionOrder]], Any],
        ] = []
        self.finished_by_callbacks: List[Callable] = []
        self.instance_updated_callbacks: List[Callable] = []
        self.task_finished_callbacks: List[Callable[[Task], Any]] = []
        self.materialflow_finished_callbacks: List[Callable] = []

    def register_callback_materialflow_started(
        self, callback_method: List[Callable[[List[Task]], Any]]
    ) -> None:
        self.materialflow_started_callbacks.append(callback_method)

    def register_callback_task_started(self, callback_method: Callable[[Task], Any]) -> None:
        self.task_started_callbacks.append(callback_method)

    def register_callback_order_started(
        self,
        callback_methdod: Callable[[str, Union[TransportOrder, MoveOrder, ActionOrder]], Any],
    ) -> None:
        self.order_started_callback.append(callback_methdod)

    def register_callback_started_by(self, callback_method: Callable) -> None:
        self.started_by_callbacks.append(callback_method)

    def register_callback_waiting_for_move(self, callback_method: Callable) -> None:
        self.waiting_for_move_callbacks.append(callback_method)

    def register_callback_moved_to_location(self, callback_method: Callable) -> None:
        self.moved_to_location_callbacks.append(callback_method)

    def register_callback_waiting_for_action(self, callback_method: Callable) -> None:
        self.waiting_for_action_callbacks.append(callback_method)

    def register_callback_action_executed(self, callback_method: Callable) -> None:
        self.action_executed_callbacks.append(callback_method)

    def register_callback_order_finished(
        self,
        callback_method: Callable[[str, Union[TransportOrder, MoveOrder, ActionOrder]], Any],
    ):
        self.order_finished_callbacks.append(callback_method)

    def register_callback_finished_by(self, callback_method: Callable):
        self.finished_by_callbacks.append(callback_method)

    def register_callback_instance_updated(self, callback_method: Callable):
        self.instance_updated_callbacks.append(callback_method)

    def register_callback_task_finished(self, callback_method: Callable[[Task], Any]):
        self.task_finished_callbacks.append(callback_method)

    def register_callback_materialflow_finished(self, callback_method: Callable):
        self.materialflow_finished_callbacks.append(callback_method)
