# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains Event class."""

# standard libraries
import json
from typing import Dict, Union


class Event:
    """Data class for controlling the PetriNet instance.

    Attributes:
        event_type: A string representing the type of the event (types above).
        data: A dict containing the corresponding data of the event type.
    """

    def __init__(self, event_type: str = "", data: Dict = None) -> None:
        self.event_type = event_type
        self.data: Dict = data

    def __eq__(self, other: "Event"):
        if not isinstance(other, Event):
            # don't attempt to compare against unrelated types
            return NotImplemented
        return self.event_type == other.event_type and self.data == other.data

    @classmethod
    def from_json(cls, json_string: str) -> Union[None, "Event"]:
        json_dict = json.loads(json_string)
        if "event_type" in json_dict and "data" in json_dict:
            return Event(event_type=json_dict["event_type"], data=json_dict["data"])
        else:
            return None
