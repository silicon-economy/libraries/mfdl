# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains OrderStep class."""

# standard libraries
from typing import Dict, List, Union

# 3rd party libs
from antlr4.ParserRuleContext import ParserRuleContext


class OrderStep:
    """Base class for the OrderSteps in the MFDL.

    Both OrderSteps (ActionOrderStep and TransportOrderStep) have a name and can
    triggered by expressions.

    Attributes:
        name: A string representing the name of the OrderStep.
        started_by_expr: The StartedBy expression represented as Dict.
        finished_by_expr: The FinishedBy expression represented as Dict.
        follow_up_task_name: A string representing the name of a possible follow up task.
    """

    def __init__(
        self,
        name: str = "",
        started_by_expr: Union[str, Dict] = None,
        finished_by_expr: Union[str, Dict] = None,
        follow_up_task_names: List[str] = None,
        context: ParserRuleContext = None,
    ) -> None:
        self.name: str = name

        self.started_by_expr: Union[str, Dict] = started_by_expr
        self.finished_by_expr: Union[str, Dict] = finished_by_expr

        if follow_up_task_names:
            self.follow_up_task_names: List[str] = follow_up_task_names
        else:
            self.follow_up_task_names: List[str] = []

        self.context: ParserRuleContext = context
