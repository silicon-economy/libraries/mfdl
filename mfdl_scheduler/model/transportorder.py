# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains TransportOrder class."""

# standard libraries
from typing import List
from uuid import uuid4

# 3rd party libs
from antlr4.ParserRuleContext import ParserRuleContext

from mfdl_scheduler.model.transportorderstep import TransportOrderStep


class TransportOrder:
    """Represents a TransportOrder (Transport statement) in the MFDL.

    Attributes:
        uuid_:
        pickup_tos_names: A List of strings representing the pickup location, parameters and logic.
        delivery_tos_name: A TransportOrderStep instance representing
                           the delivery location, parameters and logic.
    """

    def __init__(
        self,
        pickup_tos_names: List[str] = None,
        pickup_tos: List[TransportOrderStep] = None,
        delivery_tos_name: str = "",
        delivery_tos: TransportOrderStep = None,
        context: ParserRuleContext = None,
    ) -> None:
        self.uuid_: str = uuid4().__str__()
        if pickup_tos_names:
            self.pickup_tos_names: List[str] = pickup_tos_names
        else:
            self.pickup_tos_names: List[str] = []

        if pickup_tos:
            self.pickup_tos: List[str] = pickup_tos
        else:
            self.pickup_tos: List[str] = []

        self.delivery_tos_name: str = delivery_tos_name
        self.delivery_tos: TransportOrderStep = delivery_tos

        self.context: ParserRuleContext = context
