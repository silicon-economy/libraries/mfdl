# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains Module class."""

# standard libraries
from typing import Dict

# 3rd party libs
from antlr4.ParserRuleContext import ParserRuleContext
from mfdl_scheduler.model.moveorderstep import MoveOrderStep

# local sources
from mfdl_scheduler.model.struct import Struct
from mfdl_scheduler.model.instance import Instance
from mfdl_scheduler.model.rule import Rule
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.transportorderstep import TransportOrderStep
from mfdl_scheduler.model.actionorderstep import ActionOrderStep


class Module:
    """Represents a Module in the MFDL.

    Attributes:
        structs: A dict containing all structs defined in the MFDL file.
        instances: A dict containing all instances defined in the MFDL file.
        rules: A dict containing all rules defined in the MFDL file.
        tasks: A dict containing all tasks defined in the MFDL file.
        transportordersteps: A dict containing all transportordersteps defined in the MFDL file.
        moveordersteps: A dict containing all moveordersteps defined in the MFDL file.
        actionordersteps: A dict containing all actionordersteps defined in the MFDL file.
    """

    def __init__(
        self,
        name: str = "",
        structs: Dict[str, Struct] = None,
        instances: Dict[str, Instance] = None,
        rules: Dict[str, Rule] = None,
        tasks: Dict[str, Task] = None,
        transportordersteps: Dict[str, TransportOrderStep] = None,
        moveordersteps: Dict[str, MoveOrderStep] = None,
        actionordersteps: Dict[str, ActionOrderStep] = None,
        context: ParserRuleContext = None,
    ) -> None:
        self.name: str = name
        if structs:
            self.structs: Dict[str, Struct] = structs
        else:
            self.structs: Dict[str, Struct] = {}

        if instances:
            self.instances: Dict[str, Instance] = instances
        else:
            self.instances: Dict[str, Instance] = {}

        if rules:
            self.rules: Dict[str, Rule] = rules
        else:
            self.rules: Dict[str, Rule] = {}

        if tasks:
            self.tasks: Dict[str, Task] = tasks
        else:
            self.tasks: Dict[str, Task] = {}

        if transportordersteps:
            self.transportordersteps: Dict[str, TransportOrderStep] = transportordersteps
        else:
            self.transportordersteps: Dict[str, TransportOrderStep] = {}

        if moveordersteps:
            self.moveordersteps: Dict[str, MoveOrderStep] = moveordersteps
        else:
            self.moveordersteps: Dict[str, MoveOrderStep] = {}

        if actionordersteps:
            self.actionordersteps: Dict[str, ActionOrderStep] = actionordersteps
        else:
            self.actionordersteps: Dict[str, ActionOrderStep] = {}

        self.context: ParserRuleContext = context
