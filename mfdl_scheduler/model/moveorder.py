# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains MoveOrder class."""

# 3rd party libs
from uuid import uuid4
from antlr4.ParserRuleContext import ParserRuleContext
from mfdl_scheduler.model.moveorderstep import MoveOrderStep


class MoveOrder:
    """Represents a MoveOrder (Move statement) in the MFDL.

    Attributes:
        moveorderstep: A string representing the name of a TransportOrderStep
                            which is representing the location, parameters and logic of the order.
    """

    def __init__(
        self,
        moveorderstep_name: str = "",
        moveorderstep: MoveOrderStep = None,
        context: ParserRuleContext = None,
    ) -> None:
        self.uuid_: str = uuid4().__str__()
        self.moveorderstep_name: str = moveorderstep_name
        self.moveorderstep: MoveOrderStep = moveorderstep
        self.context: ParserRuleContext = context
