# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains Task class."""

# standard libraries
from typing import Dict, List, Union
from xmlrpc.client import Transport

# 3rd party libs
from antlr4.ParserRuleContext import ParserRuleContext

# local sources
from mfdl_scheduler.model.actionorder import ActionOrder
from mfdl_scheduler.model.actionorderstep import ActionOrderStep
from mfdl_scheduler.model.moveorder import MoveOrder

from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.transportorderstep import TransportOrderStep


class Task:
    """Represents a Task in the MFDL.

    A Task contains statements (orders) which are executed sequentially.
    It is also possible to define optional statements like StartedBy, FinishedBy,
    Constraints, Repeat or OnDone.

    Attributes:
        name: A string representing the name of the Task.
        statements: List of statements (orders) which are executed sequentially.
        started_by_expr: The StartedBy expression represented as Dict.
        finished_by_expr: The FinishedBy expression represented as Dict.
        constraints: Constraints to the execution of a Task in form of a expression (Dict).
        repeat: An integer representing the number of repetitions of the task.
        follow_up_task_name: A string representing the name of a possible follow up task.
    """

    def __init__(
        self,
        name: str = "",
        statements: List[Union[TransportOrder, MoveOrder, ActionOrder]] = None,
        started_by_expr: Union[str, Dict] = None,
        finished_by_expr: Union[str, Dict] = None,
        constraints: Union[str, Dict] = None,
        constraints_string: str = "",
        repeat: int = None,
        follow_up_task_names: List[str] = None,
        context: ParserRuleContext = None,
    ) -> None:
        self.name: str = name

        if statements:
            self.statements: List[Union[TransportOrder, MoveOrder, ActionOrder]] = statements
        else:
            self.statements: List[Union[TransportOrder, MoveOrder, ActionOrder]] = []

        self.started_by_expr: Union[str, Dict] = started_by_expr
        self.finished_by_expr: Union[str, Dict] = finished_by_expr

        if constraints:
            self.constraints: Union[str, Dict] = constraints
        else:
            self.constraints: Union[str, Dict] = {}

        self.constraints_string = constraints_string
        self.repeat: int = repeat

        if follow_up_task_names:
            self.follow_up_task_names: List[str] = follow_up_task_names
        else:
            self.follow_up_task_names: List[str] = []

        self.context: ParserRuleContext = context
        self.context_dict: Dict = {}

    def get_ordersteps(self) -> List[Union[TransportOrderStep, ActionOrderStep]]:
        ordersteps = []
        for statement in self.statements:
            if isinstance(statement, TransportOrder):
                ordersteps.extend(statement.pickup_tos)
                ordersteps.append(statement.delivery_tos)
            elif isinstance(statement, MoveOrder):
                ordersteps.append(statement.moveorderstep)
            else:
                ordersteps.append(statement.actionorderstep)
        return ordersteps
