# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains ActionOrder class."""

# 3rd party libs
from uuid import uuid4
from antlr4.ParserRuleContext import ParserRuleContext

from mfdl_scheduler.model.actionorderstep import ActionOrderStep


class ActionOrder:
    """Represents a ActionOrder (Action statement) in the MFDL.

    Attributes:
        actionorderstepname: A string representing the name of an ActionOrderStep
                                which is representing the parameters and logic of the order.
    """

    def __init__(
        self,
        actionorderstep_name: str = "",
        actionorderstep: ActionOrderStep = None,
        context: ParserRuleContext = None,
    ) -> None:
        self.uuid_: str = uuid4().__str__()
        self.actionorderstepname: str = actionorderstep_name
        self.actionorderstep = actionorderstep
        self.context: ParserRuleContext = context
