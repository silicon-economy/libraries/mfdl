# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains MoveOrderStep class."""

# standard libraries
from typing import Dict, List, Union

# 3rd party libs
from antlr4.ParserRuleContext import ParserRuleContext

# local sources
from mfdl_scheduler.model.orderstep import OrderStep
from mfdl_scheduler.model.instance import Instance


class MoveOrderStep(OrderStep):
    """Represents a MoveOrderStep in the MFDL.

    Inherited from OrderStep. Adds parameters and location.

    Attributes:
        name: A string representing the name of the MoveOrderStep.
        started_by_expr: The StartedBy expression represented as Dict.
        finished_by_expr: The FinishedBy expression represented as Dict.
        parameters: A single string as parameter or a Dict of parameters of the OrderStep.
        location_name: A string representing the location name this
                       MoveOrderStep is assigned to.
        follow_up_task_name: A string representing the name of a possible follow up task.
    """

    def __init__(
        self,
        name: str = "",
        started_by_expr: Union[str, Dict] = None,
        finished_by_expr: Union[str, Dict] = None,
        parameters: Union[str, Dict] = None,
        location_name: str = "",
        location: Instance = None,
        follow_up_task_names: List[str] = None,
        context: ParserRuleContext = None,
    ) -> None:
        super().__init__(name, started_by_expr, finished_by_expr, follow_up_task_names)

        self.parameters: Union[str, Dict] = parameters
        self.location_name: str = location_name
        self.location: Instance = location
        self.context: ParserRuleContext = context
        self.context_dict: Dict = {}
