# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains ActionOrderStep class."""

# standard libraries
from typing import Dict, List, Union

# 3rd party libs
from antlr4.ParserRuleContext import ParserRuleContext

# local sources
from mfdl_scheduler.model.orderstep import OrderStep


class ActionOrderStep(OrderStep):
    """Represents a ActionOrderStep in the MFDL.

    Inherited from OrderStep. Adds parameters.

    Attributes:
        name: A string representing the name of the TransportOrderStep.
        started_by_expr: The StartedBy expression represented as Dict.
        finished_by_expr: The FinishedBy expression represented as Dict.
        parameters: A single string as parameter or a Dict of parameters of the OrderStep.
        follow_up_task_name: A string representing the name of a possible follow up task.
    """

    def __init__(
        self,
        name: str = "",
        started_by_expr: Union[str, Dict] = None,
        finished_by_expr: Union[str, Dict] = None,
        parameters: Union[str, Dict] = None,
        follow_up_task_names: List[str] = None,
        context: ParserRuleContext = None,
    ) -> None:
        super().__init__(name, started_by_expr, finished_by_expr, follow_up_task_names)

        self.parameters: Dict = parameters
        self.context: ParserRuleContext = context
        self.context_dict: Dict = {}
