# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains Struct class."""

# standard libraries
from typing import Dict

# 3rd party libs
from antlr4.ParserRuleContext import ParserRuleContext


class Struct:
    """Represents a Struct in the MFDL.

    Attributes:
        name: A string representing the name of the Struct.
        attributes: A dict mapping the attribute name to the given type.
        parent_struct: A Struct object which represents the parent of this Struct (inheritance).
        context: The ANTLR context object for the Struct itself
        attribute_contexts: A dict that maps the attribute names to their ANTLR contexts.
    """

    def __init__(
        self,
        name: str = "",
        attributes: Dict[str, str] = None,
        parent_struct_name: str = "",
        context: ParserRuleContext = None,
    ) -> None:
        self.name: str = name
        if attributes:
            self.attributes: Dict[str, str] = attributes
        else:
            self.attributes: Dict[str, str] = {}

        # add attributes that exist in every struct by default
        self.attributes["id"] = "string"
        self.attributes["time"] = "number"

        self.parent_struct_name: str = parent_struct_name
        self.context = context

        self.attribute_contexts: Dict = {}
        self.attribute_contexts["id"] = None
        self.attribute_contexts["time"] = None
