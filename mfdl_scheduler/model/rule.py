# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains Rule class."""

# standard libraries
from typing import Dict, List

# 3rd party libs
from antlr4.ParserRuleContext import ParserRuleContext


class Rule:
    """Represents a Rule in the MFDL.

    Attributes:
        name: A string representing the name of the rule.
        parameters: A Dict which is mapping the parameter names to possible values.
        expressions: A list containing expressions defined in a Rule.
    """

    def __init__(
        self,
        name: str = "",
        parameters: Dict = None,
        expressions: List[Dict] = None,
        context: ParserRuleContext = None,
    ) -> None:
        self.name: str = name

        if parameters:
            self.parameters: Dict = parameters
        else:
            self.parameters: Dict = {}

        if expressions:
            self.expressions: List[Dict] = expressions
        else:
            self.expressions: List[Dict] = []

        self.context: ParserRuleContext = context
