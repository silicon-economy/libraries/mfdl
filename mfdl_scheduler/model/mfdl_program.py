# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains the MFDLProgram class."""

# standard libraries
from typing import Dict, List, Union

# local sources
from mfdl_scheduler.model.struct import Struct
from mfdl_scheduler.model.instance import Instance
from mfdl_scheduler.model.module import Module
from mfdl_scheduler.model.rule import Rule
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.transportorderstep import TransportOrderStep
from mfdl_scheduler.model.moveorderstep import MoveOrderStep
from mfdl_scheduler.model.actionorderstep import ActionOrderStep


class MFDLProgram:
    """This class represents a MFDL program and is the root of the model.

    Attributes:
        structs: A dict containing all structs defined in the MFDL file.
        instances: A dict containing all instances defined in the MFDL file.
        modules: A dict containing all modules defined in the MFDL file.
        rules: A dict containing all rules defined in the MFDL file.
        tasks: A dict containing all tasks defined in the MFDL file.
        transportordersteps: A dict containing all transportordersteps defined in the MFDL file.
        moveordersteps: A dict containing all moveordersteps defined in the MFDL file.
        actionordersteps: A dict containing all actionordersteps defined in the MFDL file.
    """

    def __init__(
        self,
        structs: Dict[str, Struct] = None,
        instances: Dict[str, Instance] = None,
        modules: Dict[str, Module] = None,
        rules: Dict[str, Rule] = None,
        tasks: Dict[str, Task] = None,
        transportordersteps: Dict[str, TransportOrderStep] = None,
        moveordersteps: Dict[str, MoveOrderStep] = None,
        actionordersteps: Dict[str, ActionOrderStep] = None,
    ) -> None:
        if structs:
            self.structs: Dict[str, Struct] = structs
        else:
            self.structs: Dict[str, Struct] = {}

        if instances:
            self.instances: Dict[str, Instance] = instances
        else:
            self.instances: Dict[str, Instance] = {}

        if modules:
            self.modules: Dict[str, Module] = modules
        else:
            self.modules: Dict[str, Module] = {}

        if rules:
            self.rules: Dict[str, Rule] = rules
        else:
            self.rules: Dict[str, Rule] = {}

        if tasks:
            self.tasks: Dict[str, Task] = tasks
        else:
            self.tasks: Dict[str, Task] = {}

        if transportordersteps:
            self.transportordersteps: Dict[str, TransportOrderStep] = transportordersteps
        else:
            self.transportordersteps: Dict[str, TransportOrderStep] = {}

        if moveordersteps:
            self.moveordersteps: Dict[str, MoveOrderStep] = moveordersteps
        else:
            self.moveordersteps: Dict[str, MoveOrderStep] = {}

        if actionordersteps:
            self.actionordersteps: Dict[str, ActionOrderStep] = actionordersteps
        else:
            self.actionordersteps: Dict[str, ActionOrderStep] = {}

    def append_mfdl_program_or_module_content(
        self, mfdl_program_or_module: Union["MFDLProgram", Module]
    ) -> None:
        """Adds the content of the given MFDL program to this one."""
        if mfdl_program_or_module:
            self.structs.update(mfdl_program_or_module.structs)
            self.instances.update(mfdl_program_or_module.instances)
            self.rules.update(mfdl_program_or_module.rules)
            self.tasks.update(mfdl_program_or_module.tasks)
            self.transportordersteps.update(mfdl_program_or_module.transportordersteps)
            self.moveordersteps.update(mfdl_program_or_module.moveordersteps)
            self.actionordersteps.update(mfdl_program_or_module.actionordersteps)

    def get_instances(self, type_of_instance: str) -> List[Instance]:
        """Returns a List of all instance that has the given type.

        The returned list contains all instances of the given type and all
        child types.
        """

        instances = []
        for instance in self.instances.values():
            struct_name = instance.struct_name
            parent_struct = ""
            while struct_name != "":
                parent_struct = struct_name
                struct_name = self.structs[struct_name].parent_struct_name

                if parent_struct == type_of_instance:
                    instances.append(instance)
                    break

        return instances
