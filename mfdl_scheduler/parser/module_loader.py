# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains ModuleLoader class."""

# standard libraries
from typing import List, Tuple, Union
from os.path import exists

# 3rd party libs
from antlr4.CommonTokenStream import CommonTokenStream
from antlr4.ParserRuleContext import ParserRuleContext
from antlr4.InputStream import InputStream
from mfdl_scheduler.model.module import Module

# local sources
from mfdl_scheduler.parser.MFDLLexer import MFDLLexer
from mfdl_scheduler.parser.MFDLParser import MFDLParser

from mfdl_scheduler.helpers import load_file
from mfdl_scheduler.parser import mfdl_visitor
from mfdl_scheduler.validation.error_handler import ErrorHandler


class ModuleLoader:
    """This class is used to load MFDL modules from a given MFDL file.

    Logic for importing modules is defined in this class. Import statements can refer to other
    files and the modules in them.
    The modules content should not refer to elements outside the module block. The ModuleLoader
    loads only the given Module block from the Import statement.

    The ModuleLoader will try to load the modules starting from the relative path of
    the given file. If there are no modules found  it will try to load from the dir
    where the scheduler is executed.

    Attributes:
        folder_path: A string representing the folder path of the MFDL file
                     from which the modules should be loaded.
    """

    def __init__(self, file_path: str, error_handler: ErrorHandler):
        if file_path.find("/") != -1 and len(file_path) >= file_path.rfind("/") + 1:
            self.folder_path: str = file_path[: file_path.rfind("/") + 1]
        else:
            self.folder_path: str = ""

        self.error_handler: ErrorHandler = error_handler

    def load_modules(
        self, paths_and_names: List[Tuple[str, str]], context: ParserRuleContext
    ) -> List[Module]:
        """Tries to load the modules from the given list of tuples consisting of path and name.

        Returns:
            The succesfully loaded Modules or an empty List if no MFDL files were found.
        """
        modules = []

        # Find modules with same path so dont load the file multiple times
        paths = {}
        for path, module_name in paths_and_names:
            if path not in paths:
                paths[path] = [module_name]
            else:
                paths[path].append(module_name)

        for module_path, module_names in paths.items():
            mfdl_string = self.load_module_file(module_path)
            if mfdl_string:
                lexer = MFDLLexer(InputStream(mfdl_string))
                token_stream = CommonTokenStream(lexer)
                parser = MFDLParser(token_stream)
                visitor = mfdl_visitor.MFDLVisitor(
                    module_path, self.error_handler, load_modules_only=True
                )
                mfdl_program = visitor.visitProgram(parser.program())

                for module_name in module_names:
                    if module_name not in mfdl_program.modules:
                        error_msg = f"Module '{module_name}' could not be found in the loaded file."
                        self.error_handler.print_error(error_msg, context=context)
                    else:
                        modules.append(mfdl_program.modules[module_name])
            else:
                error_msg = f"Failed to import file '{module_path}'."
                self.error_handler.print_error(error_msg, context=context)
        return modules

    def load_module_file(self, module_path: str) -> Union[str, None]:
        """Tries to load the file containg the module, starting from the directory of
        the scheduler. If no MFDL file is found this method searches at the relative path
        of the MFDL file from which the modules should be loaded.

        Returns:
            The content of the MFDL file as string or None if no file was found.
        """
        full_path = module_path + ".mfdl"
        if exists(full_path):
            return load_file(full_path)

        relative_path = self.folder_path + full_path
        if exists(relative_path):
            return load_file(relative_path)
        return None
