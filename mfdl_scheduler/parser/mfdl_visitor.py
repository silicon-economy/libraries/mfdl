# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains MFDLVisitor class."""

# standard libraries
import uuid
import json
from numbers import Number
from typing import Dict, List, Tuple, Union

# 3rd party
from antlr4.tree.Tree import TerminalNodeImpl

# local sources
from mfdl_scheduler.model.mfdl_program import MFDLProgram
from mfdl_scheduler.model.struct import Struct
from mfdl_scheduler.model.instance import Instance
from mfdl_scheduler.model.module import Module
from mfdl_scheduler.model.rule import Rule
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.transportorderstep import TransportOrderStep
from mfdl_scheduler.model.moveorder import MoveOrder
from mfdl_scheduler.model.moveorderstep import MoveOrderStep
from mfdl_scheduler.model.actionorder import ActionOrder
from mfdl_scheduler.model.actionorderstep import ActionOrderStep

from mfdl_scheduler.parser.MFDLParserVisitor import MFDLParserVisitor
from mfdl_scheduler.parser.MFDLParser import MFDLParser
from mfdl_scheduler.parser.module_loader import ModuleLoader

from mfdl_scheduler.helpers import import_list_to_path
from mfdl_scheduler.helpers import is_string
from mfdl_scheduler.helpers import is_int
from mfdl_scheduler.helpers import is_float

from mfdl_scheduler.validation.error_handler import ErrorHandler


class MFDLVisitor(MFDLParserVisitor):
    """Traverses the given syntax tree and store program information in a MFDLProgram object.

    This class overrides the generated visitor methods from the ANTLR generated
    MFDLParserVisitor. A MFDLProgram object is created and gets filled while traversing
    the syntax tree. The visitor assumes that the given syntax tree is from a valid MFDL program.
    If the tree from an invalid program is passed it will most likely fail.

    Attributes:
        current_program_component: A reference to the currently traversed program component.
        file_path: A string representing the file path of the parsed MFDL file.
        load_modules_only: A boolean indiciating if only the modules
                           should be loaded (used for importing functionality).
    """

    def __init__(
        self,
        file_path: str,
        error_handler: ErrorHandler,
        load_modules_only: bool = False,
    ) -> None:
        super().__init__()
        self.current_program_component = None
        self.load_modules_only: bool = load_modules_only
        self.file_path: str = file_path
        self.error_handler: ErrorHandler = error_handler

    def visitProgram(self, ctx: MFDLParser.ProgramContext) -> MFDLProgram:
        """Starts the visiting of the syntax tree of the given MFDL program."""
        mfdl_program = MFDLProgram()

        if ctx.children:
            for child in ctx.children:
                if not isinstance(child, TerminalNodeImpl):  # a new line token
                    if isinstance(child, MFDLParser.Import_stmtContext):
                        modules = self.visitImport_stmt(child)
                        for module in modules:
                            mfdl_program.append_mfdl_program_or_module_content(module)
                    else:
                        program_component = self.visit(child)

                        if isinstance(program_component, Module):
                            if program_component.name not in mfdl_program.modules:
                                mfdl_program.modules[program_component.name] = program_component
                            else:
                                error_msg = f"A Module with the name '{program_component.name}' was already defined."
                                self.error_handler.print_error(
                                    error_msg, context=program_component.context
                                )
                        elif self.load_modules_only is False:
                            self.process_mfdl_component(program_component, mfdl_program)

        self.second_pass(mfdl_program)

        return mfdl_program

    def second_pass(self, mfdl_program: MFDLProgram):
        """Iterate over model objects to set missing data.

        Some data can only be set if we iterate over the generated model objects.
        """
        for task in mfdl_program.tasks.values():
            for statement in task.statements:
                if isinstance(statement, TransportOrder):
                    pickup_tos = []
                    for pickup_tos_name in statement.pickup_tos_names:
                        if pickup_tos_name in mfdl_program.transportordersteps:
                            pickup_tos.append(mfdl_program.transportordersteps[pickup_tos_name])
                    statement.pickup_tos = pickup_tos
                    if statement.delivery_tos_name in mfdl_program.transportordersteps:
                        statement.delivery_tos = mfdl_program.transportordersteps[
                            statement.delivery_tos_name
                        ]
                elif isinstance(statement, MoveOrder):
                    if statement.moveorderstep_name in mfdl_program.moveordersteps:
                        statement.moveorderstep = mfdl_program.moveordersteps[
                            statement.moveorderstep_name
                        ]
                else:  # ActionOrder
                    if statement.actionorderstepname in mfdl_program.actionordersteps:
                        statement.actionorderstep = mfdl_program.actionordersteps[
                            statement.actionorderstepname
                        ]
        for orderstep in (
            mfdl_program.transportordersteps.values() or mfdl_program.moveordersteps.values()
        ):
            if orderstep.location_name in mfdl_program.instances:
                orderstep.location = mfdl_program.instances[orderstep.location_name]
            else:
                # TODO print error and dont check it in SemanticErrorChecker anymore
                pass

    def process_mfdl_component(
        self,
        program_component: Union[Struct, Instance, Rule, Task, TransportOrderStep, ActionOrderStep],
        mfdl_program_or_module: Union[MFDLProgram, Module],
    ) -> None:
        """Checks the type of the program component (except modules) and add its to
        the corresponding dict inside the MFDLProgram or Module object."""
        components = {
            "Struct": mfdl_program_or_module.structs,
            "Instance": mfdl_program_or_module.instances,
            "Rule": mfdl_program_or_module.rules,
            "Task": mfdl_program_or_module.tasks,
            "TransportOrderStep": mfdl_program_or_module.transportordersteps,
            "MoveOrderStep": mfdl_program_or_module.moveordersteps,
            "ActionOrderStep": mfdl_program_or_module.actionordersteps,
        }

        component_type = program_component.__class__.__name__
        if program_component.name not in components[component_type]:
            components[component_type][program_component.name] = program_component
        else:
            error_msg = f"A '{component_type}' with the name '{program_component.name}' was already defined."
            self.error_handler.print_error(error_msg, context=program_component.context)

    def visitImport_stmt(self, ctx: MFDLParser.Import_stmtContext) -> List[Module]:
        """Get the module names and try to load and parse the content of the files.

        Parse the new files and concatanate the generated syntax tree to the already existing tree.

        Returns:
            A list of Modules that were successfully loaded.
        """
        module_loader = ModuleLoader(self.file_path, self.error_handler)
        module_paths_and_names = []
        for module_path_context in ctx.module_path():
            import_list = self.visitModule_path(module_path_context)
            module_path = import_list_to_path(import_list[:-1])
            module_name = import_list[-1]
            module_paths_and_names.append((module_path, module_name))

        modules = module_loader.load_modules(module_paths_and_names, ctx)
        return modules

    def visitModule_path(self, ctx: MFDLParser.Module_pathContext) -> List[str]:
        import_list = []
        for module in ctx.ID():
            import_list.append(module.getText())
        return import_list

    def visitStruct(self, ctx: MFDLParser.StructContext) -> Struct:
        struct_name = self.visitStruct_id(ctx.struct_id()[0])
        parent_struct_name = ""
        if len(ctx.struct_id()) == 2:
            parent_struct_name = self.visitStruct_id(ctx.struct_id()[1])

        struct = Struct(name=struct_name, parent_struct_name=parent_struct_name, context=ctx)
        self.current_program_component = struct
        for attribute_definition_ctx in ctx.attribute_definition():
            attribute_name, attribute_type = self.visitAttribute_definition(
                attribute_definition_ctx
            )
            struct.attributes[attribute_name] = attribute_type
            struct.attribute_contexts[attribute_name] = attribute_definition_ctx
        return struct

    def visitStruct_id(self, ctx: MFDLParser.Struct_idContext) -> str:
        return ctx.children[0].getText()

    def visitInstance(self, ctx: MFDLParser.InstanceContext) -> Instance:
        instance_name = ctx.ID().getText()
        struct_name = self.visitStruct_id(ctx.struct_id())
        instance = Instance(name=instance_name, struct_name=struct_name, context=ctx)
        self.current_program_component = instance

        for attribute_assignment_ctx in ctx.attribute_assignment():
            attribute_name, attribute_value = self.visitAttribute_assignment(
                attribute_assignment_ctx
            )
            # JSON value
            if isinstance(attribute_value, Dict):
                attribute_value = Instance.from_json(attribute_value, self.error_handler, ctx)
            instance.attributes[attribute_name] = attribute_value
            instance.attribute_contexts[attribute_name] = attribute_assignment_ctx

        # set default values for attributes that exists in every struct if they was not set
        if "time" not in instance.attributes:
            instance.attributes["time"] = 0
        if "id" not in instance.attributes:
            instance.attributes["id"] = str(uuid.uuid4())
        return instance

    def visitRule_(self, ctx: MFDLParser.Rule_Context) -> Rule:
        rule_name, rule_parameters = self.visitRule_call(ctx.rule_call())
        rule = Rule(name=rule_name, parameters=rule_parameters, context=ctx)
        self.current_program_component = rule
        for expression_context in ctx.expression():
            expression = self.visitExpression(expression_context)
            rule.expressions.append(expression)
        return rule

    def visitRule_call(
        self, ctx: MFDLParser.Rule_callContext
    ) -> Tuple[str, Dict[str, Union[None, str]]]:
        rule_name = ctx.ID().getText()
        rule_parameter = {}
        for rule_param_ctx in ctx.rule_parameter():
            parameter_name, parameter_value = self.visitRule_parameter(rule_param_ctx)
            if parameter_name in rule_parameter:
                error_msg = f"The parameter '{parameter_name}' was already defined."
                self.error_handler.print_error(error_msg, context=ctx)
            else:
                rule_parameter[parameter_name] = parameter_value
        return (rule_name, rule_parameter)

    def visitRule_parameter(
        self, ctx: MFDLParser.Rule_parameterContext
    ) -> Tuple[str, Union[None, str]]:
        parameter = ""
        if ctx.ID():
            parameter = ctx.ID().getText()
        else:
            parameter = self.visitValue(ctx.value()[0])

        default_value = None
        if len(ctx.children) == 3:  # 3, because of the '='
            default_value = self.visitValue(ctx.children[2])

        return (parameter, default_value)

    def visitModule(self, ctx: MFDLParser.ModuleContext) -> Module:
        module = Module(name=ctx.ID().getText(), context=ctx)
        self.current_program_component = module
        if ctx.children:
            for child in ctx.children:
                if not isinstance(child, TerminalNodeImpl):  # a new line token
                    program_component = self.visit(child)
                    self.process_mfdl_component(program_component, module)
        return module

    def visitTask(self, ctx: MFDLParser.TaskContext) -> Task:
        task = Task(name=ctx.ID().getText(), context=ctx)
        self.current_program_component = task
        for statement_ctx in ctx.taskStatement():
            self.visitTaskStatement(statement_ctx)
        return task

    def visitTaskStatement(self, ctx: MFDLParser.TaskStatementContext) -> None:
        if ctx.transportStatement():
            transport_order = self.visitTransportStatement(ctx.transportStatement())
            self.current_program_component.statements.append(transport_order)
        if ctx.moveStatement():
            move_order = self.visitMoveStatement(ctx.moveStatement())
            self.current_program_component.statements.append(move_order)
        if ctx.actionStatement():
            action_order = self.visitActionStatement(ctx.actionStatement())
            self.current_program_component.statements.append(action_order)
        if ctx.optStatement():
            self.visitOptStatement(ctx.optStatement())
        if ctx.repeatStatement():
            self.current_program_component.repeat = self.visitRepeatStatement(ctx.repeatStatement())
        if ctx.constraintStatement():
            constraints, constraints_string = self.visitConstraintStatement(
                ctx.constraintStatement()
            )
            self.current_program_component.constraints = constraints
            self.current_program_component.constraints_string = constraints_string

    def visitTransportStatement(self, ctx: MFDLParser.TransportStatementContext) -> TransportOrder:
        transport_order = TransportOrder(
            pickup_tos_names=self.visitFromStatement(ctx.fromStatement()),
            delivery_tos_name=self.visitToStatement(ctx.toStatement()),
            context=ctx,
        )
        return transport_order

    def visitFromStatement(self, ctx: MFDLParser.FromStatementContext) -> List[str]:
        pickup_tos_names = []
        for tos_id in ctx.ID():
            pickup_tos_names.append(tos_id.getText())
        return pickup_tos_names

    def visitToStatement(self, ctx: MFDLParser.ToStatementContext) -> str:
        return ctx.ID().getText()

    def visitMoveStatement(self, ctx: MFDLParser.MoveStatementContext) -> MoveOrder:
        return MoveOrder(ctx.ID().getText(), context=ctx)

    def visitActionStatement(self, ctx: MFDLParser.ActionStatementContext) -> ActionOrder:
        return ActionOrder(ctx.ID().getText(), context=ctx)

    def visitOptStatement(self, ctx: MFDLParser.OptStatementContext):
        if ctx.eventStatement():
            self.visitEventStatement(ctx.eventStatement())
        else:
            on_done_task_names = self.visitOnDoneStatement(ctx.onDoneStatement())
            self.current_program_component.follow_up_task_names = on_done_task_names

    def visitRepeatStatement(self, ctx: MFDLParser.RepeatStatementContext) -> str:
        return ctx.INTEGER().getText()

    def visitConstraintStatement(
        self, ctx: MFDLParser.ConstraintStatementContext
    ) -> Tuple[Union[None, str, Dict], str]:
        constraints = None
        self.current_program_component.context_dict["Constraints"] = ctx

        if ctx.expression():
            constraints = self.visitExpression(ctx.expression())
            return (constraints, ctx.expression().getText())
        else:
            constraints = self.visitJson_object(ctx.json_object())
            return (constraints, ctx.json_object().getText())

    def visitEventStatement(self, ctx: MFDLParser.EventStatementContext) -> None:
        expression = self.visitExpression(ctx.expression())
        if ctx.STARTED_BY():
            self.current_program_component.context_dict["StartedBy"] = ctx
            self.current_program_component.started_by_expr = expression
        else:
            self.current_program_component.context_dict["FinishedBy"] = ctx
            self.current_program_component.finished_by_expr = expression

    def visitOnDoneStatement(self, ctx: MFDLParser.OnDoneStatementContext) -> List[str]:
        on_done_task_names = []
        self.current_program_component.context_dict["OnDone"] = ctx
        for on_done_task_name in ctx.ID():
            on_done_task_names.append(on_done_task_name.getText())
        return on_done_task_names

    def visitOrderStep(
        self, ctx: MFDLParser.OrderStepContext
    ) -> Union[ActionOrderStep, TransportOrderStep]:
        if ctx.transportOrderStep():
            return self.visitTransportOrderStep(ctx.transportOrderStep())
        elif ctx.moveOrderStep():
            return self.visitMoveOrderStep(ctx.moveOrderStep())
        return self.visitActionOrderStep(ctx.actionOrderStep())

    def visitTransportOrderStep(
        self, ctx: MFDLParser.TransportOrderStepContext
    ) -> TransportOrderStep:
        transport_order_step = TransportOrderStep(name=ctx.ID().getText(), context=ctx)
        self.current_program_component = transport_order_step
        for statement_ctx in ctx.tosStatement():
            self.visitTosStatement(statement_ctx)
        return transport_order_step

    def visitTosStatement(self, ctx: MFDLParser.TosStatementContext) -> None:
        if ctx.locationStatement():
            location_name = self.visitLocationStatement(ctx.locationStatement())
            self.current_program_component.location_name = location_name
        if ctx.parameterStatement():
            parameters = self.visitParameterStatement(ctx.parameterStatement())
            self.current_program_component.parameters = parameters
        if ctx.optStatement():
            self.visitOptStatement(ctx.optStatement())

    def visitLocationStatement(self, ctx: MFDLParser.LocationStatementContext) -> str:
        self.current_program_component.context_dict["Location"] = ctx
        return ctx.ID().getText()

    def visitParameterStatement(
        self, ctx: MFDLParser.ParameterStatementContext
    ) -> Union[str, Dict]:
        self.current_program_component.context_dict["Parameter"] = ctx
        if ctx.value():
            return self.visitValue(ctx.value())
        return self.visitJson_object(ctx.json_object())

    def visitMoveOrderStep(self, ctx: MFDLParser.MoveOrderStepContext) -> MoveOrderStep:
        move_order_step = MoveOrderStep(name=ctx.ID().getText(), context=ctx)
        self.current_program_component = move_order_step
        for statement_ctx in ctx.mosStatement():
            self.visitMosStatement(statement_ctx)
        return move_order_step

    def visitMosStatement(self, ctx: MFDLParser.MosStatementContext) -> None:
        if ctx.locationStatement():
            location = self.visitLocationStatement(ctx.locationStatement())
            self.current_program_component.location = location
        if ctx.optStatement():
            self.visitOptStatement(ctx.optStatement())

    def visitActionOrderStep(self, ctx: MFDLParser.ActionOrderStepContext) -> ActionOrderStep:
        action_order_step = ActionOrderStep(name=ctx.ID().getText(), context=ctx)
        self.current_program_component = action_order_step
        for statement_ctx in ctx.aosStatement():
            self.visitAosStatement(statement_ctx)
        return action_order_step

    def visitAosStatement(self, ctx: MFDLParser.AosStatementContext) -> None:
        if ctx.parameterStatement():
            parameters = self.visitParameterStatement(ctx.parameterStatement())
            self.current_program_component.parameters = parameters
        if ctx.optStatement():
            self.visitOptStatement(ctx.optStatement())

    def visitAttribute_definition(
        self, ctx: MFDLParser.Attribute_definitionContext
    ) -> Tuple[str, str]:
        return (ctx.ID().getText(), self.visitPrimitive(ctx.primitive()))

    def visitAttribute_assignment(
        self, ctx: MFDLParser.Attribute_assignmentContext
    ) -> Tuple[List[str], Union[str, Dict]]:
        value = None
        if ctx.value():
            value = self.visitValue(ctx.value())
        else:
            value = self.visitJson_object(ctx.json_object())

        return (
            self.visitAttribute_access(ctx.attribute_access()),
            value,
        )

    def visitAttribute_access(self, ctx: MFDLParser.Attribute_accessContext) -> str:
        return ctx.getText()

    def visitPrimitive(self, ctx: MFDLParser.PrimitiveContext) -> str:
        return ctx.children[0].getText()

    def visitValue(self, ctx: MFDLParser.ValueContext) -> Union[str, Number, bool]:
        value = ctx.children[0].getText()
        if is_string(value):
            return value.replace('"', "")
        if is_int(value):
            return int(value)
        if is_float(value):
            return float(value)
        if value in ["True", "False"]:
            return value == "True"
        return value

    def visitExpression(self, ctx: MFDLParser.ExpressionContext) -> Union[str, Dict]:
        length = len(ctx.children)

        if length == 1:
            ele = self.get_content(ctx.children[0])
            return ele
        if length == 2:
            un_op = self.get_content(ctx.children[0])
            ele = self.get_content(ctx.children[1])
            return dict(unOp=un_op, value=ele)

        if length == 3:
            left = self.get_content(ctx.children[0])
            bin_op = self.get_content(ctx.children[1])
            right = self.get_content(ctx.children[2])
            return dict(binOp=bin_op, left=left, right=right)

        return None

    def visitBinOperation(self, ctx: MFDLParser.BinOperationContext) -> str:
        return ctx.children[0].getText()

    def visitUnOperation(self, ctx: MFDLParser.UnOperationContext) -> str:
        return ctx.children[0].getText()

    def get_content(self, child):
        ele = self.visit(child)

        if ele is None:
            ele = child.getText()
        return ele

    def visitJson_object(self, ctx: MFDLParser.Json_objectContext) -> Union[Dict, None]:
        """Returns the parsed JSON object."""
        try:
            return json.loads(ctx.getText())
        except ValueError:
            print(
                "Possible error in the grammar specification! The JSON string to parse"
                + " should be valid at this point of the parsing process."
            )
            raise
