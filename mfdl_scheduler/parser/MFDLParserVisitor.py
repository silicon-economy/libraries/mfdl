# Generated from MFDLParser.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .MFDLParser import MFDLParser
else:
    from MFDLParser import MFDLParser

# This class defines a complete generic visitor for a parse tree produced by MFDLParser.

class MFDLParserVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by MFDLParser#program.
    def visitProgram(self, ctx:MFDLParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#import_stmt.
    def visitImport_stmt(self, ctx:MFDLParser.Import_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#module_path.
    def visitModule_path(self, ctx:MFDLParser.Module_pathContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#struct.
    def visitStruct(self, ctx:MFDLParser.StructContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#struct_id.
    def visitStruct_id(self, ctx:MFDLParser.Struct_idContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#instance.
    def visitInstance(self, ctx:MFDLParser.InstanceContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#rule_.
    def visitRule_(self, ctx:MFDLParser.Rule_Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#rule_call.
    def visitRule_call(self, ctx:MFDLParser.Rule_callContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#rule_parameter.
    def visitRule_parameter(self, ctx:MFDLParser.Rule_parameterContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#module.
    def visitModule(self, ctx:MFDLParser.ModuleContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#task.
    def visitTask(self, ctx:MFDLParser.TaskContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#taskStatement.
    def visitTaskStatement(self, ctx:MFDLParser.TaskStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#transportStatement.
    def visitTransportStatement(self, ctx:MFDLParser.TransportStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#fromStatement.
    def visitFromStatement(self, ctx:MFDLParser.FromStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#toStatement.
    def visitToStatement(self, ctx:MFDLParser.ToStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#moveStatement.
    def visitMoveStatement(self, ctx:MFDLParser.MoveStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#actionStatement.
    def visitActionStatement(self, ctx:MFDLParser.ActionStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#constraintStatement.
    def visitConstraintStatement(self, ctx:MFDLParser.ConstraintStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#repeatStatement.
    def visitRepeatStatement(self, ctx:MFDLParser.RepeatStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#orderStep.
    def visitOrderStep(self, ctx:MFDLParser.OrderStepContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#transportOrderStep.
    def visitTransportOrderStep(self, ctx:MFDLParser.TransportOrderStepContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#tosStatement.
    def visitTosStatement(self, ctx:MFDLParser.TosStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#locationStatement.
    def visitLocationStatement(self, ctx:MFDLParser.LocationStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#parameterStatement.
    def visitParameterStatement(self, ctx:MFDLParser.ParameterStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#optStatement.
    def visitOptStatement(self, ctx:MFDLParser.OptStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#eventStatement.
    def visitEventStatement(self, ctx:MFDLParser.EventStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#onDoneStatement.
    def visitOnDoneStatement(self, ctx:MFDLParser.OnDoneStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#moveOrderStep.
    def visitMoveOrderStep(self, ctx:MFDLParser.MoveOrderStepContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#mosStatement.
    def visitMosStatement(self, ctx:MFDLParser.MosStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#actionOrderStep.
    def visitActionOrderStep(self, ctx:MFDLParser.ActionOrderStepContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#aosStatement.
    def visitAosStatement(self, ctx:MFDLParser.AosStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#attribute_definition.
    def visitAttribute_definition(self, ctx:MFDLParser.Attribute_definitionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#attribute_assignment.
    def visitAttribute_assignment(self, ctx:MFDLParser.Attribute_assignmentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#attribute_access.
    def visitAttribute_access(self, ctx:MFDLParser.Attribute_accessContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#primitive.
    def visitPrimitive(self, ctx:MFDLParser.PrimitiveContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#value.
    def visitValue(self, ctx:MFDLParser.ValueContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#expression.
    def visitExpression(self, ctx:MFDLParser.ExpressionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#binOperation.
    def visitBinOperation(self, ctx:MFDLParser.BinOperationContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#unOperation.
    def visitUnOperation(self, ctx:MFDLParser.UnOperationContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#json_object.
    def visitJson_object(self, ctx:MFDLParser.Json_objectContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#pair.
    def visitPair(self, ctx:MFDLParser.PairContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#json_open_bracket.
    def visitJson_open_bracket(self, ctx:MFDLParser.Json_open_bracketContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#json_value.
    def visitJson_value(self, ctx:MFDLParser.Json_valueContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MFDLParser#json_array.
    def visitJson_array(self, ctx:MFDLParser.Json_arrayContext):
        return self.visitChildren(ctx)



del MFDLParser