# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains PetriNetLogic class."""

# standard libraries
from typing import Dict, OrderedDict, Union

# 3rd party packages
from nets import PetriNet, Value

# local sources
from mfdl_scheduler.scheduling.event import Event

from mfdl_scheduler.petri_net.drawer import draw_petri_net


class PetriNetLogic:
    """Provides methods for interacting with the generated petri nets for scheduling.

    Scheduling of the production process with the help of the
    generated petri nets is done in this class.

    Attributes:
        petri_net_generator: A PetriNetGenerator instance corresponding to the scheduled MFDL file.
        image_path: A string representing the path were the PetriNetImage will be saved.
        petri_net: The PetriNet instance from the PetriNetGenerator instance.
        transition_dict: A reference to the transition_dict from the PetriNetGenerator instance.
    """

    def __init__(
        self,
        petri_net: PetriNet,
        transition_dict: OrderedDict,
        task_ids: Dict[str, Dict[str, Union[str, Dict]]],
        draw_petri_net: bool = True,
    ) -> None:
        self.petri_net = petri_net
        self.transition_dict: Dict = transition_dict
        self.task_ids = task_ids
        self.draw_pn: bool = draw_petri_net

    def evaluate_petri_net(self) -> None:
        """Tries to fire every transition as long as all transitions
        were tried and nothing can be done anymore."""
        index = 0

        transitions = list(self.petri_net._trans)
        while index < len(self.petri_net._trans):
            transition_id = transitions[index]
            transition_fired = False

            try:
                self.petri_net.transition(transition_id).fire(Value(1))
                transition_fired = True
            except ValueError:
                transition_fired = False

            if transition_fired:
                if transition_id in self.transition_dict:
                    callbacks = self.transition_dict[transition_id]
                    for callback in callbacks:
                        callback()  # run functools.partials() function here

                # we fired a transition so the net is in a new state
                index = 0
            else:
                index = index + 1

        if self.draw_pn:
            draw_petri_net(self.petri_net, "Materialflow")

    def fire_event(self, event: Event):
        """Adds a token to the corresponding place of the event in the petri net."""

        name_in_petri_net = ""
        if event.event_type == "mf_started":
            name_in_petri_net = "mf_started"
        elif event.data and "task" in event.data:
            # it is a task specific event
            task_name = event.data["task"]
            if event.event_type == "started_by":
                if "orderstep" in event.data:
                    orderstep = event.data["orderstep"]
                    name_in_petri_net = self.task_ids[task_name][orderstep]["started_by"]
                else:
                    name_in_petri_net = self.task_ids[task_name]["started_by"]
            elif event.event_type == "finished_by":
                if "orderstep" in event.data:
                    orderstep_name = event.data["orderstep"]
                    name_in_petri_net = self.task_ids[task_name][orderstep_name]["finished_by"]
                else:
                    name_in_petri_net = self.task_ids[task_name]["finished_by"]
            elif event.event_type == "orderstep_update":
                orderstep_id = event.data["orderstep_id"]
                agv_status = event.data["status"]
                name_in_petri_net = self.task_ids[task_name][orderstep_id][agv_status]

        if self.petri_net.has_place(name_in_petri_net):
            self.petri_net.place(name_in_petri_net).add(1)

            if self.draw_pn:
                draw_petri_net(self.petri_net, "Materialflow")
            self.evaluate_petri_net()
