# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains PetriNetCallbacks class."""

# standard libraries
from typing import Any, Callable, List, Union

# local sources
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.moveorder import MoveOrder
from mfdl_scheduler.model.actionorder import ActionOrder


class PetriNetCallbacks:
    """#TODO"""

    def __init__(self) -> None:
        self.materialflow_started_callback: Callable[[List[Task]], Any] = None
        self.task_started_callback: Callable[[Task], Any] = None
        self.order_started_callback: Callable[
            [str, Union[TransportOrder, MoveOrder, ActionOrder], str]
        ] = None
        self.started_by_callback: Callable = None
        self.waiting_for_move_callback: Callable = None
        self.moved_to_location_callback: Callable = None
        self.waiting_for_action_callback: Callable = None
        self.action_executed_callback: Callable = None
        self.order_finished_callback: Callable[
            [str, Union[TransportOrder, MoveOrder, ActionOrder]], Any
        ] = None
        self.orders_finished_callback: Callable = None
        self.finished_by_callback: Callable = None
        self.instance_updated_callback: Callable = None
        self.task_finished_callback: Callable[[Task], Any] = None
        self.materialflow_finished_callback: Callable = None
