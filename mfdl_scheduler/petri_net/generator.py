# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains the PetriNetGenerator class."""

# standard libraries
from typing import Callable, Dict, List, OrderedDict, Tuple, Union
import uuid
import functools

# 3rd party packages
from snakes import plugins
from mfdl_scheduler.api.orderstep_api import OrderStepAPI

from mfdl_scheduler.model.actionorder import ActionOrder
from mfdl_scheduler.model.actionorderstep import ActionOrderStep

# local sources
from mfdl_scheduler.model.mfdl_program import MFDLProgram
from mfdl_scheduler.model.moveorder import MoveOrder
from mfdl_scheduler.model.moveorderstep import MoveOrderStep
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.transportorderstep import TransportOrderStep

from mfdl_scheduler.petri_net.drawer import draw_petri_net
from mfdl_scheduler.petri_net.callbacks import PetriNetCallbacks

plugins.load(["labels", "gv"], "snakes.nets", "nets")
from nets import PetriNet, Place, Transition, Value


class PetriNetGenerator:
    """Takes a MFDL file as argument and generates a Petri Net.

    Attributes:
        path_for_image: The path where the image of the generated Petri Net is saved.
        mfdl_program: The MFDLProgram instance from which the Petri Net is generated.
        petri_net: The corresponding Snakes PetriNet instance of the MFDLProgram.
        transition_dict: A dict for mapping the UUIDs of the Transitions to their behavior.
        task_ids: A dict for mapping the Task name to the ids of places and transitions in the PetriNet.
        orderstep_to_finish_transitions: A dict for mapping a tos name to all finish transitions of all
                                   ActionOrderStep in each Task.
        on_done_transition_dict: A dict for mapping the follow up Taks name to its transition.
    """

    def __init__(self, draw_petri_net: bool = True) -> None:
        self.mfdl_program: MFDLProgram = None
        self.petri_net = PetriNet("Materialflow")
        self.transition_dict: OrderedDict = OrderedDict()
        self.task_ids: Dict[str, Dict[str, Union[str, Dict]]] = {}
        self.name_to_finish_transition_id: Dict[str, str] = {}
        self.orderstep_to_finish_transitions: Dict[str, List[str]] = {}
        self.on_done_transition_dict: Dict[str, str] = {}
        self.callbacks = PetriNetCallbacks()
        self.draw_petri_net: bool = draw_petri_net
        self.ordersteps: List[OrderStepAPI] = []
        self.test_id_counter: int = -1

    def add_callback(self, transition_id: str, callback_function: Callable, *args):
        """Registers the given callback function in the transition_dict.

        If the transition the transition_id refers to is fired, the callback function
        will be called with the given arguments inside the PetriNetLogic class.

        Args:
            transition_id: The UUID of the transition where the callback is called if fired.
            callback_function: The callback function which should be called.
            *args: Arguments with which the callback function is called.
        """
        callback = functools.partial(callback_function, *args)
        if transition_id not in self.transition_dict:
            self.transition_dict[transition_id] = []

        self.transition_dict[transition_id].append(callback)

    def generate_petri_net(
        self, mfdl_program: MFDLProgram, startable_tasks: List[Task], image_path: str = ""
    ) -> Tuple[PetriNet, OrderedDict, Dict[str, Dict[str, Union[str, Dict]]]]:
        """Generates a Petri Net from the given MFDLProgram object.

        Returns:
            A PetriNet instance.
        """

        self.petri_net = PetriNet("Materialflow")
        self.mfdl_program = mfdl_program

        materialflow_started_id = create_place(
            "Materialflow \n started", self.petri_net, "mf_started"
        )

        parallel_tasks_branch_id = create_transition("", "", self.petri_net)
        self.petri_net.add_input(materialflow_started_id, parallel_tasks_branch_id, Value(1))

        self.add_callback(
            parallel_tasks_branch_id, self.callbacks.materialflow_started_callback, startable_tasks
        )

        parallel_tasks_sync_id = create_transition("", "", self.petri_net)

        self.add_callback(parallel_tasks_sync_id, self.callbacks.materialflow_finished_callback)

        materialflow_finished_id = create_place(
            "Materialflow \n finished", self.petri_net, "mf_finished"
        )
        self.petri_net.add_output(materialflow_finished_id, parallel_tasks_sync_id, Value(1))

        for task in mfdl_program.tasks.values():
            # check only for the startable tasks
            if task in startable_tasks and task.started_by_expr is not None:
                self.add_callback(
                    parallel_tasks_branch_id, self.callbacks.started_by_callback, task
                )
            task_started_id, task_finished_id = self.generate_task(task)
            if task in startable_tasks:
                self.petri_net.add_output(task_started_id, parallel_tasks_branch_id, Value(1))
            if not task.follow_up_task_names:
                self.petri_net.add_input(task_finished_id, parallel_tasks_sync_id, Value(1))

        for task in mfdl_program.tasks.values():
            self.generate_on_done(task)
        for orderstep in self.ordersteps:
            self.generate_on_done(orderstep)

        if self.draw_petri_net:
            draw_petri_net(self.petri_net, image_path + "Materialflow")

        return (self.petri_net, self.transition_dict, self.task_ids)

    def generate_task(self, task: Task) -> Tuple[str, str]:
        """Generate Petri Net components for the given Task.

        Generate places for StartedBy and FinishedBy if they are defined in the Task.
        Iterate over the statements in the Task and call the generate_statements method
        for each statement. Make sure to also connect the different components with each
        other via connections (transitions).

        Returns:
            A tuple consisting of the starting and finish place id.
        """
        self.task_ids[task.name] = {}
        starting_places = []
        end_places = []

        task_started_id = create_place(task.name + "\n started", self.petri_net)

        starting_places.append(task_started_id)
        task_finished_id = ""

        if not task.follow_up_task_names:
            task_finished_id = create_place(task.name + "\n finished", self.petri_net)
            end_places.append(task_finished_id)

        if task.started_by_expr is not None:
            started_by_id = create_place("StartedBy \n satisfied", self.petri_net)
            self.task_ids[task.name]["started_by"] = started_by_id
            starting_places.append(started_by_id)

        last_transition_id = self.generate_orders(
            task, task.statements, starting_places, end_places
        )

        if task.finished_by_expr is not None:
            finished_by_id = create_place("FinishedBy \n satisfied", self.petri_net)
            self.task_ids[task.name]["finished_by"] = finished_by_id
            self.petri_net.add_input(finished_by_id, last_transition_id, Value(1))

        self.add_callback(last_transition_id, self.callbacks.task_finished_callback, task.name)
        self.task_ids[task.name]["starting_place"] = task_started_id
        self.task_ids[task.name]["last_transition"] = last_transition_id

        return (task_started_id, task_finished_id)

    def generate_orders(
        self,
        task: Task,
        statements: List,
        start_places: List[str],
        end_places: List[str],
    ) -> str:
        """Generate Petri Net components for each order statement in the given Task

        Iterate over the Orders of the given Tasks and generate the corresponding
        Petri Net components. Connect the individual components with each other via a
        transition.

        Returns:
            The id of the last connection (transition).
        """

        last_place_id = ""
        for i, statement in enumerate(statements):
            connection_id = create_transition("connection", "", self.petri_net)

            # Connect passed places with first connection transition
            # otherwise connect the new connection with the last place from the last order
            if i == 0:
                for start_place in start_places:
                    self.petri_net.add_input(start_place, connection_id, Value(1))
            else:
                self.petri_net.add_input(last_place_id, connection_id, Value(1))

            args = (statement, task.name)
            self.add_callback(connection_id, self.callbacks.order_started_callback, *args)

            last_statement = False
            if i == len(statements) - 1:
                last_statement = True

            ids = None
            if isinstance(statement, TransportOrder):
                ids = self.generate_transport_order(statement, task, last_statement)
            elif isinstance(statement, MoveOrder):
                ids = self.generate_move_order(statement, task, last_statement)
            else:
                ids = self.generate_action_order(statement, task, last_statement)

            first_place_id, last_place_id = ids
            self.petri_net.add_output(first_place_id, connection_id, Value(1))

        last_connection_id = create_transition("connection", "", self.petri_net)
        self.petri_net.add_input(last_place_id, last_connection_id, Value(1))
        for end_point in end_places:
            self.petri_net.add_output(end_point, last_connection_id, Value(1))

        return last_connection_id

    def generate_transport_order(
        self, transport_order: TransportOrder, task: Task, last_statement: bool = False
    ) -> Tuple[str, str]:
        """Generate the Petri Net components for a TransportOrder.

        Returns:
            A tuple consisting of the ids of the TransportOrder started and finished place.
        """

        transport_started_id = create_place("Transport \n started", self.petri_net)
        branch_transition_id = create_transition("", "", self.petri_net)
        sync_transition_id = create_transition("", "", self.petri_net)

        self.petri_net.add_input(transport_started_id, branch_transition_id, Value(1))

        for pickup_tos_name in transport_order.pickup_tos_names:
            tos = self.mfdl_program.transportordersteps[pickup_tos_name]
            tos_api = OrderStepAPI(tos)
            if self.test_id_counter != -1:
                tos_api.id = str(self.test_id_counter)
                self.test_id_counter = self.test_id_counter + 1

            self.ordersteps.append(tos_api)
            ids = self.generate_transport_order_step(tos_api, transport_order, task)

            if tos.started_by_expr is not None:
                args = (task, tos_api)
                self.add_callback(branch_transition_id, self.callbacks.started_by_callback, *args)
            else:
                args = (transport_order, tos_api, task)
                self.add_callback(
                    branch_transition_id, self.callbacks.waiting_for_move_callback, *args
                )

            tos_started_id, tos_finished_id, last_transition_id = ids
            self.petri_net.add_output(tos_started_id, branch_transition_id, Value(1))
            self.petri_net.add_input(tos_finished_id, sync_transition_id, Value(1))

        tos = self.mfdl_program.transportordersteps[transport_order.delivery_tos_name]
        tos_api = OrderStepAPI(tos)
        if self.test_id_counter != -1:
            tos_api.id = str(self.test_id_counter)
            self.test_id_counter = self.test_id_counter + 1

        self.ordersteps.append(tos_api)

        if tos.started_by_expr is not None:
            args = (task, tos_api)
            self.add_callback(sync_transition_id, self.callbacks.started_by_callback, *args)
        else:
            args = (transport_order, tos_api, task)
            self.add_callback(sync_transition_id, self.callbacks.waiting_for_move_callback, *args)

        tos_started_id, tos_finished_id, last_transition_id = self.generate_transport_order_step(
            tos_api, transport_order, task
        )
        self.petri_net.add_output(tos_started_id, sync_transition_id, Value(1))

        args = (transport_order.uuid_, last_statement, task)
        self.add_callback(last_transition_id, self.callbacks.order_finished_callback, *args)

        return (transport_started_id, tos_finished_id)

    def generate_transport_order_step(
        self,
        tos_api: OrderStepAPI,
        order: Union[TransportOrder, MoveOrder, ActionOrder],
        task: Task,
    ) -> Tuple[str, str, str]:
        """Generate the Petri Net components for a TransportOrderStep.

        Returns:
            A tuple consisting of the ids of the TransportOrderStep started and finished place
            and the id of the last transition.
        """
        tos = tos_api.orderstep

        tos_started_id = create_place(tos.name + "\n started", self.petri_net)
        tos_finished_id = create_place(tos.name + "\n finished", self.petri_net)

        first_transition_id = create_transition("", "", self.petri_net)
        moved_to_location_id = create_place(f"Moved to \n {tos.location_name}", self.petri_net)

        self.petri_net.add_input(tos_started_id, first_transition_id, Value(1))
        self.petri_net.add_input(moved_to_location_id, first_transition_id, Value(1))

        cb_args = (order, tos_api, task)
        self.add_callback(first_transition_id, self.callbacks.moved_to_location_callback, *cb_args)

        action_executed_id = ""
        last_transition_id = first_transition_id

        if tos.parameters:
            waiting_for_action_id = create_place("Waiting for action", self.petri_net)
            action_executed_id = create_place("Action executed", self.petri_net)

            last_transition_id = create_transition("", "", self.petri_net)

            self.petri_net.add_output(waiting_for_action_id, first_transition_id, Value(1))
            self.petri_net.add_input(waiting_for_action_id, last_transition_id, Value(1))
            self.petri_net.add_input(action_executed_id, last_transition_id, Value(1))

            self.add_callback(
                first_transition_id, self.callbacks.waiting_for_action_callback, *cb_args
            )
            self.add_callback(last_transition_id, self.callbacks.action_executed_callback, *cb_args)

        # check if there are StartedBy or FinishedBy statements and if so, generate components
        started_by_id = ""
        finished_by_id = ""
        if tos.started_by_expr is not None:
            started_by_id, tos_started_id, start_transition_id = self.generate_started_by(
                tos_started_id
            )

            self.add_callback(
                start_transition_id, self.callbacks.waiting_for_move_callback, *cb_args
            )

        if tos.finished_by_expr is not None:
            finished_by_id, finished_by_transition_id = self.generate_finished_by(
                last_transition_id, tos_finished_id
            )

            self.add_callback(last_transition_id, self.callbacks.finished_by_callback, *cb_args)

            last_transition_id = finished_by_transition_id  # update last transition
        else:
            self.petri_net.add_output(tos_finished_id, last_transition_id, Value(1))

        if tos_api.id not in self.orderstep_to_finish_transitions:
            self.orderstep_to_finish_transitions[tos_api.id] = []
        self.orderstep_to_finish_transitions[tos_api.id].append(last_transition_id)

        self.task_ids[task.name][tos_api.id] = {
            "moved_to_location": moved_to_location_id,
            "action_executed": action_executed_id,
            "started_by": started_by_id,
            "finished_by": finished_by_id,
        }

        return (tos_started_id, tos_finished_id, last_transition_id)

    def generate_move_order(
        self, move_order: MoveOrder, task: Task, last_statement: bool = False
    ) -> Tuple[str, str]:
        """Generate the Petri Net components for a MoveOrder.

        Returns:
            A tuple consisting of the ids of the MoveOrder started and finished place.
        """
        mos = self.mfdl_program.moveordersteps[move_order.moveorderstep_name]
        mos_api = OrderStepAPI(mos)
        if self.test_id_counter != -1:
            mos_api.id = str(self.test_id_counter)
            self.test_id_counter = self.test_id_counter + 1

        self.ordersteps.append(mos_api)

        move_started_id = create_place("Move \n started", self.petri_net)
        first_transition_id = create_transition("", "", self.petri_net)

        self.petri_net.add_input(move_started_id, first_transition_id, Value(1))

        mos_started_id, mos_finished_id, mos_last_transition_id = self.generate_move_order_step(
            mos_api, move_order, task
        )

        if mos.started_by_expr is not None:
            args = (task, mos_api)
            self.add_callback(first_transition_id, self.callbacks.started_by_callback, *args)
        else:
            args = (move_order, mos_api, task)
            self.add_callback(first_transition_id, self.callbacks.waiting_for_move_callback, *args)

        self.petri_net.add_output(mos_started_id, first_transition_id, Value(1))

        args = (move_order.uuid_, last_statement, task)
        self.add_callback(mos_last_transition_id, self.callbacks.order_finished_callback, *args)

        return (move_started_id, mos_finished_id)

    def generate_move_order_step(self, mos_api: OrderStepAPI, order, task):
        mos = mos_api.orderstep

        mos_started_id = create_place(mos.name + "\n started", self.petri_net)
        mos_finished_id = create_place(mos.name + "\n finished", self.petri_net)

        first_transition_id = create_transition("", "", self.petri_net)
        moved_to_location_id = create_place(f"Moved to \n {mos.location_name}", self.petri_net)

        self.petri_net.add_input(mos_started_id, first_transition_id, Value(1))
        self.petri_net.add_input(moved_to_location_id, first_transition_id, Value(1))

        cb_args = (order, mos_api, task)
        self.add_callback(first_transition_id, self.callbacks.moved_to_location_callback, *cb_args)

        last_transition_id = first_transition_id

        # check if there are StartedBy or FinishedBy statements and if so, generate components
        started_by_id = ""
        finished_by_id = ""
        if mos.started_by_expr is not None:
            started_by_id, mos_started_id, start_transition_id = self.generate_started_by(
                mos_started_id
            )

            self.add_callback(
                start_transition_id, self.callbacks.waiting_for_move_callback, *cb_args
            )

        if mos.finished_by_expr is not None:
            finished_by_id, finished_by_transition_id = self.generate_finished_by(
                last_transition_id, mos_finished_id
            )

            self.add_callback(last_transition_id, self.callbacks.finished_by_callback, *cb_args)

            last_transition_id = finished_by_transition_id  # update last transition
        else:
            self.petri_net.add_output(mos_finished_id, last_transition_id, Value(1))

        if mos_api.id not in self.orderstep_to_finish_transitions:
            self.orderstep_to_finish_transitions[mos_api.id] = []
        self.orderstep_to_finish_transitions[mos_api.id].append(last_transition_id)

        self.task_ids[task.name][mos_api.id] = {
            "moved_to_location": moved_to_location_id,
            "started_by": started_by_id,
            "finished_by": finished_by_id,
        }

        return (mos_started_id, mos_finished_id, last_transition_id)

    def generate_action_order(
        self, action_order: ActionOrder, task: Task, last_statement: bool = False
    ) -> Tuple[str, str]:
        """Generate the Petri Net components for an ActionOrder.

        Returns:
            A tuple consisting of the ids of the ActionOrder started and finished place.
        """
        aos = self.mfdl_program.actionordersteps[action_order.actionorderstepname]
        aos_api = OrderStepAPI(aos)
        if self.test_id_counter != -1:
            aos_api.id = str(self.test_id_counter)
            self.test_id_counter = self.test_id_counter + 1
        self.ordersteps.append(aos_api)

        action_started_id = create_place("Action \n started", self.petri_net)
        first_transition_id = create_transition("", "", self.petri_net)

        self.petri_net.add_input(action_started_id, first_transition_id, Value(1))

        aos_started_id, aos_finished_id, aos_last_transition_id = self.generate_action_order_step(
            aos_api, action_order, task
        )

        if aos.started_by_expr is not None:
            args = (task, aos_api)
            self.add_callback(first_transition_id, self.callbacks.started_by_callback, *args)
        else:
            args = (action_order, aos_api, task)
            self.add_callback(
                first_transition_id, self.callbacks.waiting_for_action_callback, *args
            )

        self.petri_net.add_output(aos_started_id, first_transition_id, Value(1))

        args = (action_order.uuid_, last_statement, task)
        self.add_callback(aos_last_transition_id, self.callbacks.order_finished_callback, *args)

        return (action_started_id, aos_finished_id)

    def generate_action_order_step(self, aos_api: OrderStepAPI, order, task):
        aos = aos_api.orderstep

        aos_started_id = create_place(aos.name + "\n started", self.petri_net)
        aos_finished_id = create_place(aos.name + "\n finished", self.petri_net)

        first_transition_id = create_transition("", "", self.petri_net)
        action_executed_id = create_place("Action executed", self.petri_net)

        self.petri_net.add_input(aos_started_id, first_transition_id, Value(1))
        self.petri_net.add_input(action_executed_id, first_transition_id, Value(1))

        cb_args = (order, aos_api, task)
        self.add_callback(first_transition_id, self.callbacks.action_executed_callback, *cb_args)

        last_transition_id = first_transition_id

        # check if there are StartedBy or FinishedBy statements and if so, generate components
        started_by_id = ""
        finished_by_id = ""
        if aos.started_by_expr is not None:
            started_by_id, aos_started_id, start_transition_id = self.generate_started_by(
                aos_started_id
            )

            self.add_callback(
                start_transition_id, self.callbacks.waiting_for_action_callback, *cb_args
            )

        if aos.finished_by_expr is not None:
            finished_by_id, finished_by_transition_id = self.generate_finished_by(
                last_transition_id, aos_finished_id
            )

            self.add_callback(last_transition_id, self.callbacks.finished_by_callback, *cb_args)

            last_transition_id = finished_by_transition_id  # update last transition
        else:
            self.petri_net.add_output(aos_finished_id, last_transition_id, Value(1))

        if aos_api.id not in self.orderstep_to_finish_transitions:
            self.orderstep_to_finish_transitions[aos_api.id] = []
        self.orderstep_to_finish_transitions[aos_api.id].append(last_transition_id)

        self.task_ids[task.name][aos_api.id] = {
            "action_executed": action_executed_id,
            "started_by": started_by_id,
            "finished_by": finished_by_id,
        }

        return (aos_started_id, aos_finished_id, last_transition_id)

    def generate_started_by(self, start_place: str):
        waiting_for_started_by_id = create_place("Waiting for \n StartedBy", self.petri_net)
        started_by_id = create_place("StartedBy \n satisfied", self.petri_net)
        start_transition_id = create_transition("", "", self.petri_net)
        self.petri_net.add_input(started_by_id, start_transition_id, Value(1))
        self.petri_net.add_input(waiting_for_started_by_id, start_transition_id, Value(1))
        self.petri_net.add_output(start_place, start_transition_id, Value(1))
        start_place = waiting_for_started_by_id  # set new starting point

        return (started_by_id, start_place, start_transition_id)

    def generate_finished_by(self, last_transition_id: str, finished_place_id: str):
        waiting_for_finished_by_id = create_place("Waiting for \n FinishedBy", self.petri_net)
        finished_by_id = create_place("FinishedBy \n satisfied", self.petri_net)
        finished_by_transition_id = create_transition("", "", self.petri_net)
        self.petri_net.add_input(finished_by_id, finished_by_transition_id, Value(1))
        self.petri_net.add_input(waiting_for_finished_by_id, finished_by_transition_id, Value(1))
        self.petri_net.add_output(waiting_for_finished_by_id, last_transition_id, Value(1))
        self.petri_net.add_output(finished_place_id, finished_by_transition_id, Value(1))

        return (finished_by_id, finished_by_transition_id)

    def generate_on_done(
        self, task_or_orderstep: Union[Task, TransportOrderStep, MoveOrderStep, ActionOrderStep]
    ) -> None:
        last_transition_ids = []
        if isinstance(task_or_orderstep, Task):
            last_transition_ids = [self.task_ids[task_or_orderstep.name]["last_transition"]]
        else:
            last_transition_ids = self.orderstep_to_finish_transitions[task_or_orderstep.id]
            task_or_orderstep = task_or_orderstep.orderstep

        for last_transition_id in last_transition_ids:
            for follow_up_task in task_or_orderstep.follow_up_task_names:
                connection_id = ""

                on_done_place_id = create_place(" ", self.petri_net)

                # For every follow up task there is a connection transition created.
                # If multiple tasks connects to this transition dont create a new one every time
                if follow_up_task not in self.on_done_transition_dict:
                    connection_id = create_transition("connection", "", self.petri_net)

                    self.add_callback(
                        connection_id,
                        self.callbacks.task_started_callback,
                        self.mfdl_program.tasks[follow_up_task],
                    )

                    self.on_done_transition_dict[follow_up_task] = connection_id
                    starting_place = self.task_ids[follow_up_task]["starting_place"]
                    self.petri_net.add_output(starting_place, connection_id, Value(1))

                    # add callback only once
                    task = self.mfdl_program.tasks[follow_up_task]
                    self.add_callback(connection_id, self.callbacks.started_by_callback, task)
                else:
                    connection_id = self.on_done_transition_dict[follow_up_task]

                self.petri_net.add_input(on_done_place_id, connection_id, Value(1))
                self.petri_net.add_output(on_done_place_id, last_transition_id, Value(1))


def create_place(name: str, petri_net: PetriNet, id_="") -> str:
    """Utility function for creating a place with the snakes module.

    This function is used to add a place with the given name and to add labels for
    scheduling (for example if the place represents an event or if its initialized).
    """
    if id_ == "":
        id_ = str(uuid.uuid4())
    petri_net.add_place(Place(id_, []))
    petri_net.place(id_).label(name=name)
    return id_


def create_transition(transition_name: str, transition_type: str, petri_net: PetriNet) -> str:
    """Utility function for creating a transition with the snakes module.

    This function is used to add a transition with the given name and to add labels for
    scheduling (currently only the type of the transition).
    """
    transition_id = str(uuid.uuid4())
    petri_net.add_transition(Transition(transition_id))
    petri_net.transition(transition_id).label(name=transition_name, transitionType=transition_type)
    return transition_id
