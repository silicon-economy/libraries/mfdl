# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains SyntaxErrorListener class."""

# 3rd party libs
from antlr4.error.ErrorListener import ErrorListener

# local sources
from mfdl_scheduler.validation.error_handler import ErrorHandler


class SyntaxErrorListener(ErrorListener):
    """Custom ErrorListener for the MFDL.

    Overrides Antlr ErrorListener class so we can use
    our ErrorHandler class for syntax errors.

    Attributes:
        error_handler: ErrorHandler instance for printing errors while visiting.
    """

    def __init__(self, error_handler: ErrorHandler) -> None:
        super().__init__()
        self.error_handler = error_handler

    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e) -> None:
        """Overwrites the ANTLR ErrorListener method to use the error handler."""
        self.error_handler.print_error(msg, line=line, column=column, syntax_error=True)
