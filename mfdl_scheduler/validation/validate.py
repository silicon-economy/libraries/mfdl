# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains helper functions for starting the scheduler."""

# standard libraries
import re
from typing import Union

# 3rd party libs
from antlr4.CommonTokenStream import CommonTokenStream
from antlr4.InputStream import InputStream

# local sources
from mfdl_scheduler.parser.mfdl_visitor import MFDLVisitor
from mfdl_scheduler.parser.MFDLLexer import MFDLLexer
from mfdl_scheduler.parser.MFDLParser import MFDLParser
from mfdl_scheduler.model.mfdl_program import MFDLProgram
from mfdl_scheduler.helpers import load_file
from mfdl_scheduler.validation.error_handler import ErrorHandler
from mfdl_scheduler.validation.syntax_error_listener import SyntaxErrorListener
from mfdl_scheduler.validation.semantic_error_checker import SemanticErrorChecker


def parse_string(mfdl_string, file_path="") -> Union[None, MFDLProgram]:
    """Instantiate the ANTLR lexer and parser and parses the given MFDL string.

    Args:
        mfdl_string: A string containing the content of a MFDL file.
        file_path: The path of the MFDL file (used for error messages).
    """

    error_handler = ErrorHandler(file_path)
    error_listener = SyntaxErrorListener(error_handler)
    lexer = MFDLLexer(InputStream(mfdl_string))
    lexer.removeErrorListeners()
    lexer.addErrorListener(error_listener)
    token_stream = CommonTokenStream(lexer)

    parser = MFDLParser(token_stream)
    parser.removeErrorListeners()
    parser.addErrorListener(error_listener)

    tree = parser.program()

    mfdl_program = None
    if not error_handler.has_error():
        visitor = MFDLVisitor(file_path, error_handler)
        mfdl_program = visitor.visitProgram(tree)
        write_tokens_to_file(token_stream)

        if not error_handler.has_error():
            semantic_error_checker = SemanticErrorChecker(
                "", mfdl_program, error_handler=error_handler
            )

            if not semantic_error_checker.mfdl_program_valid():
                mfdl_program = None
    return mfdl_program


def parse_file(file_path) -> Union[None, MFDLProgram]:
    """Loads the content of the file from the given path and calls the parse_string function.

    Args:
        file_path: The path to the MFDL file.
    """

    mfdl_string = load_file(file_path)
    return parse_string(mfdl_string, file_path=file_path)


def write_tokens_to_file(token_stream: CommonTokenStream) -> None:
    """Writes the given ANTLR CommonTokenStream into a file named 'token.txt'."""
    with open("token.txt", "w", encoding="utf-8") as file:
        pattern = re.compile("\r?\n.*")
        for token in token_stream.tokens:
            token_text = token.text
            if re.match(pattern, token.text):
                token_text = "NL"
            file.write(token_text + "\n")
