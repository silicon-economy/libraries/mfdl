# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains the ErrorHandler class."""


class ErrorHandler:
    """Keeps track of the total error amount in an MFDL file.

    Provides a method for printing an erro which counts the errors.

    Attributes:
        total_error_count: Total number of errors
        syntax_error_count: Number of syntax errors
        semantic_error_count: Number of static semantic errors
        file_path: The file path to the MFDL file
    """

    def __init__(self, file_path: str) -> None:
        self.total_error_count: int = 0
        self.syntax_error_count: int = 0
        self.semantic_error_count: int = 0
        self.file_path: str = file_path

    def print_error(
        self,
        error_msg: str,
        line: int = 0,
        column: int = 0,
        context=None,
        syntax_error: bool = False,
    ) -> None:
        """Prints an error into the standard output.

        Args:
            error_msg: A string containing the error message
            line: The line in which the error occured
            column: The column in which the error occured
            off_symbol_length: Length of the offending symbol
            context: ANTLR Context object (lines and column will be used from this if not None)
            syntax_error: A boolean indicating whether the error is a syntax error or not.
        """
        if context:
            line = context.start.line
            column = context.start.column
        print(error_msg)
        print("File: " + self.file_path + ", in line " + str(line) + ":" + str(column))

        if syntax_error:
            self.syntax_error_count = self.syntax_error_count + 1
        else:
            self.semantic_error_count = self.semantic_error_count + 1

        self.total_error_count = self.total_error_count + 1

    def has_error(self) -> bool:
        """Returns true if the total error_count is greater than zero."""
        return self.total_error_count > 0
