# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Contains the SemanticErrorChecker class."""

# standard libraries
from numbers import Number
from typing import Any, Dict, Tuple, Union

# 3rd party libs
from antlr4.ParserRuleContext import ParserRuleContext

# local sources
from mfdl_scheduler.model.mfdl_program import MFDLProgram
from mfdl_scheduler.model.moveorderstep import MoveOrderStep
from mfdl_scheduler.model.struct import Struct
from mfdl_scheduler.model.instance import Instance
from mfdl_scheduler.model.module import Module
from mfdl_scheduler.model.rule import Rule
from mfdl_scheduler.model.task import Task
from mfdl_scheduler.model.moveorder import MoveOrder
from mfdl_scheduler.model.actionorder import ActionOrder
from mfdl_scheduler.model.transportorder import TransportOrder
from mfdl_scheduler.model.transportorderstep import TransportOrderStep
from mfdl_scheduler.model.actionorderstep import ActionOrderStep

from mfdl_scheduler.validation.error_handler import ErrorHandler

from mfdl_scheduler.helpers import (
    check_type_of_value,
    is_boolean,
    is_attribute_access,
    substitute_parameter_in_expression,
)

# Constants
TRIGGERED_BY_KEY = "TriggeredBy"
FINISHED_BY_KEY = "FinishedBy"
ELEMENTARY_ATTRIBUTES = ["id", "time"]
NUMBER_OPERATORS = ["<", ">", "<=", ">=", "+", "-", "/", "*"]
PRIMITIVES = [
    "number",
    "string",
    "boolean",
    "Location",
    "Event",
    "Time",
    "Constraint",
]

# TODO: Check if there is either a Location or a Parameter statement or both.
# TODO: Check if ActionOrderStep name is in TransportOrderSteps -> error
class SemanticErrorChecker:
    """Checks the given MFDLProgram for static semantic errors and prints them.

    After parsing and visiting this phase checks for semantic errors and adds
    missing information in the model classes.
    Bitwise operators are used in the check methods if methods should be executed even if
    one method returns False for example (so no short-circuit evaluation).

    Attributes:
        file_path: A string representing the file path of the parsed MFDL file.
        mfdl_program: The MFDLProgram instance that should be checked for errors.
        error_handler: The ErrorHandler instance for printing the errors.
    """

    def __init__(
        self,
        file_path: str,
        mfdl_program: MFDLProgram,
        error_handler: ErrorHandler = None,
    ) -> None:
        self.file_path: str = file_path
        self.mfdl_program: MFDLProgram = mfdl_program
        self.error_handler: ErrorHandler = error_handler

    def mfdl_program_valid(self) -> bool:
        """Starts static semantic checks for the MFDL program.

        Returns:
            True, if the MFDL program contains no errors.
        """
        return (
            self.check_structs()
            & self.check_instances()
            & self.check_modules()
            & self.check_rules()
            & self.check_tasks()
            & self.check_transport_order_steps()
            & self.check_move_order_steps()
            & self.check_action_order_steps()
        )

    def check_structs(self) -> bool:
        """Executes semantic checks for each Struct.

        Returns:
            True if all Struct definitions are valid.
        """
        error_found = False
        for struct in self.mfdl_program.structs.values():
            if not (
                self.check_for_invalid_struct_name(struct)
                & self.check_for_unknown_parent_struct(struct)
                & self.check_for_unknown_datatypes_in_struct_definition(struct)
            ):
                error_found = True
        return not error_found

    def check_for_invalid_struct_name(self, struct: Struct) -> bool:
        """Checks if the Struct name starts with a Uppercase character.

        Returns:
            True if the Struct name starts with an Uppercase character.
        """
        if struct.name[0] != "_" and struct.name[0].islower():
            error_msg = f"The Struct '{struct.name}' contains an invalid identifier."
            self.error_handler.print_error(error_msg, context=struct.context)
            return False
        return True

    def check_for_unknown_parent_struct(self, struct: Struct) -> bool:
        """Checks if the Structs parent is a known Struct.

        Returns:
            True if the Structs parent is a known Struct."""
        if (
            struct.parent_struct_name != ""
            and struct.parent_struct_name not in self.mfdl_program.structs
        ):
            error_msg = (
                f"The Struct '{struct.name}' tries to inherit from an unknown Struct "
                f"'{struct.parent_struct_name}'."
            )
            self.error_handler.print_error(error_msg, context=struct.context)
            return False
        return True

    def check_for_unknown_datatypes_in_struct_definition(self, struct: Struct) -> bool:
        """Checks for each attribute definition in the Struct if the defined type exists.

        Returns:
            True if all attributes are defined with a known type.
        """
        datatypes_correct = True
        for identifier, attribute_type in struct.attributes.items():
            if not self.check_if_variable_definition_is_valid(
                identifier, attribute_type, struct.attribute_contexts[identifier]
            ):
                datatypes_correct = False
        return datatypes_correct

    def check_instances(self) -> bool:
        """Executes semantic checks for all Instances.

        Returns:
            True if all Instances are valid.
        """
        error_found = False
        for instance in self.mfdl_program.instances.values():
            error_found = not self.check_for_invalid_instance_name(instance)
            struct_name = instance.struct_name
            struct = None
            if struct_name in self.mfdl_program.structs:
                struct = self.mfdl_program.structs[struct_name]

            # check first, if the corresponding Struct exists
            if struct is None:
                error_msg = (
                    f"The Instance '{instance.name}' refers to a struct that does not exist."
                )
                self.error_handler.print_error(error_msg, context=instance.context)
                error_found = True
            else:
                if not (
                    self.check_if_instance_attributes_exist_in_struct(struct, instance)
                    & (self.check_if_struct_attributes_are_assigned(struct, instance))
                ):
                    error_found = True
                else:
                    error_found = not self.check_if_value_matches_with_defined_type(
                        struct, instance
                    )
        return not error_found

    def check_for_invalid_instance_name(self, instance: Instance) -> bool:
        """Checks if the Instance name starts with a lowercase character.

        Returns:
            True if the Instance name starts with a lowercase character.
        """
        if instance.name[0] != "_" and instance.name[0].isupper():
            error_msg = f"The Instance '{instance.name}' contains an invalid identifier."
            self.error_handler.print_error(error_msg, context=instance.context)
            return False
        return True

    def check_if_instance_attributes_exist_in_struct(
        self, struct: Struct, instance: Instance
    ) -> bool:
        """Checks if all attributes in the given Instance exists in the corresponding Struct.

        Returns:
            True if all attributes in the given Instance exists in the corresponding Struct.
        """
        error_found = False
        for attribute_name, attribute_value in instance.attributes.items():
            if attribute_name not in ELEMENTARY_ATTRIBUTES:
                attribute_found = False

                for struct_attribute in struct.attributes:
                    if is_attribute_access(attribute_name) and self.check_attribute_access(
                        struct, attribute_name
                    ):
                        attribute_found = True
                    elif attribute_name == struct_attribute:
                        attribute_found = True
                if attribute_found is False:
                    error_msg = (
                        f"The attribute '{attribute_name}' in instance '{instance.name}' "
                        + "was not defined in the corresponding struct"
                    )

                    self.error_handler.print_error(
                        error_msg,
                        context=instance.context,
                    )
                    error_found = True
                elif isinstance(attribute_value, Instance):
                    # the attribute exists in the given struct so we can get the type
                    nested_struct = self.mfdl_program.structs[struct.attributes[attribute_name]]
                    if not self.check_if_instance_attributes_exist_in_struct(
                        nested_struct, attribute_value
                    ):
                        error_found = True
        return not error_found

    def check_attribute_access(self, struct: Struct, attr_access: str) -> bool:
        """Checks if every attribute in the attribute access exists in the corresponding structs.

        Returns:
            True if every attribute in the attribute access exists in the corresponding structs.
        """
        attributes = attr_access.split(".")

        for i, attribute in enumerate(attributes):
            if attribute in struct.attributes:

                # last element in attribute list
                if i == len(attributes) - 1:
                    pass
                elif struct.attributes[attribute] not in self.mfdl_program.structs:
                    error_msg = f"Unknown attribute '{attribute}'"
                    self.error_handler.print_error(error_msg, context=struct.context)
                    return False
                else:
                    struct = self.mfdl_program.structs[struct.attributes[attribute]]
            else:
                error_msg = f"Unknown attribute '{attribute}'"
                self.error_handler.print_error(error_msg, context=struct.context)
                return False
        return True

    def check_if_struct_attributes_are_assigned(self, struct: Struct, instance: Instance) -> bool:
        """Checks if all attributes from the corresponding struct are assigned
        with values in the instance.
        """

        struct_attributes = dict(
            filter(lambda e: e[0] not in ELEMENTARY_ATTRIBUTES, struct.attributes.items())
        )
        error_found = False
        for struct_attribute in struct_attributes:
            attribute_found = False
            for attribute_name, attribute_value in instance.attributes.items():
                if is_attribute_access(attribute_name):
                    attribute_name = attribute_name.split(".")[0]
                if struct_attribute == attribute_name:
                    attribute_found = True
                if isinstance(attribute_value, Instance):
                    nested_struct = self.mfdl_program.structs[struct.attributes[attribute_name]]
                    if not self.check_if_struct_attributes_are_assigned(
                        nested_struct, attribute_value
                    ):
                        error_found = True
            if attribute_found is False:
                error_msg = (
                    f"The attribute '{struct_attribute}' from the corresponding struct was not "
                    f"definied in instance '{instance.name}'"
                )
                self.error_handler.print_error(error_msg, context=instance.context)
                error_found = True
        return not error_found

    def check_if_value_matches_with_defined_type(self, struct: Struct, instance: Instance) -> bool:
        """Checks if the assigned values in the Instance match with the defined type in the Struct.

        Returns:
            True if if the assigned values in the Instance match
            with the defined type in the Struct.
        """
        error_found = False
        for attribute_name, attribute_value in instance.attributes.items():
            struct_attr_type = None
            if is_attribute_access(attribute_name):
                (
                    struct_attr_name,
                    struct_attr_type,
                ) = self.get_last_type_and_name_of_attribute_access(attribute_name, struct)
            else:
                # This methdod assumes that the attribut exists in the Struct
                struct_attr_type = struct.attributes[attribute_name]

            if isinstance(attribute_value, Instance):
                nested_struct = self.mfdl_program.structs[struct_attr_type]
                error_found = not self.check_if_value_matches_with_defined_type(
                    nested_struct, attribute_value
                )
            elif not check_type_of_value(attribute_value, struct_attr_type):
                error_msg = (
                    f"The attribute '{attribute_name}' in instance '{instance.name}' has the "
                    f"wrong type: should be '{struct_attr_type}'."
                )
                self.error_handler.print_error(error_msg, context=instance.context)
                error_found = True
        return not error_found

    def check_modules(self) -> bool:
        """Executes semantic checks for all Modules.

        Returns:
            True if all Modules are valid.
        """
        error_found = False
        temp_error_checker = SemanticErrorChecker(self.file_path, None, self.error_handler)

        for module in self.mfdl_program.modules.values():
            if not self.check_for_invalid_module_name(module):
                error_found = True

            mfdl_program = MFDLProgram()
            mfdl_program.append_mfdl_program_or_module_content(module)
            temp_error_checker.mfdl_program = mfdl_program
            temp_error_checker.mfdl_program_valid()
        return not error_found

    def check_for_invalid_module_name(self, module: Module) -> bool:
        """Checks if the Module name starts with a lowercase character.

        Returns:
            True if the Module name starts with a lowercase character.
        """
        if module.name[0] != "_" and module.name[0].isupper():
            error_msg = f"Invalid identifier '{module.name}'."
            self.error_handler.print_error(error_msg, context=module.context)
            return False
        return True

    def check_rules(self) -> bool:
        """Executes semantic checks for all Rules.

        Returns:
            True if all Rules are valid.
        """
        error_found = False
        for rule in self.mfdl_program.rules.values():
            if not self.check_for_invalid_rule_name(rule):
                error_found = True
        return not error_found

    def check_for_invalid_rule_name(self, rule: Rule) -> bool:
        """Checks if the Rule name starts with a lowercase character.

        Returns:
            True if the Rule name starts with a lowercase character.
        """
        if rule.name[0] != "_" and rule.name[0].isupper():
            error_msg = f"Invalid identifier '{rule.name}'."
            self.error_handler.print_error(error_msg, context=rule.context)
            return False
        return True

    def check_tasks(self) -> bool:
        """Executes semantic checks for all Tasks.

        Returns:
            True if the Task definition contains no static semantic errors."""
        error_found = False
        for task in self.mfdl_program.tasks.values():
            if not (
                self.check_for_invalid_task_name(task)
                & self.check_task_statements(task)
                & self.check_started_by(task)
                & self.check_finished_by(task)
                & self.check_on_done(task)
                & self.check_repeat_or_on_done(task)
                & self.check_constraints(task)
            ):
                error_found = True
        return not error_found

    def check_for_invalid_task_name(self, task: Task) -> bool:
        """Checks if the Task name starts with a lowercase character.

        Returns:
            True if the Task name starts with a lowercase character.
        """
        if task.name[0] != "_" and task.name[0].isupper():
            error_msg = f"Invalid identifier '{task.name}'."
            self.error_handler.print_error(error_msg, context=task.context)
            return False
        return True

    def check_task_statements(self, task: Task) -> bool:
        """Executes semantic checks for all statements in a Task
        (TransportOrder, MoveOrder and ActionOrder).

        Returns:
            True if all statements are valid.
        """
        error_found = False

        transport_found = False

        for task_statement in task.statements:
            if isinstance(task_statement, TransportOrder):
                if not self.check_transport_order(task, task_statement):
                    error_found = True
                transport_found = True
            elif isinstance(task_statement, MoveOrder):
                if not self.check_move_order(task, task_statement):
                    error_found = True
                if not transport_found:
                    error_found = True
                    error_msg = f"Move or Action before Transport in Task '{task.name}'."
                    self.error_handler.print_error(error_msg, context=task.context)
            elif isinstance(task_statement, ActionOrder):
                if not self.check_action_order(task, task_statement):
                    error_found = True
                if not transport_found:
                    error_found = True
                    error_msg = f"Move or Action before Transport in Task '{task.name}'."
                    self.error_handler.print_error(error_msg, context=task.context)

        return not error_found

    def check_transport_order(self, task: Task, transport_order: TransportOrder) -> bool:
        """Executes semantic checks for the given TransportOrder.

        Returns:
            True if the TransportOrder is valid.
        """

        for tos_name in transport_order.pickup_tos_names:
            if not self.tos_exists(tos_name):
                error_msg = (
                    f"Task '{task.name}' refers to an unknown "
                    f"TransportOrderStep in 'From': '{tos_name}'."
                )
                self.error_handler.print_error(error_msg, context=transport_order.context)
                return False

        if not self.tos_exists(transport_order.delivery_tos_name):
            error_msg = (
                f"Task '{task.name}' refers to an unknown "
                f"TransportOrderStep in 'To': '{transport_order.delivery_tos_name}'."
            )
            self.error_handler.print_error(error_msg, context=transport_order.context)
            return False
        return True

    def check_move_order(self, task: Task, move_order: MoveOrder) -> bool:
        """Executes semantic checks for the given MoveOrder.

        Returns:
            True if the MoveOrder is valid.
        """
        mos_name = move_order.moveorderstep_name
        if not self.mos_exists(mos_name):
            error_msg = (
                f"Task '{task.name}' refers to an unknown " f"MoveOrderStep in 'To': '{mos_name}'."
            )
            self.error_handler.print_error(error_msg, context=move_order.context)
            return False
        return True

    def check_action_order(self, task: Task, action_order: ActionOrder) -> bool:
        """Executes semantic checks for the given ActionOrder.

        Returns:
            True if the ActionOrder is valid.
        """
        aos_name = action_order.actionorderstepname
        if not self.aos_exists(aos_name):
            error_msg = (
                f"Task '{task.name}' refers to an unknown "
                f"ActionOrderStep in 'Do': '{aos_name}'."
            )
            self.error_handler.print_error(error_msg, context=action_order.context)
            return False
        return True

    def check_started_by(
        self, task_or_tos_or_aos: Union[Task, TransportOrderStep, ActionOrderStep]
    ) -> bool:
        """Checks if the StartedBy expression is valid.

        Returns:
            True if the StartedBy expression is valid.
        """
        expression = task_or_tos_or_aos.started_by_expr
        if expression:
            return self.check_expression(expression, task_or_tos_or_aos.context_dict["StartedBy"])
        return True

    def check_finished_by(
        self, task_or_tos_or_aos: Union[Task, TransportOrderStep, ActionOrderStep]
    ) -> bool:
        """Checks if the FinishedBy expression is valid.

        Returns:
            True if the FinishedBy expression is valid.
        """
        expression = task_or_tos_or_aos.finished_by_expr
        if expression:
            return self.check_expression(expression, task_or_tos_or_aos.context_dict["FinishedBy"])
        return True

    def check_on_done(
        self, task_or_tos_or_aos: Union[Task, TransportOrderStep, ActionOrderStep]
    ) -> bool:
        """Checks if the tasks in OnDone refer to known tasks.

        Returns:
            True if the tasks in OnDone refer to known tasks.
        """
        error_found = False
        for on_done_task in task_or_tos_or_aos.follow_up_task_names:
            if self.task_exists(on_done_task) is False:
                error_msg = (
                    f"The task name '{on_done_task}' in the OnDone statement refers "
                    f"to an unknown Task."
                )
                self.error_handler.print_error(error_msg, context=task_or_tos_or_aos.context)
                error_found = True
        return not error_found

    def check_repeat_or_on_done(self, task: Task) -> bool:
        """Checks if the task has either a Repeat or a OnDone statement.

        Returns:
            True if the task has either a Repeat or a OnDone statement.
        """
        if task.repeat and task.follow_up_task_names:
            error_msg = (
                f"The task '{task.name}' has both OnDone and Repeat statements. "
                f"Only one of the two is allowed."
            )
            self.error_handler.print_error(error_msg, context=task.context)
            return False
        return True

    def check_constraints(self, task: Task) -> bool:
        """Checks if the constraint is valid.

        Constraints can be a JSON object or an expression. If its a JSON object no further checks
        have to be executed, so just check if the constraint is an expression.

        Returns:
            True if the constraint is valid.
        """
        if isinstance(task.constraints, Dict):
            if not ("value" in task.constraints or "binOp" in task.constraints):
                # its a json object, will not be checked here
                return True
        return self.check_expression(task.constraints, task.context_dict["Constraints"])

    def check_rule_call(
        self,
        rule_call: Tuple,
        context: ParserRuleContext = None,
    ) -> bool:
        """Executes semantic checks for a Rule call.

        A Rule call can be used inside a Rule definiton or inside
        Tasks and OrderSteps (e.g. with StartedBy or FinishedBy).
        If parameters are used, substitute the parameters inside the corresponding
        Rule definition with the real values and check the expressions then.

        Returns:
            True if the Rule call is valid.
        """
        rule_name = rule_call[0]
        if not rule_name in self.mfdl_program.rules:
            error_msg = f"Rulecall '{rule_name}' refers to an unknown Rule."
            self.error_handler.print_error(error_msg, context=context)
            return False

        error_found = False

        rule = self.mfdl_program.rules[rule_name]
        rule_call_parameters = rule_call[1]
        subs = {}
        if len(rule.parameters) > 0:

            for i, (rule_parameter, default_value) in enumerate(rule.parameters.items()):
                if i >= len(rule_call_parameters):  # use default value from rule call
                    if default_value is not None:
                        subs[rule_parameter] = default_value
                    else:
                        error_msg = f"Neither default value nor value for parameter '{rule_parameter}' was provided."
                        self.error_handler.print_error(error_msg, context=context)
                        error_found = True
                else:
                    subs[rule_parameter] = list(rule_call_parameters)[
                        i
                    ]  # values of the rule call are the keys

        for expression in rule.expressions:
            if subs:
                sub_expr = substitute_parameter_in_expression(expression, subs)
                if not self.check_expression(sub_expr):
                    error_found = True
            else:
                if not self.check_expression(expression):
                    error_found = True
        return not error_found

    def check_expression(
        self, expression: Union[str, Tuple, Dict], context: ParserRuleContext = None
    ) -> bool:
        """Executes checks for the given expression.

        Returns:
            True if the given expression is valid.
        """
        if isinstance(expression, str) and not self.check_single_expression(expression, context):
            return False

        # A Rule call is always a boolean expression so check if the rule call
        # itself is valid with the given parameters
        if isinstance(expression, Tuple):
            return self.check_rule_call(expression, context)
        if isinstance(expression, dict):
            if len(expression) == 2:
                if not self.check_unary_operation(expression, context):
                    return False
            else:
                if not self.check_binary_operation(expression, context):
                    return False
        return True

    def check_single_expression(self, expression, context: ParserRuleContext) -> bool:
        """Checks if a single expression is a valid expression.

        Returns:
            True if the expression is valid.
        """
        if not (is_boolean(expression) or self.expression_is_number(expression, context)):
            error_msg = f"'{expression}' is not a valid boolean expression."
            self.error_handler.print_error(error_msg, context=context)
            return False
        return True

    def check_unary_operation(self, expression, context: ParserRuleContext) -> bool:
        """Checks if a unary expression is a valid expression.

        Returns:
            True if the given unary expression is a valid expression.
        """
        return self.check_expression(expression["value"], context)

    def check_binary_operation(self, expression, context: ParserRuleContext) -> bool:
        """Checks if a binary expression is a valid expression.

        Returns:
            True if the given binary expression is a valid expression.
        """
        left = expression["left"]
        right = expression["right"]

        if expression["binOp"] in NUMBER_OPERATORS:
            # Check if left and right side represent numbers or strings
            if self.expression_is_number(left, context) and self.expression_is_number(
                right, context
            ):
                return True

            error_msg = "Types of Right and left side of the comparison dont match"
            self.error_handler.print_error(error_msg, context=context)
            return False
        if expression["binOp"] in ["==", "!=", "And", "Or"]:
            if self.expression_is_string(left, context) and self.expression_is_string(
                right, context
            ):
                return True
            # Check if left and right side represent numbers or strings
            if self.expression_is_number(left, context) and self.expression_is_number(
                right, context
            ):
                return True
            if self.check_expression(left, context) and self.check_expression(right, context):
                return True

            error_msg = "Types of Right and left side of the comparison dont match"
            self.error_handler.print_error(error_msg, context=context)
            return False

        if left == "(" and right == ")":
            return self.check_expression(expression["binOp"], context)

    def check_transport_order_steps(self) -> bool:
        """Executes checks for all TransportOrderSteps.

        Returns:
            True if all TransportOrderSteps are valid.
        """
        error_found = False
        for tos in self.mfdl_program.transportordersteps.values():
            if not (
                self.check_for_invalid_tos_name(tos)
                & self.check_location(tos)
                & self.check_on_done(tos)
                & self.check_started_by(tos)
                & self.check_finished_by(tos)
            ):
                error_found = True
        return not error_found

    def check_for_invalid_tos_name(self, tos: TransportOrderStep) -> bool:
        """Checks if the TransportOrderStep name starts with a lowercase character.

        Returns:
            True if the TransportOrderStep name starts with a lowercase character.
        """
        if tos.name[0] != "_" and tos.name[0].isupper():
            error_msg = f"Invalid identifier '{tos.name}'."
            self.error_handler.print_error(error_msg, context=tos.context)
            return False
        return True

    def check_location(self, tos: TransportOrderStep) -> bool:
        """Executes semantic checks for the Location statement of the TransportOrderStep.

        Returns:
            True if the Location statement of the TransportOrderStep is valid.
        """
        if tos.location_name != "":
            if tos.location_name in self.mfdl_program.instances:
                location_instance = self.mfdl_program.instances[tos.location_name]
                if location_instance.struct_name != "Location":
                    error_msg = (
                        f"The given instance '{tos.location_name}' is not a Location instance."
                    )
                    self.error_handler.print_error(error_msg, context=tos.context_dict["Location"])
                    return False
            else:
                error_msg = f"There is no location instance with the name '{tos.location_name}'."
                self.error_handler.print_error(error_msg, context=tos.context_dict["Location"])
                return False
        return True

    def check_move_order_steps(self) -> bool:
        """Executes checks for all MoveOrderSteps.

        Returns:
            True if all MoveOrderSteps are valid.
        """
        error_found = False
        for mos in self.mfdl_program.moveordersteps.values():
            if not (
                self.check_for_invalid_mos_name(mos)
                & self.check_location(mos)
                & self.check_on_done(mos)
                & self.check_started_by(mos)
                & self.check_finished_by(mos)
            ):
                error_found = True
        return not error_found

    def check_for_invalid_mos_name(self, mos: MoveOrderStep) -> bool:
        """Checks if the MoveOrderStep name starts with a lowercase character.

        Returns:
            True if the MoveOrderStep name starts with a lowercase character.
        """
        if mos.name[0] != "_" and mos.name[0].isupper():
            error_msg = f"Invalid identifier '{mos.name}'."
            self.error_handler.print_error(error_msg, context=mos.context)
            return False
        return True

    def check_action_order_steps(self) -> bool:
        """Executes checks for all ActionOrderSteps.

        Returns:
            True if all ActionOrderSteps are valid.
        """
        error_found = False

        for aos in self.mfdl_program.actionordersteps.values():
            if not (
                self.check_for_invalid_aos_name(aos)
                & self.check_on_done(aos)
                & self.check_started_by(aos)
                & self.check_finished_by(aos)
            ):
                error_found = True
        return not error_found

    def check_for_invalid_aos_name(self, aos: ActionOrderStep) -> bool:
        """Checks if the ActionorderStep name starts with a lowercase character.

        Returns:
            True if the ActionorderStep name starts with a lowercase character.
        """
        if aos.name[0] != "_" and aos.name[0].isupper():
            error_msg = f"Invalid identifier '{aos.name}'."
            self.error_handler.print_error(error_msg, context=aos.context)
            return False
        return True

    def check_if_variable_definition_is_valid(
        self, identifier: str, variable_type: str, context
    ) -> bool:
        """Checks if the variable has the correct type.

        Returns:
            True if variable definition is valid.
        """
        if not self.variable_type_exists(variable_type):
            error_msg = f"Unknown data type '{variable_type}' " f"input variable '{identifier}'"
            self.error_handler.print_error(error_msg, context=context)
            return False

        return True

    def expression_is_number(self, expression, context: ParserRuleContext) -> bool:
        """Checks if the given expression is a number (int or float).

        Returns:
            True if the given expression is a number.
        """
        if isinstance(expression, str) and is_attribute_access(expression):
            attribute_access = expression[expression.find(".") + 1 :]
            instance_name = expression.split(".")[0]
            if instance_name in self.mfdl_program.instances:
                struct = self.mfdl_program.structs[
                    self.mfdl_program.instances[instance_name].struct_name
                ]
                name, type = self.get_last_type_and_name_of_attribute_access(
                    attribute_access, struct
                )
                return type == "number"
            return False
        if isinstance(expression, Number):
            return True
        if isinstance(expression, dict) and len(expression) == 3:
            if expression["left"] == "(" and expression["right"] == ")":
                return self.expression_is_number(expression["binOp"], context)
            if expression["binOp"] in NUMBER_OPERATORS:
                return self.expression_is_number(
                    expression["left"], context
                ) and self.expression_is_number(expression["right"], context)
        return False

    def expression_is_string(self, expression, context: ParserRuleContext) -> bool:
        """Checks if the given expression is a MFDL string.

        Returns:
            True if the given expression is a MFDL string.
        """
        if isinstance(expression, str) and is_attribute_access(expression):
            attribute_access = expression[expression.find(".") + 1 :]
            instance_name = expression.split(".")[0]
            if instance_name in self.mfdl_program.instances:
                struct = self.mfdl_program.structs[
                    self.mfdl_program.instances[instance_name].struct_name
                ]
                name, type = self.get_last_type_and_name_of_attribute_access(
                    attribute_access, struct
                )
                return type == "string"

            return False
        if isinstance(expression, str):
            return True
        if (
            isinstance(expression, dict)
            and len(expression) == 3
            and expression["left"] == "("
            and expression["right"] == ")"
        ):
            return self.expression_is_string(expression["binOp"], context)
        return False

    def tos_exists(self, name: str) -> bool:
        """Checks if the given string is the name of a TransportOrderStep.

        Returns:
            True if the given string is the name of a TransportOrderStep.
        """
        for tos_name in self.mfdl_program.transportordersteps:
            if name == tos_name:
                return True
        return False

    def mos_exists(self, name: str) -> bool:
        """Checks if the given string is the name of a MoveOrderStep.

        Returns:
            True if the given string is the name of a MoveOrderStep.
        """
        for mos_name in self.mfdl_program.moveordersteps:
            if name == mos_name:
                return True
        return False

    def aos_exists(self, name: str) -> bool:
        """Checks if the given string is the name of a ActionOrderStep.

        Returns:
            True if the given string is the name of a ActionOrderStep.
        """
        for aos_name in self.mfdl_program.actionordersteps:
            if name == aos_name:
                return True
        return False

    def task_exists(self, name: str) -> bool:
        """Checks if the given string is the name of a Task.

        Returns:
            True if the given string is the name of a Task.
        """
        for task_name in self.mfdl_program.tasks:
            if name == task_name:
                return True
        return False

    def get_last_type_and_name_of_attribute_access(
        self, attr_list: str, struct: Struct
    ) -> Tuple[str, str]:
        """Traverses the attribute access to get the last attribute name and type.

        Returns:
            A Tuple consisting of the attribute name of the last element in the attribute access
            and the type of the corresponding attribute.
        """
        attributes = attr_list.split(".")
        last_name = ""
        last_type = ""
        for i, attribute in enumerate(attributes):
            # last element in attribute list
            if i == len(attributes) - 1:
                last_name = attribute
                last_type = struct.attributes[attribute]
            else:
                struct = self.mfdl_program.structs[struct.attributes[attribute]]
        return (last_name, last_type)

    def variable_type_exists(self, variable_type: str) -> bool:
        """Checks if the given variable type exists in the MFDL file.

        A variable type can be a primitive (number, string or boolean) or
        an identifier of a defined Struct.

        Returns:
            True if the variable type exists within the MFDL file.
        """
        if variable_type[0].isupper():
            if variable_type not in self.mfdl_program.structs:
                return False

        elif variable_type not in PRIMITIVES:
            return False
        return True
