from setuptools import setup

setup(
    py_modules=[],
    name="MFDL Scheduler",
    version="0.0.1",
    description="Execution engine for the Material Flow Description Language (MFDL).",
    author="Peter Detzner, Maximilian Hoerstrup",
    author_email="maximilian.hoerstrup@iml.fraunhofer.de",
    classifiers=[
        "Programming Language :: Python :: 3 :: Only",
    ],
    install_requires=[
        "antlr4-python3-runtime==4.9.3",
        "antlr-denter",
        "snakes",
        "croniter",
        "networkx",
    ],  # external packages as dependencies
)
