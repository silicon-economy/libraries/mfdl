lexer grammar MFDLLexer;

tokens { INDENT, DEDENT }

@lexer::header{
from antlr_denter.DenterHelper import DenterHelper
from MFDLParser import MFDLParser
}
@lexer::members {
class MFDLDenter(DenterHelper):
    def __init__(self, lexer, nl_token, indent_token, dedent_token, ignore_eof):
        super().__init__(nl_token, indent_token, dedent_token, ignore_eof)
        self.lexer: MFDLLexer = lexer

    def pull_token(self):
        return super(MFDLLexer, self.lexer).nextToken()

denter = None

def nextToken(self):
    if not self.denter:
        self.denter = self.MFDLDenter(self, self.NL, MFDLLexer.INDENT, MFDLLexer.DEDENT, ignore_eof=False)
    return self.denter.next_token()
}

// Main grammar
STRUCT: 'Struct';
RULE: 'Rule';
MODULE: 'Module';
IMPORT: 'Import';
TASK: 'Task';
TRANSPORT: 'Transport';
MOVE: 'Move';
ACTION: 'Action';
FROM: 'From';
TO: 'To';
DO: 'Do';
ON_DONE: 'OnDone';
END: 'End';
REPEAT: 'Repeat';
TRANSPORT_ORDER_STEP: 'TransportOrderStep';
MOVE_ORDER_STEP: 'MoveOrderStep';
ACTION_ORDER_STEP: 'ActionOrderStep';
CONSTRAINTS: 'Constraints';
PARAMETERS: 'Parameters';
STARTED_BY: 'StartedBy';
FINISHED_BY: 'FinishedBy';
LOCATION: 'Location';
EVENT: 'Event';
TIME: 'Time';
CONSTRAINT: 'Constraint';
NUMBER_P: 'number';
STRING_P: 'string';
BOOLEAN_P: 'boolean';
TRUE: 'True';
FALSE: 'False';

COLON: ':';
DOT: '.';
COMMA: ',';
JSON_OPEN: '{' -> pushMode(JSON);

QUOTE: '"';

ARRAY_LEFT: '[';
ARRAY_RIGHT: ']';

COMMENT : '#'~[\n]+  -> skip;
WHITESPACE: [ \t]+ -> skip;
NL: ('\r'? '\n' ' '*);

LEFT_PARENTHESIS: '(';
RIGHT_PARENTHESIS: ')';
LESS_THAN: '<';
LESS_THAN_OR_EQUAL: '<=';
GREATER_THAN: '>';
GREATER_THAN_OR_EQUAL: '>=';
EQUAL: '=';
EQUAL_B: '==';
NOT_EQUAL_B: '!=';
PLUS: '+';
MINUS: '-';
SLASH: '/';
STAR: '*';

BOOLEAN_AND: 'And';
BOOLEAN_OR: 'Or';
BOOLEAN_NOT: '!';

INTEGER: [0-9]+;
FLOAT: INTEGER '.'INTEGER;

STRING: '"' ('\\"'|.)*? '"';
ID: [a-zA-Z_][a-zA-Z0-9_]*;

// JSON sub grammar
mode JSON;

JSON_STRING: '"' ('\\"'|.)*? '"';

JSON_COLON: ':';
JSON_QUOTE: '"';

JSON_COMMENT : '#' ~[\n]+  -> skip;

JSON_ARRAY_LEFT: '[';
JSON_ARRAY_RIGHT: ']';

JSON_COMMA: ',';

NUMBER
   : '-'? INT ('.' [0-9] +)? EXP?
   ;

fragment INT
   : '0' | [1-9] [0-9]*
   ;

// no leading zeros

fragment EXP
   : [Ee] [+\-]? INT
   ;

WS
   : [ \t\n\r] + -> skip
   ;

JSON_OPEN_2: '{' -> pushMode(JSON);
JSON_CLOSE: '}' -> popMode;
