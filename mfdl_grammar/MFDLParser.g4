parser grammar MFDLParser;

options {
    tokenVocab = MFDLLexer;
}

program:
    import_stmt* (struct | instance | rule_ | module | task | orderStep | NL)* EOF;

import_stmt:
    IMPORT  module_path (COMMA module_path)* NL;

module_path:
    ID (DOT ID)*;

struct:
    STRUCT struct_id (COLON struct_id)? INDENT attribute_definition+ DEDENT END;

struct_id:
    LOCATION
    | EVENT
    | TIME
    | CONSTRAINT
    | ID;

instance:
    struct_id ID INDENT (attribute_assignment NL)+ DEDENT END;

rule_:
    RULE rule_call INDENT (expression NL)+ DEDENT END;

rule_call:
    ID LEFT_PARENTHESIS (rule_parameter (COMMA rule_parameter)*)? RIGHT_PARENTHESIS;

rule_parameter:
    (ID | value) (EQUAL value)?;

module:
    MODULE ID INDENT (struct | instance | rule_ | task | transportOrderStep | NL)* DEDENT END;

task:
    TASK ID INDENT taskStatement+ DEDENT END;

taskStatement:
    transportStatement
    | moveStatement
    | actionStatement
    | optStatement
    | repeatStatement
    | constraintStatement;

transportStatement:
    TRANSPORT NL fromStatement toStatement;

fromStatement:
    FROM ID (COMMA ID)* NL;

toStatement:
    TO ID NL;

moveStatement:
    MOVE NL TO ID NL;

actionStatement:
    ACTION NL DO ID NL;

constraintStatement:
    CONSTRAINTS (expression | json_object) NL;

repeatStatement:
    REPEAT INTEGER NL;

orderStep:
    transportOrderStep | moveOrderStep | actionOrderStep;

transportOrderStep:
    TRANSPORT_ORDER_STEP ID INDENT tosStatement+ DEDENT END;

tosStatement:
    locationStatement | parameterStatement | optStatement;

locationStatement:
    LOCATION ID NL;

parameterStatement:
    PARAMETERS (value | json_object) NL;

optStatement:
    eventStatement | onDoneStatement;

eventStatement:
    STARTED_BY expression NL
    | FINISHED_BY expression NL;

onDoneStatement:
    ON_DONE ID (COMMA ID)* NL;

moveOrderStep:
    MOVE_ORDER_STEP ID INDENT mosStatement+ DEDENT END;

mosStatement:
    locationStatement | optStatement;

actionOrderStep:
    ACTION_ORDER_STEP ID INDENT aosStatement+ DEDENT END;

aosStatement:
    parameterStatement | optStatement;

attribute_definition:
    ID COLON primitive NL;

attribute_assignment:
    attribute_access COLON (value | json_object);

attribute_access:
    ID (DOT ID)*;

primitive:
    NUMBER_P
    | STRING_P
    | BOOLEAN_P
    | LOCATION
    | EVENT
    | TIME
    | CONSTRAINT
    | ID;

value:
    TRUE 
    | FALSE
    | INTEGER
    | FLOAT
    | STRING;

expression:
    LEFT_PARENTHESIS expression RIGHT_PARENTHESIS
    | unOperation expression
    | expression binOperation expression
    | expression BOOLEAN_AND expression
    | expression BOOLEAN_OR expression 
    | attribute_access
    | value
    | rule_call;

binOperation:
    LESS_THAN
    | LESS_THAN_OR_EQUAL
    | GREATER_THAN
    | GREATER_THAN_OR_EQUAL
    | EQUAL_B
    | NOT_EQUAL_B
    | STAR
    | SLASH
    | MINUS
    | PLUS;

unOperation:
    BOOLEAN_NOT;

// Rules for JSON Objects
json_object:
    json_open_bracket pair (JSON_COMMA pair)* JSON_CLOSE
    | json_open_bracket JSON_CLOSE;

pair:
    JSON_STRING JSON_COLON json_value;

json_open_bracket:
    JSON_OPEN | JSON_OPEN_2;

json_value:
    JSON_STRING
    | TRUE 
    | FALSE
    | NUMBER
    | json_object
    | json_array;

json_array:
    JSON_ARRAY_LEFT json_value (JSON_COMMA json_value)* JSON_ARRAY_RIGHT
    | JSON_ARRAY_LEFT JSON_ARRAY_RIGHT;
